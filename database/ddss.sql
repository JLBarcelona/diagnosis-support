/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1_3306
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : ddss

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-05-19 15:27:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_account
-- ----------------------------
DROP TABLE IF EXISTS `tbl_account`;
CREATE TABLE `tbl_account` (
  `account_id` int(255) NOT NULL AUTO_INCREMENT,
  `license_number` varchar(255) DEFAULT NULL,
  `s2_number` varchar(11) DEFAULT NULL,
  `date_issue` date DEFAULT NULL,
  `date_expiry` date DEFAULT NULL,
  `ptr_number` varchar(11) DEFAULT NULL,
  `fn` varchar(255) DEFAULT NULL,
  `mn` varchar(255) DEFAULT NULL,
  `ln` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` longtext CHARACTER SET utf8,
  `user_type` int(1) DEFAULT '1',
  `question_1` varchar(255) DEFAULT NULL,
  `question_2` varchar(255) DEFAULT NULL,
  `answer_1` varchar(255) DEFAULT NULL,
  `answer_2` varchar(255) DEFAULT NULL,
  `is_delete` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_account
-- ----------------------------
INSERT INTO `tbl_account` VALUES ('1', null, null, null, null, null, 'JL', 'Soriano', 'Barcelona', 'Male', 'admin', '$2y$10$GPRzTMcURWAZ3x2kVnymeOiwyCN9ilU4P3/Y4vpEKZ6Z3.y4RTSvK', '0', null, null, null, null, null);
INSERT INTO `tbl_account` VALUES ('2', '091029849', '974910', '2009-07-23', '2030-12-25', '094761823', 'Greyson Cody', 'Almojera', 'Barcelona', 'Male', 'doctor', '$2y$10$GPRzTMcURWAZ3x2kVnymeOiwyCN9ilU4P3/Y4vpEKZ6Z3.y4RTSvK', '1', null, null, null, null, null);
INSERT INTO `tbl_account` VALUES ('3', null, null, null, null, null, 'Eleonor', 'Almojera', 'Bacelona', 'Female', 'nurse', '$2y$10$GPRzTMcURWAZ3x2kVnymeOiwyCN9ilU4P3/Y4vpEKZ6Z3.y4RTSvK', '2', null, null, null, null, null);
INSERT INTO `tbl_account` VALUES ('4', null, null, null, null, null, 'Jowee Anne', 'Soriano', 'Barcelona', 'Female', 'medic', '$2y$10$GPRzTMcURWAZ3x2kVnymeOiwyCN9ilU4P3/Y4vpEKZ6Z3.y4RTSvK', '3', null, null, null, null, null);
INSERT INTO `tbl_account` VALUES ('12', '', '', '0000-00-00', '0000-00-00', '', 'John Luis', 's', 'Barcelona', 'Male', 'asd', '$2y$10$z9.H7tufPDgLrGlyxqiSW.3.kaXYocc2g2tDkYa3vUI4941arFePm', '4', 'What was your childhood nickname?', 'What was your favorite place to visit as a child?', 'eeng', 'SCC', null);
INSERT INTO `tbl_account` VALUES ('13', '', '', '0000-00-00', '0000-00-00', '', 'asd', 'asd', 'asd', 'Male', 'asdsss', '$2y$10$kSVLeB2Zm/oOaYJqOM/tk.N7jOd31HcQNo9SikYq/gmuva.yLe9eW', '4', 'What was your childhood nickname?', 'What was the name of your first stuffed animal?', 'asd', 'asd', null);

-- ----------------------------
-- Table structure for tbl_illness
-- ----------------------------
DROP TABLE IF EXISTS `tbl_illness`;
CREATE TABLE `tbl_illness` (
  `illness_id` int(255) NOT NULL AUTO_INCREMENT,
  `illness_name` varchar(255) DEFAULT NULL,
  `is_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`illness_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_illness
-- ----------------------------
INSERT INTO `tbl_illness` VALUES ('1', 'Allergies', null);
INSERT INTO `tbl_illness` VALUES ('2', 'Alzheimers', null);
INSERT INTO `tbl_illness` VALUES ('3', 'Arthritis', null);
INSERT INTO `tbl_illness` VALUES ('4', 'Asthma', null);
INSERT INTO `tbl_illness` VALUES ('5', 'Blood Pressure', null);

-- ----------------------------
-- Table structure for tbl_illness_symptoms
-- ----------------------------
DROP TABLE IF EXISTS `tbl_illness_symptoms`;
CREATE TABLE `tbl_illness_symptoms` (
  `symptoms_id` int(255) NOT NULL AUTO_INCREMENT,
  `illness_id` int(255) DEFAULT NULL,
  `symptoms_name` varchar(255) DEFAULT NULL,
  `is_delete` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`symptoms_id`),
  KEY `illness_symp` (`illness_id`),
  CONSTRAINT `illness_symp` FOREIGN KEY (`illness_id`) REFERENCES `tbl_illness` (`illness_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_illness_symptoms
-- ----------------------------
INSERT INTO `tbl_illness_symptoms` VALUES ('4', '2', 'Memory lost', null);
INSERT INTO `tbl_illness_symptoms` VALUES ('5', '2', 'Chills', null);
INSERT INTO `tbl_illness_symptoms` VALUES ('6', '2', 'Pain', null);
INSERT INTO `tbl_illness_symptoms` VALUES ('7', '3', 'Stiffness', null);
INSERT INTO `tbl_illness_symptoms` VALUES ('8', '3', 'Swelling', null);
INSERT INTO `tbl_illness_symptoms` VALUES ('9', '3', 'Redness', null);
INSERT INTO `tbl_illness_symptoms` VALUES ('11', '4', 'Coughing', null);
INSERT INTO `tbl_illness_symptoms` VALUES ('12', '4', 'Difficulty breathing', null);
INSERT INTO `tbl_illness_symptoms` VALUES ('13', '4', 'Chest tightness', null);
INSERT INTO `tbl_illness_symptoms` VALUES ('14', '4', 'Shortness of breath', null);
INSERT INTO `tbl_illness_symptoms` VALUES ('15', '5', 'Severe headache.', null);
INSERT INTO `tbl_illness_symptoms` VALUES ('16', '5', 'Fatigue or confusion.', null);
INSERT INTO `tbl_illness_symptoms` VALUES ('17', '5', 'Vision problems.', null);
INSERT INTO `tbl_illness_symptoms` VALUES ('18', '5', 'Chest pain.', null);
INSERT INTO `tbl_illness_symptoms` VALUES ('19', '5', 'Difficulty breathing.', null);
INSERT INTO `tbl_illness_symptoms` VALUES ('20', '5', 'Irregular heartbeat.', null);
INSERT INTO `tbl_illness_symptoms` VALUES ('21', '5', 'Blood in the urine.', null);
INSERT INTO `tbl_illness_symptoms` VALUES ('22', '5', 'Pounding in your chest.', null);
INSERT INTO `tbl_illness_symptoms` VALUES ('30', '1', 'Itching', null);
INSERT INTO `tbl_illness_symptoms` VALUES ('31', '1', 'Skin Irritation', null);
INSERT INTO `tbl_illness_symptoms` VALUES ('32', '1', 'asd', null);

-- ----------------------------
-- Table structure for tbl_logs
-- ----------------------------
DROP TABLE IF EXISTS `tbl_logs`;
CREATE TABLE `tbl_logs` (
  `act_id` int(255) NOT NULL AUTO_INCREMENT,
  `message` longtext,
  `account_id` int(255) DEFAULT NULL,
  `date_entry` datetime DEFAULT NULL,
  `is_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`act_id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_logs
-- ----------------------------
INSERT INTO `tbl_logs` VALUES ('1', 'Login on 2020-01-16 07:02:27 PM', '1', '2020-01-16 07:02:27', null);
INSERT INTO `tbl_logs` VALUES ('2', 'Login on 2020-01-16 07:11:25 PM', '3', '2020-01-16 07:11:25', null);
INSERT INTO `tbl_logs` VALUES ('3', 'Added Patient \"asdads asdasd\" on 2020-01-16 07:13:44 PM', '3', '2020-01-16 07:13:44', null);
INSERT INTO `tbl_logs` VALUES ('4', 'Added Check up record of \"Asdasd, Asdads A\" on 2020-01-17 09:30:21 AM', '3', '2020-01-17 09:30:21', null);
INSERT INTO `tbl_logs` VALUES ('5', 'Edited Check up record of \"Asdasd, Asdads A\" on 2020-01-17 10:50:18 AM', '3', '2020-01-17 10:50:18', null);
INSERT INTO `tbl_logs` VALUES ('6', 'Login on 2020-02-08 04:33:11 PM', '1', '2020-02-08 04:33:11', null);
INSERT INTO `tbl_logs` VALUES ('7', 'Login on 2020-02-10 11:16:12 AM', '1', '2020-02-10 11:16:12', null);
INSERT INTO `tbl_logs` VALUES ('8', 'Login on 2020-02-10 11:16:51 AM', '1', '2020-02-10 11:16:51', null);
INSERT INTO `tbl_logs` VALUES ('9', 'Login on 2020-02-11 09:53:38 AM', '1', '2020-02-11 09:53:38', null);
INSERT INTO `tbl_logs` VALUES ('10', 'Added Account of \"Ronaldo rivera\" as  on 2020-02-11 02:31:05 PM', '1', '2020-02-11 02:31:05', null);
INSERT INTO `tbl_logs` VALUES ('11', 'Added Account of \"sdasd asdasd\" as  on 2020-02-11 02:46:48 PM', '1', '2020-02-11 02:46:48', null);
INSERT INTO `tbl_logs` VALUES ('12', 'Added Account of \"sdasd asdasd\" as  on 2020-02-11 02:52:04 PM', '1', '2020-02-11 02:52:04', null);
INSERT INTO `tbl_logs` VALUES ('13', 'Added Account of \"John Luis Barcelona\" as Record Officer on 2020-02-11 02:54:21 PM', '1', '2020-02-11 02:54:21', null);
INSERT INTO `tbl_logs` VALUES ('14', 'Added Account of \"John Luis Barcelona\" as Physician on 2020-02-11 03:01:22 PM', '1', '2020-02-11 03:01:22', null);
INSERT INTO `tbl_logs` VALUES ('15', 'Login on 2020-02-11 03:13:18 PM', '4', '2020-02-11 03:13:18', null);
INSERT INTO `tbl_logs` VALUES ('16', 'Login on 2020-02-11 07:04:26 PM', '1', '2020-02-11 07:04:26', null);
INSERT INTO `tbl_logs` VALUES ('17', 'Added Account of \"John Luis Barcelona\" as Record Officer on 2020-02-11 07:08:57 PM', '1', '2020-02-11 07:08:57', null);
INSERT INTO `tbl_logs` VALUES ('18', 'Login on 2020-02-12 11:10:23 AM', '1', '2020-02-12 11:10:23', null);
INSERT INTO `tbl_logs` VALUES ('19', 'Added Account of \"John Luis Barcelona\" as Record Officer on 2020-02-12 11:10:58 AM', '1', '2020-02-12 11:10:58', null);
INSERT INTO `tbl_logs` VALUES ('20', 'Login on 2020-02-12 02:15:25 PM', '1', '2020-02-12 02:15:25', null);
INSERT INTO `tbl_logs` VALUES ('21', 'Login on 2020-02-12 03:01:22 PM', '12', '2020-02-12 03:01:22', null);
INSERT INTO `tbl_logs` VALUES ('22', 'Login on 2020-02-12 03:05:04 PM', '3', '2020-02-12 03:05:04', null);
INSERT INTO `tbl_logs` VALUES ('23', 'Added Check up record of \"Asdasd, Asdads A\" on 2020-02-12 03:05:33 PM', '3', '2020-02-12 03:05:33', null);
INSERT INTO `tbl_logs` VALUES ('24', 'Login on 2020-02-12 03:07:04 PM', '2', '2020-02-12 03:07:04', null);
INSERT INTO `tbl_logs` VALUES ('25', 'Asdasd, Asdads A is Diagnosed \"Blood Pressure\" on 2020-02-12 03:09:04 PM', '2', '2020-02-12 03:09:04', null);
INSERT INTO `tbl_logs` VALUES ('26', 'Asdasd, Asdads A is Diagnosed \"Blood Pressure\" on 2020-02-12 03:09:04 PM', '2', '2020-02-12 03:09:04', null);
INSERT INTO `tbl_logs` VALUES ('27', 'Asdasd, Asdads A is Diagnosed \"Allergies\" on 2020-02-12 03:12:11 PM', '2', '2020-02-12 03:12:11', null);
INSERT INTO `tbl_logs` VALUES ('28', 'Login on 2020-02-14 03:03:18 PM', '3', '2020-02-14 03:03:18', null);
INSERT INTO `tbl_logs` VALUES ('29', 'Added Check up record of \"Asdasd, Asdads A\" on 2020-02-14 03:16:22 PM', '3', '2020-02-14 03:16:22', null);
INSERT INTO `tbl_logs` VALUES ('30', 'Login on 2020-02-14 03:20:00 PM', '3', '2020-02-14 03:20:00', null);
INSERT INTO `tbl_logs` VALUES ('31', 'Added Patient \"Juan Dela Cruz\" on 2020-02-14 03:20:39 PM', '3', '2020-02-14 03:20:39', null);
INSERT INTO `tbl_logs` VALUES ('32', 'Added Check up record of \"Dela Cruz, Juan A\" on 2020-02-14 03:21:24 PM', '3', '2020-02-14 03:21:24', null);
INSERT INTO `tbl_logs` VALUES ('33', 'Login on 2020-02-15 07:19:11 AM', '1', '2020-02-15 07:19:11', null);
INSERT INTO `tbl_logs` VALUES ('34', 'Login on 2020-02-15 07:19:58 AM', '2', '2020-02-15 07:19:58', null);
INSERT INTO `tbl_logs` VALUES ('35', 'Login on 2020-02-15 07:20:26 AM', '3', '2020-02-15 07:20:26', null);
INSERT INTO `tbl_logs` VALUES ('36', 'Login on 2020-02-15 09:41:44 AM', '12', '2020-02-15 09:41:44', null);
INSERT INTO `tbl_logs` VALUES ('37', 'Login on 2020-02-15 09:49:12 AM', '3', '2020-02-15 09:49:12', null);
INSERT INTO `tbl_logs` VALUES ('38', 'Deleted Patient \"Asdasd, Asdads A\" on 2020-02-15 10:56:27 AM', '3', '2020-02-15 10:56:27', null);
INSERT INTO `tbl_logs` VALUES ('39', 'Login on 2020-02-15 08:23:18 PM', '1', '2020-02-15 08:23:18', null);
INSERT INTO `tbl_logs` VALUES ('40', 'Login on 2020-02-15 08:25:36 PM', '2', '2020-02-15 08:25:36', null);
INSERT INTO `tbl_logs` VALUES ('41', 'Login on 2020-02-15 08:27:51 PM', '3', '2020-02-15 08:27:51', null);
INSERT INTO `tbl_logs` VALUES ('42', 'Added Check up record of \"Dela Cruz, Juan A\" on 2020-02-15 09:33:12 PM', '3', '2020-02-15 09:33:12', null);
INSERT INTO `tbl_logs` VALUES ('43', 'Login on 2020-02-15 09:48:02 PM', '3', '2020-02-15 09:48:02', null);
INSERT INTO `tbl_logs` VALUES ('44', 'Edited Check up record of \"Dela Cruz, Juan A\" on 2020-02-15 09:58:18 PM', '3', '2020-02-15 09:58:18', null);
INSERT INTO `tbl_logs` VALUES ('45', 'Edited Check up record of \"Dela Cruz, Juan A\" on 2020-02-15 10:00:43 PM', '3', '2020-02-15 10:00:43', null);
INSERT INTO `tbl_logs` VALUES ('46', 'Edited Check up record of \"Dela Cruz, Juan A\" on 2020-02-15 10:01:23 PM', '3', '2020-02-15 10:01:23', null);
INSERT INTO `tbl_logs` VALUES ('47', 'Login on 2020-02-16 07:37:24 AM', '4', '2020-02-16 07:37:24', null);
INSERT INTO `tbl_logs` VALUES ('48', 'Login on 2020-02-18 07:29:28 AM', '2', '2020-02-18 07:29:28', null);
INSERT INTO `tbl_logs` VALUES ('49', 'Login on 2020-02-18 07:45:03 AM', '3', '2020-02-18 07:45:03', null);
INSERT INTO `tbl_logs` VALUES ('50', 'Edited Patient \"Pedro Abas\" on 2020-02-18 07:45:56 AM', '3', '2020-02-18 07:45:56', null);
INSERT INTO `tbl_logs` VALUES ('51', 'Added Patient \"Juan Dela Cruz\" on 2020-02-18 07:46:25 AM', '3', '2020-02-18 07:46:25', null);
INSERT INTO `tbl_logs` VALUES ('52', 'Login on 2020-02-19 10:19:41 AM', '2', '2020-02-19 10:19:41', null);
INSERT INTO `tbl_logs` VALUES ('53', 'Login on 2020-02-19 10:21:03 AM', '4', '2020-02-19 10:21:03', null);
INSERT INTO `tbl_logs` VALUES ('54', 'Login on 2020-02-19 10:25:19 AM', '3', '2020-02-19 10:25:19', null);
INSERT INTO `tbl_logs` VALUES ('55', 'Login on 2020-02-19 10:35:44 AM', '2', '2020-02-19 10:35:44', null);
INSERT INTO `tbl_logs` VALUES ('56', 'Login on 2020-02-19 10:36:24 AM', '3', '2020-02-19 10:36:24', null);
INSERT INTO `tbl_logs` VALUES ('57', 'Login on 2020-02-19 10:38:51 AM', '2', '2020-02-19 10:38:51', null);
INSERT INTO `tbl_logs` VALUES ('58', 'Login on 2020-02-21 04:41:27 PM', '2', '2020-02-21 04:41:27', null);
INSERT INTO `tbl_logs` VALUES ('59', 'Login on 2020-02-21 04:43:53 PM', '3', '2020-02-21 04:43:53', null);
INSERT INTO `tbl_logs` VALUES ('60', 'Login on 2020-02-23 10:21:11 AM', '2', '2020-02-23 10:21:11', null);
INSERT INTO `tbl_logs` VALUES ('61', 'Abas, Pedro A is Diagnosed \"Allergies\" on 2020-02-23 11:25:40 AM', '2', '2020-02-23 11:25:40', null);
INSERT INTO `tbl_logs` VALUES ('62', 'Login on 2020-02-23 07:41:31 PM', '3', '2020-02-23 07:41:31', null);
INSERT INTO `tbl_logs` VALUES ('63', 'Added Check up record of \"Abas, Pedro A\" on 2020-02-23 07:42:50 PM', '3', '2020-02-23 07:42:50', null);
INSERT INTO `tbl_logs` VALUES ('64', 'Abas, Pedro A is Diagnosed \"Allergies\" on 2020-02-23 08:06:15 PM', '2', '2020-02-23 08:06:15', null);
INSERT INTO `tbl_logs` VALUES ('65', 'Abas, Pedro A is Diagnosed \"Allergies\" on 2020-02-23 08:11:31 PM', '2', '2020-02-23 08:11:31', null);
INSERT INTO `tbl_logs` VALUES ('66', 'Abas, Pedro A is Diagnosed \"Allergies\" on 2020-02-23 08:20:00 PM', '2', '2020-02-23 08:20:00', null);
INSERT INTO `tbl_logs` VALUES ('67', 'Abas, Pedro A is Diagnosed \"Allergies\" on 2020-02-23 08:21:16 PM', '2', '2020-02-23 08:21:16', null);
INSERT INTO `tbl_logs` VALUES ('68', 'Abas, Pedro A is Diagnosed \"Allergies\" on 2020-02-23 08:39:38 PM', '2', '2020-02-23 08:39:38', null);
INSERT INTO `tbl_logs` VALUES ('69', 'Login on 2020-02-23 10:46:24 PM', '1', '2020-02-23 10:46:24', null);
INSERT INTO `tbl_logs` VALUES ('70', 'Added Patient \"Juan Dela Cruz\" on 2020-02-23 11:02:54 PM', '1', '2020-02-23 11:02:54', null);
INSERT INTO `tbl_logs` VALUES ('71', 'Login on 2020-02-23 11:07:29 PM', '4', '2020-02-23 11:07:29', null);
INSERT INTO `tbl_logs` VALUES ('72', 'Abas, Pedro A is Diagnosed \"Allergies\" on 2020-02-23 11:08:42 PM', '2', '2020-02-23 11:08:42', null);
INSERT INTO `tbl_logs` VALUES ('73', 'Login on 2020-02-24 08:49:08 AM', '2', '2020-02-24 08:49:08', null);
INSERT INTO `tbl_logs` VALUES ('74', 'Login on 2020-02-25 08:07:20 AM', '3', '2020-02-25 08:07:20', null);
INSERT INTO `tbl_logs` VALUES ('75', 'Login on 2020-02-25 11:14:54 AM', '12', '2020-02-25 11:14:54', null);
INSERT INTO `tbl_logs` VALUES ('76', 'Login on 2020-02-26 09:39:03 AM', '12', '2020-02-26 09:39:03', null);
INSERT INTO `tbl_logs` VALUES ('77', 'Login on 2020-02-26 09:53:09 AM', '2', '2020-02-26 09:53:09', null);
INSERT INTO `tbl_logs` VALUES ('78', 'Login on 2020-02-26 10:02:32 AM', '4', '2020-02-26 10:02:32', null);
INSERT INTO `tbl_logs` VALUES ('79', 'Login on 2020-02-26 10:50:20 AM', '1', '2020-02-26 10:50:20', null);
INSERT INTO `tbl_logs` VALUES ('80', 'Login on 2020-02-26 10:56:23 AM', '2', '2020-02-26 10:56:23', null);
INSERT INTO `tbl_logs` VALUES ('81', 'Login on 2020-02-26 01:26:53 PM', '2', '2020-02-26 01:26:53', null);
INSERT INTO `tbl_logs` VALUES ('82', 'Login on 2020-02-26 01:27:35 PM', '3', '2020-02-26 01:27:35', null);
INSERT INTO `tbl_logs` VALUES ('83', 'Deleted Patient \"Asdasd, Asdads A\" on 2020-02-26 01:27:45 PM', '3', '2020-02-26 01:27:45', null);
INSERT INTO `tbl_logs` VALUES ('84', 'Added Check up record of \"Abas, Pedro A\" on 2020-02-26 01:31:36 PM', '3', '2020-02-26 01:31:36', null);
INSERT INTO `tbl_logs` VALUES ('85', 'Abas, Pedro A is Diagnosed \"Allergies\" on 2020-02-26 01:34:04 PM', '2', '2020-02-26 01:34:04', null);
INSERT INTO `tbl_logs` VALUES ('86', 'Login on 2020-02-26 01:50:04 PM', '4', '2020-02-26 01:50:04', null);
INSERT INTO `tbl_logs` VALUES ('87', 'Login on 2020-02-26 01:50:53 PM', '2', '2020-02-26 01:50:53', null);
INSERT INTO `tbl_logs` VALUES ('88', 'Login on 2020-02-26 02:14:25 PM', '3', '2020-02-26 02:14:25', null);
INSERT INTO `tbl_logs` VALUES ('89', 'Added Check up record of \"Dela Cruz, Juan D\" on 2020-02-26 02:16:08 PM', '3', '2020-02-26 02:16:08', null);
INSERT INTO `tbl_logs` VALUES ('90', 'Dela Cruz, Juan D is Diagnosed \"Asthma\" on 2020-02-26 02:19:12 PM', '2', '2020-02-26 02:19:12', null);
INSERT INTO `tbl_logs` VALUES ('91', 'Dela Cruz, Juan D is Diagnosed \"Asthma\" on 2020-02-26 02:21:12 PM', '2', '2020-02-26 02:21:12', null);
INSERT INTO `tbl_logs` VALUES ('92', 'Dela Cruz, Juan D is Diagnosed \"Asthma\" on 2020-02-26 02:27:25 PM', '2', '2020-02-26 02:27:25', null);
INSERT INTO `tbl_logs` VALUES ('93', 'Dela Cruz, Juan D is Diagnosed \"Asthma\" on 2020-02-26 03:26:21 PM', '2', '2020-02-26 03:26:21', null);
INSERT INTO `tbl_logs` VALUES ('94', 'Dela Cruz, Juan D is Diagnosed \"Asthma\" on 2020-02-26 03:29:27 PM', '2', '2020-02-26 03:29:27', null);
INSERT INTO `tbl_logs` VALUES ('95', 'Dela Cruz, Juan D is Diagnosed \"Asthma\" on 2020-02-26 03:34:27 PM', '2', '2020-02-26 03:34:27', null);
INSERT INTO `tbl_logs` VALUES ('96', 'Dela Cruz, Juan D is Diagnosed \"Asthma\" on 2020-02-26 03:34:53 PM', '2', '2020-02-26 03:34:53', null);
INSERT INTO `tbl_logs` VALUES ('97', 'Dela Cruz, Juan D is Diagnosed \"Asthma\" on 2020-02-26 03:36:18 PM', '2', '2020-02-26 03:36:18', null);
INSERT INTO `tbl_logs` VALUES ('98', 'Dela Cruz, Juan D is Diagnosed \"Asthma\" on 2020-02-26 03:36:19 PM', '2', '2020-02-26 03:36:19', null);
INSERT INTO `tbl_logs` VALUES ('99', 'Dela Cruz, Juan D is Diagnosed \"Asthma\" on 2020-02-26 03:38:32 PM', '2', '2020-02-26 03:38:32', null);
INSERT INTO `tbl_logs` VALUES ('100', 'Dela Cruz, Juan D is Diagnosed \"Asthma\" on 2020-02-26 03:40:45 PM', '2', '2020-02-26 03:40:45', null);
INSERT INTO `tbl_logs` VALUES ('101', 'Dela Cruz, Juan D is Diagnosed \"Asthma\" on 2020-02-26 03:41:52 PM', '2', '2020-02-26 03:41:52', null);
INSERT INTO `tbl_logs` VALUES ('102', 'Dela Cruz, Juan D is Diagnosed \"Asthma\" on 2020-02-26 03:41:53 PM', '2', '2020-02-26 03:41:53', null);
INSERT INTO `tbl_logs` VALUES ('103', 'Dela Cruz, Juan D is Diagnosed \"Asthma\" on 2020-02-26 03:42:53 PM', '2', '2020-02-26 03:42:53', null);
INSERT INTO `tbl_logs` VALUES ('104', 'Dela Cruz, Juan D is Diagnosed \"Asthma\" on 2020-02-26 03:43:05 PM', '2', '2020-02-26 03:43:05', null);
INSERT INTO `tbl_logs` VALUES ('105', 'Dela Cruz, Juan D is Diagnosed \"Asthma\" on 2020-02-26 03:45:54 PM', '2', '2020-02-26 03:45:54', null);
INSERT INTO `tbl_logs` VALUES ('106', 'Dela Cruz, Juan D is Diagnosed \"Asthma\" on 2020-02-26 03:47:54 PM', '2', '2020-02-26 03:47:54', null);
INSERT INTO `tbl_logs` VALUES ('107', 'Dela Cruz, Juan D is Diagnosed \"Asthma\" on 2020-02-26 03:49:27 PM', '2', '2020-02-26 03:49:27', null);
INSERT INTO `tbl_logs` VALUES ('108', 'Login on 2020-03-14 02:12:56 PM', '1', '2020-03-14 02:12:56', null);
INSERT INTO `tbl_logs` VALUES ('109', 'Added Account of \"asdasd asdasd\" as Physician on 2020-03-14 02:17:18 PM', '1', '2020-03-14 02:17:18', null);
INSERT INTO `tbl_logs` VALUES ('110', 'Login on 2020-05-04 03:21:34 PM', '1', '2020-05-04 03:21:34', null);
INSERT INTO `tbl_logs` VALUES ('111', 'Added Account of \"asd asd\" as Record Officer on 2020-05-04 03:47:46 PM', '1', '2020-05-04 03:47:46', null);
INSERT INTO `tbl_logs` VALUES ('112', 'Login on 2020-05-16 05:38:13 PM', '1', '2020-05-16 05:38:13', null);
INSERT INTO `tbl_logs` VALUES ('113', 'Login on 2020-05-16 05:41:05 PM', '2', '2020-05-16 05:41:05', null);
INSERT INTO `tbl_logs` VALUES ('114', 'Abas, Pedro A is Diagnosed \"Allergies\" on 2020-05-16 06:13:38 PM', '2', '2020-05-16 06:13:38', null);

-- ----------------------------
-- Table structure for tbl_medical
-- ----------------------------
DROP TABLE IF EXISTS `tbl_medical`;
CREATE TABLE `tbl_medical` (
  `medical_id` int(255) NOT NULL AUTO_INCREMENT,
  `patient_id` int(255) DEFAULT NULL,
  `doctor_name` int(255) DEFAULT NULL,
  `in_charge` int(255) DEFAULT NULL,
  `height` varchar(255) DEFAULT NULL,
  `weight` varchar(255) DEFAULT NULL,
  `bp` varchar(255) DEFAULT NULL,
  `symptoms` longtext,
  `urine_analysis` varchar(255) DEFAULT NULL,
  `fasting_blood_sugar` varchar(255) DEFAULT NULL,
  `illness_history` varchar(255) DEFAULT NULL,
  `medical_history` varchar(255) DEFAULT NULL,
  `temp` varchar(255) DEFAULT NULL,
  `diagnoses_result` varchar(255) DEFAULT NULL,
  `action_taken` longtext,
  `pulse_rate` varchar(255) DEFAULT NULL,
  `respiratory_rate` varchar(255) DEFAULT NULL,
  `cbc` varchar(255) DEFAULT NULL,
  `fecalysis` varchar(255) DEFAULT NULL,
  `gram_staining` varchar(255) DEFAULT NULL,
  `hepa_b` varchar(255) DEFAULT NULL,
  `syphilis_screening` varchar(255) DEFAULT NULL,
  `bun` varchar(255) DEFAULT NULL,
  `ca` varchar(255) DEFAULT NULL,
  `ci` varchar(255) DEFAULT NULL,
  `chol` varchar(255) DEFAULT NULL,
  `creat` varchar(255) DEFAULT NULL,
  `potassium` varchar(255) DEFAULT NULL,
  `sodium` varchar(255) DEFAULT NULL,
  `tsh` varchar(255) DEFAULT NULL,
  `urea` varchar(255) DEFAULT NULL,
  `date_entry` datetime DEFAULT NULL,
  `is_finish` int(1) DEFAULT '0',
  `is_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`medical_id`),
  KEY `in_charge` (`in_charge`),
  KEY `doctor` (`doctor_name`),
  KEY `patient_name` (`patient_id`),
  CONSTRAINT `doctor` FOREIGN KEY (`doctor_name`) REFERENCES `tbl_account` (`account_id`) ON UPDATE CASCADE,
  CONSTRAINT `in_charge` FOREIGN KEY (`in_charge`) REFERENCES `tbl_account` (`account_id`) ON UPDATE CASCADE,
  CONSTRAINT `patient_name` FOREIGN KEY (`patient_id`) REFERENCES `tbl_patient` (`patient_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_medical
-- ----------------------------
INSERT INTO `tbl_medical` VALUES ('4', '2', '2', '3', '51234', '61234', '11234567', 'Itching,Pain,Chest pain.', '81234', '151234', '201234', '211234', '4123423', 'Allergies', 'asd', '21234234', '31234', '71234', '9123423', 'Negative', 'Negative', 'Negative', '101234', '111234', '121234', '131234', '141234', '161234', '171234', '181234', '191234', '2020-05-16 22:01:23', '1', null);
INSERT INTO `tbl_medical` VALUES ('5', '2', '2', '3', '150', '140', '150/110', 'Chills,Itching', '', '', 'Fever', 'Paracetamol', '35', 'Allergies', 'Maligo ka araw-araw', '25/50', '55', '', '', 'Negative', 'Negative', 'Negative', '', '', '', '', '', '', '', '', '', '2020-02-23 19:42:50', '2', null);
INSERT INTO `tbl_medical` VALUES ('6', '2', '2', '3', '170', '170', '150/80', 'Itching', '12', '', 'Fever', 'Paracetamol', '37', 'Allergies', 'Maligo ka araw-araw.', '25/25', '24/24', '12', '12', 'Negative', 'Negative', 'Negative', '', '', '', '', '', '', '', '', '', '2020-02-26 13:31:36', '2', null);

-- ----------------------------
-- Table structure for tbl_medical_med_reco
-- ----------------------------
DROP TABLE IF EXISTS `tbl_medical_med_reco`;
CREATE TABLE `tbl_medical_med_reco` (
  `med_id` int(255) NOT NULL AUTO_INCREMENT,
  `medical_id` int(255) DEFAULT NULL,
  `medicine_id` int(255) DEFAULT NULL,
  `qty_reco` int(55) DEFAULT NULL,
  `dosage` varchar(255) DEFAULT NULL,
  `intake_schedule` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`med_id`),
  KEY `medical` (`medical_id`),
  KEY `medicine` (`medicine_id`),
  CONSTRAINT `medical` FOREIGN KEY (`medical_id`) REFERENCES `tbl_medical` (`medical_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_medical_med_reco
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_medicine
-- ----------------------------
DROP TABLE IF EXISTS `tbl_medicine`;
CREATE TABLE `tbl_medicine` (
  `medicine_id` int(255) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(255) DEFAULT NULL,
  `generic_name` varchar(255) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `dosage` varchar(255) DEFAULT NULL,
  `form_type` varchar(255) DEFAULT NULL,
  `supplier` varchar(255) DEFAULT NULL,
  `illness` longtext,
  `date_entry` datetime DEFAULT NULL,
  `is_delete` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`medicine_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_medicine
-- ----------------------------
INSERT INTO `tbl_medicine` VALUES ('7', 'Bio', 'Genic', '8', '12 ml', 'box', 'aabbcc', 'Allergies,Asthma', '2019-10-24 03:46:26', null);
INSERT INTO `tbl_medicine` VALUES ('8', 'Ethyl Alcohol', 'Alcohol', '98', '12 ml', 'box', 'Fernando Eduarte', 'Allergies,Asthma', '2019-10-29 12:50:30', null);
INSERT INTO `tbl_medicine` VALUES ('9', 'Neoblock', 'Neoblock 50mg', '100', '12 ml', 'box', 'DOH', 'High blood pressure', '2019-12-17 09:08:23', null);
INSERT INTO `tbl_medicine` VALUES ('10', 'Amlodipin', 'Amlodipine 5mg', '100', '12 ml', 'box', 'DOH', 'High blood pressure', '2019-12-17 09:09:17', null);
INSERT INTO `tbl_medicine` VALUES ('11', 'Amlodipine', 'Amlodipine 10mg', '100', '12 ml', 'box', 'DOH', 'High blood pressure', '2019-12-17 09:09:55', null);
INSERT INTO `tbl_medicine` VALUES ('12', 'Zyrtec', 'cetirizine hydrochloride', '500', '12 ml', 'box', 'DOH', 'Seasonal Allergic Rhinitis,Perennial Allergic Rhinitis,Chronic Urticaria', '2019-12-20 08:56:07', null);
INSERT INTO `tbl_medicine` VALUES ('13', 'Ventolin', 'albuterol sulfate', '500', '12 ml', 'box', 'DOH', 'Bronchospasm', '2019-12-20 08:59:44', null);
INSERT INTO `tbl_medicine` VALUES ('14', 'Maxair Autohaler', 'pirbuterol acetate', '500', '12 ml', 'box', 'DOH', 'Asthma', '2019-12-20 09:02:36', null);
INSERT INTO `tbl_medicine` VALUES ('15', 'Serevent Diskus', 'salmeterol xinofoate', '396', '12 ml', 'box', 'DOH', 'Asthma,Exercised-induced Bronchospasm,COPD/Emphysema', '2019-12-20 09:04:45', null);
INSERT INTO `tbl_medicine` VALUES ('16', 'Brethine', 'terbutaline sulfate', '500', '12 ml', 'box', 'DOH', 'Bronchospasm', '2019-12-20 09:11:01', null);
INSERT INTO `tbl_medicine` VALUES ('17', 'Fungizone', 'amphotericin B desoxycholate', '200', '12 ml', 'box', 'DOH', 'hystoplasmosis,coccidioidomycosis,blastomycosis,cryptococcosis,disseminated candidiasis,aspergillosis,phycomycisis,zygomycosis,meningitis', '2019-12-20 09:15:37', null);
INSERT INTO `tbl_medicine` VALUES ('18', 'Aralen Phosphate', 'chloroquine phosphate', '300', '12 ml', 'box', 'DOH', 'acute malarial attacks,malaria', '2019-12-20 09:19:02', null);
INSERT INTO `tbl_medicine` VALUES ('19', 'Plaquenil Sulfate', 'hydroxychloroquine sulfate', '250', '12 ml', 'box', 'DOH', 'suppressive prevention of malarial attacks, malaria', '2019-12-20 09:20:30', null);
INSERT INTO `tbl_medicine` VALUES ('20', 'Seromycin', 'cycloserine', '300', '12 ml', 'box', 'DOH', 'Adjunctive treatment for pulmunary or extrapulmonary tuberculosis,TB,Tuberculosis', '2019-12-20 09:22:44', null);
INSERT INTO `tbl_medicine` VALUES ('21', 'Myambutol', 'ethambutol hydrochloride', '560', '12 ml', 'box', 'DOH', 'Pulmunary tuberculosis,TB, Tuberculosis', '2019-12-20 09:24:18', null);
INSERT INTO `tbl_medicine` VALUES ('22', 'Rifadin', 'rifampin (rifampicin)', '560', '12 ml', 'box', 'DOH', 'Pulmunary Tuberculosis,TB, Tuberculosis', '2019-12-20 09:26:12', null);
INSERT INTO `tbl_medicine` VALUES ('23', 'Priftin', 'rifapentine', '360', '12 ml', 'box', 'DOH', 'Pulmonary Tuberculosis,TB,Tuberculosis', '2019-12-20 09:27:47', null);
INSERT INTO `tbl_medicine` VALUES ('24', 'Amoxyl', 'amoxicillin trihydrate', '800', '12 ml', 'box', 'LGU', 'infections of ear nose throat,genitourinary tract,infections of the lower respiratory tract,endocarditis', '2019-12-20 09:31:47', null);
INSERT INTO `tbl_medicine` VALUES ('25', 'Novo Ampicillin', 'ampicillin', '400', '12 ml', 'box', 'LGU', 'respiratory tract,skin or skin-structure infections,GI infections,UTI,bacterial meningitis,septicemia,uncomplicated gonorrhea,endocarditis in patients having dental GI and GU procedures', '2019-12-20 09:34:38', null);
INSERT INTO `tbl_medicine` VALUES ('26', 'Ziagen', 'abacavir sulfate', '200', '12 ml', 'box', 'DOH', 'HIV-1 Infection', '2019-12-21 07:41:19', null);
INSERT INTO `tbl_medicine` VALUES ('27', 'Hepsera', 'adefovir dipivoxil', '179', '12 ml', 'box', 'DOH', 'Chronic Hepatitis B infection, Hepa-B,Hepatitis-B', '2019-12-21 07:43:11', null);
INSERT INTO `tbl_medicine` VALUES ('28', 'Tenormin', 'atenolol', '1000', '12 ml', 'box', 'LGU', 'Hypertension, Agina Pectoris, Migraine prophylaxis', '2019-12-21 07:50:20', null);
INSERT INTO `tbl_medicine` VALUES ('29', 'Lotensin', 'benazepril hydrochloride', '1000', '12 ml', 'box', 'LGU', 'Hypertension', '2019-12-21 07:51:34', null);
INSERT INTO `tbl_medicine` VALUES ('30', 'Atacand', 'candesartan cilexetil', '2000', '12 ml', 'box', 'LGU', 'Hypertension,Heart Failure', '2019-12-21 07:52:46', null);
INSERT INTO `tbl_medicine` VALUES ('31', 'Acenorm', 'captopril', '1000', '12 ml', 'box', 'LGU', 'Hypertension, Diabetic nephropathy, Heart failure, Left ventricular dysfunction after acute MI', '2019-12-21 07:54:32', null);
INSERT INTO `tbl_medicine` VALUES ('32', 'Coreg', 'carvedilol', '1000', '12 ml', 'box', 'LGU', 'Hypertension,Left ventricular dysfunction after MI, Mild to severe heart faiulure, Angina pectoris, Idiopathic cardiomyopathy', '2019-12-21 07:56:43', null);
INSERT INTO `tbl_medicine` VALUES ('33', 'Catapres - TSS', 'clonidine', '5000', '12 ml', 'box', 'LGU/DOH', 'Essential and renal hypertension, Pheochromocytoma diagnosis, Migraine prophylaxis, Dysmenorrhea, Vasamotor symptoms of menopause, Opiate dependence, Alcohol dependence, Smoking cessation, Attention deficit hyperactivity dosorder, Hypertension', '2019-12-21 08:01:24', null);
INSERT INTO `tbl_medicine` VALUES ('34', 'Catapres', 'clonidine hydrochloride', '4000', '12 ml', 'box', 'LGU/DOH', 'Essential and renal hypertension, Pheochromocytoma diagnosis, Migraine prophylaxis, Dysmenorrhea, Vasamotor symptoms of menopause, Opiate dependence, Alcohol dependence, Smoking cessation, Attention deficit hyperactivity dosorder, Hypertension', '2019-12-21 08:03:27', null);
INSERT INTO `tbl_medicine` VALUES ('35', 'Cardura', 'doxazosin mesylate', '3000', '12 ml', 'box', 'LGU', 'Essential Hypertension, Hypertension,BPH', '2019-12-21 08:04:38', null);
INSERT INTO `tbl_medicine` VALUES ('36', 'Vasotec', 'enalapril maleate', '3000', '12 ml', 'box', 'LGU', 'Hypertension, Symptomatic heart failure, Asymptomatic left ventricular dysfunction', '2019-12-21 08:06:04', null);
INSERT INTO `tbl_medicine` VALUES ('37', 'Inspra', 'eplerenone', '2000', '12 ml', 'box', 'LGU', 'Hypertension, Heart failure after an MI', '2019-12-21 08:07:08', null);
INSERT INTO `tbl_medicine` VALUES ('38', 'Teveten', 'eprosartan mesylate', '1000', '12 ml', 'box', 'LGU', 'Hypertension', '2019-12-21 08:07:50', null);
INSERT INTO `tbl_medicine` VALUES ('39', 'Agon SR', 'felodipine', '1000', '12 ml', 'box', 'LGU', 'Hypertension', '2019-12-21 08:08:33', null);
INSERT INTO `tbl_medicine` VALUES ('40', 'Monopril', 'fosinopril sodium', '1000', '12 ml', 'box', 'DOH', 'Hypertension, Heart failure', '2019-12-21 08:09:20', null);
INSERT INTO `tbl_medicine` VALUES ('41', 'Tenex', 'guanfacine hydrochloride', '1000', '12 ml', 'box', 'LGU', 'Hypertension, Relieve symptoms of heroin withdrawal, Migraine', '2019-12-21 08:10:48', null);
INSERT INTO `tbl_medicine` VALUES ('42', 'Alphapress', 'hydralazine hydrochloride', '200', '12 ml', 'box', 'LGU', 'Hypertension, Hypertensive crisis, Preeclampsia,Eclampsia, Heart failure', '2019-12-21 08:12:42', null);
INSERT INTO `tbl_medicine` VALUES ('43', 'Avapro', 'irbesartan', '100', '12 ml', 'box', 'DOH', 'Hypertension, Nephropathy in patients with type 2 diabetes', '2019-12-21 08:14:14', null);
INSERT INTO `tbl_medicine` VALUES ('44', 'Normodyne', 'lebatalol hydrochloride', '100', '12 ml', 'box', 'LGU', 'Hypertension, Severe hypertension, Hypertensive emergencies', '2019-12-21 08:15:29', null);
INSERT INTO `tbl_medicine` VALUES ('45', 'Prinivil', 'lisinopril', '100', '12 ml', 'box', 'DOH', 'Hypertension, Adjunct treatment (with diuretics and cardiac glycosides )for heart failure, Hemodynamically stable patients within 24 hours of acute MI to improve survival, Hypertension in children', '2019-12-21 08:18:28', null);
INSERT INTO `tbl_medicine` VALUES ('46', 'Cozaar', 'lozartan potassium', '5000', '12 ml', 'box', 'LGU/DOH', 'Hypertension, Nephropathy in type 2 diabetic patients, reduce risk of stroke on patients with hypertension and left ventricular hypertrophy', '2019-12-21 08:26:33', null);
INSERT INTO `tbl_medicine` VALUES ('47', 'Toprol-XL', 'metoprolol succinate', '4000', '12 ml', 'box', 'LGU', 'Hypertension, Early intervention in acute MI, Angina pectoris,Stable symptomatic heart failure resulting from ischema,Stable symptomatic heart failure resulting from hypertension, Stable symptomatic heart failure resulting to cardiomyopathy', '2019-12-21 08:30:50', null);
INSERT INTO `tbl_medicine` VALUES ('48', 'Apo-Metoprolol', 'metoprolol tartrate', '5000', '12 ml', 'box', 'LGU', 'Hypertension, Early intervention in acute MI, Angina pectoris,Stable symptomatic heart failure resulting from ischema,Stable symptomatic heart failure resulting from hypertension, Stable symptomatic heart failure resulting to cardiomyopathy', '2019-12-21 08:32:52', null);

-- ----------------------------
-- Table structure for tbl_out_medicine
-- ----------------------------
DROP TABLE IF EXISTS `tbl_out_medicine`;
CREATE TABLE `tbl_out_medicine` (
  `outmed_id` int(255) NOT NULL AUTO_INCREMENT,
  `medical_id` int(255) DEFAULT NULL,
  `medicine_brand` varchar(255) DEFAULT NULL,
  `medicine_generic_name` varchar(255) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `dossage` varchar(255) DEFAULT NULL,
  `intake_schedule` varchar(255) DEFAULT NULL,
  `date_entry` datetime DEFAULT NULL,
  PRIMARY KEY (`outmed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_out_medicine
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_patient
-- ----------------------------
DROP TABLE IF EXISTS `tbl_patient`;
CREATE TABLE `tbl_patient` (
  `patient_id` int(255) NOT NULL AUTO_INCREMENT,
  `fn` varchar(255) DEFAULT NULL,
  `mn` varchar(255) DEFAULT NULL,
  `ln` varchar(255) DEFAULT NULL,
  `bdate` datetime(6) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `municipality` varchar(255) DEFAULT NULL,
  `barangay` varchar(255) DEFAULT NULL,
  `is_delete` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`patient_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_patient
-- ----------------------------
INSERT INTO `tbl_patient` VALUES ('1', 'asdads', 'asdasd', 'asdasd', '2020-01-01 00:00:00.000000', 'Male', '2', 'CAGAYAN', 'ALCALA', 'AFUSING BATO', '2020-02-26 01:27:45.000000');
INSERT INTO `tbl_patient` VALUES ('2', 'Pedro', 'A.', 'Abas', '1997-01-30 00:00:00.000000', 'Male', '1', 'ILOCOS NORTE', 'ADAMS', 'ADAMS (POB.)', null);
INSERT INTO `tbl_patient` VALUES ('3', 'Juan', 'D.', 'Dela Cruz', '2003-11-28 00:00:00.000000', 'Male', '3', 'BATAAN', 'BAGAC', 'BAGUMBAYAN (POB.)', null);
