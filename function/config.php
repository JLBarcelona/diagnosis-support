<?php 
// Database Connection
$server = "localhost";		//Server name
$username = "root";			//database Username
$db_password = "";			//database Password


try {
	$ATTR = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC];
	// quotation always double ""
	$con = new PDO("mysql:host=$server;dbname=ddss", $username, $db_password, $ATTR);
	// echo 'connected';  //Display text if connected
} catch (PDOException $e) {
	 echo "Connection failed: " . $e->getMessage();
}

 ?>