<?php 


$id = (isset($_SESSION['account_id'])) ? $_SESSION['account_id'] : '';

$auth = get_session($con,$id);

$able = '';

if (isset($_SESSION['account_id'])) {
	$able = ($auth['user_type'] == 2)? 'none' : ''; 
}

function list_form_type(){
  $str =' tablet, capsule, injection, suspension, aerosol, vial, powder for injection, oral suspension, tablets (chewable), suspension for injection, powder for oral suspension, oral suspension, oral solution';

  $array = explode(',', $str);
  
  for ($i=0; $i < count($array); $i++) { 
    echo '<option>'.trim($array[$i]).'</option>';
  }

}

function get_percentage($total_count,$points){
   $total = 100 / $total_count;
   $result = $total * $points;

   return round($result, 2);
}


function user_position(){
	$arr = array(0 => 'Admin',
			  1 => 'Physician', 
			  2 => 'Nurse',
			  3 => 'Pharmacist',
			  4 => 'Record Officer'
			);

	return $arr;
}

function get_month_option(){
	for ($i=1; $i <= 12; $i++) { 
		$date = '2019-'.$i.'-1';

		$month = date('F',strtotime($date));
		echo '<option values="'.$month.'">'.$month.'</option>';
	}
}


function get_year_option(){
	$date_today = date('Y');

	$total_ = $date_today + 1;

	for ($i=1; $i <= 30; $i++) { 
		$date = ($total_ - $i).'-1-1';

		$year = date('Y',strtotime($date));
		echo '<option values="'.$year.'">'.$year.'</option>';
	}
}

function to_list_symptoms($con,$id){
	$data = array('id' => $id);
	$sql = "SELECT * from tbl_illness_symptoms where illness_id=:id";
	$result = fetch_record($con,$data,$sql);
	while ($row = $result->fetch()) {
		echo '<span class="h5 ml-1"><span class="badge badge-primary">'.$row['symptoms_name'].'</span></span>';
	}
}

function option_illness($con){
	$data = array();
	$sql = "SELECT * from tbl_illness where is_delete is null";

	$result = fetch_record($con,$data,$sql);

	while ($row = $result->fetch()) {
		echo '<option value="'.$row['illness_name'].'">';
	}
}

function option_symptoms($con){
	$data = array();
	$sql = "SELECT * from tbl_illness_symptoms where is_delete is null group by symptoms_name";

	$result = fetch_record($con,$data,$sql);

	while ($row = $result->fetch()) {
		echo '<option value="'. $row['symptoms_name'].'">';
	}
}

function count_dashboard($con,$type){
	$data = array('type' => $type);
	$sql = "SELECT count(account_id) from tbl_account where user_type=:type and is_delete is null";
	return count_record($con,$data,$sql,'account_id');
}


function count_all($con,$tbl,$arrs,$count){
	$data = array();
	$sql = "SELECT count(".$count.") from ".$tbl." ".$arrs."";
	return count_record($con,$data,$sql,$count);
}


function sum_all($con,$tbl,$arrs,$count){
	$data = array();
	$sql = "SELECT sum(".$count.") from ".$tbl." ".$arrs."";
	return sum_record($con,$data,$sql,$count);
}

function get_age($bdates){
	$bdate = date('Y',strtotime(date('Y-m-d'))) - date('Y',strtotime($bdates));

	$months = date('m',strtotime(date('Y-m-d'))) - date('m',strtotime($bdates));


	if ($bdate > 0) {
		return $bdate.' Yrs old';
	}else{
		return $months.' Months old';
	}
}

function get_medicine_qty($con,$id_med){
	$arr_check = array('med_id' => $id_med);
	$check_sql = "SELECT * from tbl_medicine where medicine_id=:med_id";
	$check_result = fetch_record($con,$arr_check,$check_sql);
	$rr_check = $check_result->fetch();

	return $rr_check['qty'];
}


function save($con,$data,$sql){
	// sample
	// INSERT INTO tbl_employee (firstname,lastname) VALUES(:fn,:ln)
	$prep = $con->prepare($sql);
	$prep->execute($data);

	if ($prep) {
		return 1;
	}
}



function logs($con,$message,$account_id,$date_entry){
	$data = array('message' => $message,
				  'account_id' => $account_id,
				  'date_entry' => $date_entry
				 );
	$sql = "INSERT INTO tbl_logs(message,account_id,date_entry) VALUES(:message,:account_id,:date_entry)";

	if (save($con,$data,$sql) > 0) {
		return 1;
	}
}

function avatar($avatar,$gender,$user_type){
	if (!empty($avatar)) {
		return $avatar;
	}else{
		if ($user_type == 3) {
			return 'img/'.$gender.'.png';
		}else if($user_type == 2){
			return 'img/'.$gender.'P.png';
		}else{
			return 'img/admin.png';
		}
	}
}



function get_max_id($con){
	$data = array();
	$sql ="SELECT max(account_id) from tbl_account";

	return max_record($con,$data,$sql,'account_id') + 1;
}

// Get count record
function count_record($con,$data,$sql,$count){
    $result = fetch_record($con,$data,$sql);
    $row = $result->fetch();

    if ($row['count('.$count.')'] > 0) {
    	return $row['count('.$count.')'];
    }else{
    	return 0;
    }
}


function sum_record($con,$data,$sql,$count){
    $result = fetch_record($con,$data,$sql);
    $row = $result->fetch();

    if ($row['sum('.$count.')'] > 0) {
    	return $row['sum('.$count.')'];
    }else{
    	return 0;
    }
}


// get  max record
function max_record($con,$data,$sql,$count){
    $result = fetch_record($con,$data,$sql);
    $row = $result->fetch();

    if ($row['max('.$count.')'] > 0) {
    	return $row['max('.$count.')'];
    }else{
    	return 0;
    }
}


function get_session($con,$id){
	$data = array('id' => $id);
    $sql = "SELECT * from tbl_account where account_id = :id";
    $result = fetch_record($con,$data,$sql);
    $row = $result->fetch();

    return $row;
}

function fetch_record($con,$data,$sql){
	// sample
	// INSERT INTO tbl_employee (firstname,lastname) VALUES(:fn,:ln)
	$prep = $con->prepare($sql);
	$prep->execute($data);

	return $prep;
}

function iteration($data){
	if (!empty($data)) {
		return $data;
	}else{
		return '';
	}
}




function verify_record($con,$data,$sql){
	$prep = $con->prepare($sql);
	$prep->execute($data);

	$row = $prep->fetch();

	return $prep->rowCount();
}





function userAuth($row){
	$_SESSION['account_id'] = iteration($row['account_id']);
	$_SESSION['fullname'] = iteration($row['fn'].' '.$row['ln']);
	$_SESSION['firstname'] = iteration($row['fn']);
	$_SESSION['middlename'] = iteration($row['mn']);
	$_SESSION['lastname'] = iteration($row['ln']);

	$_SESSION['gender'] = iteration($row['gender']);
	// user types 1 = admin, 2 = staff and 3 = pwd_user
	$_SESSION['user_type'] = iteration($row['user_type']);
	// verifications puposes
	$_SESSION['password'] = iteration($row['password']);
}


function GetLastId($con,$data,$sql){
	// sample
	// INSERT INTO tbl_employee (firstname,lastname) VALUES(:fn,:ln)
	$prep = $con->prepare($sql);
	$prep->execute($data);

	return $con->lastInsertId();
}

function format_date($format,$date){
	return date ($format,strtotime($date));
}

function delete_query($con,$data,$sql){
	// sample
	// DELETE from tbl_employee WHERE id=:id
	$prep = $con->prepare($sql);
	$prep->execute($data);

	if ($prep) {
		return 1;
	}
}


function send_email($name,$email,$username,$password){
	// Instantiation and passing `true` enables exceptions
	$mail = new PHPMailer(true);

	try {
	    //Server settings
     	$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	    $mail->SMTPDebug = false; 								  // Enable verbose debug output
	    $mail->isSMTP(); 									  // Set mailer to use SMTP
	    $mail->Host = 'smtp.gmail.com';  				      // Specify main and backup SMTP servers
	    $mail->SMTPAuth = true; 							  // Enable SMTP authentication
	    $mail->Username = 'jltesting12345@gmail.com'; 				  // SMTP username
	    $mail->Password = 'GreatWhite18!';                           // SMTP password
	    $mail->Port = 587;                                    // TCP port to connect to

	    //Recipients
	    $mail->setFrom('jltesting12345@gmail.com', 'AMA Student Class Attendance Monitoring');
	    $mail->addAddress($email, $name);     // Add a recipient
	 
	    $messages = 'Hi, '.$name.'!<br><br><br>';
	    $messages .= 'This is your account information.<br><br>';
	    $messages .= '------------------------------------------<br>';
	    $messages .= 'Username:'.$username.'<br>';
	    $messages .= 'Password:'.$password.'<br>';
	    $messages .= '------------------------------------------<br>';

	    // Content
	    $mail->isHTML(true);                                  // Set email format to HTML
	    $mail->Subject = '(AMA Student Class Attendance Monitoring) Account Information';
	    $mail->Body = $messages;
	    $mail->AltBody = '';

		if ($mail->send()) {
			echo  1;
		}

	} catch (Exception $e) {
	    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
	}
}


// For address
function ph_region(){
  $str = file_get_contents("../dist/ph.json");
  $json = json_decode($str, true);
  	echo '<option></option>';
  foreach ($json as $key => $value) {
   	echo '<option value="'.$key.'">REGION '.$key.'</option>';
  }
}


function ph_province($region){
  $str = file_get_contents("../dist/ph.json");
  $json = json_decode($str, true);
  	echo '<option></option>';
  foreach ($json[$region]['province_list'] as $key => $value) {
	echo '<option value="'.$key.'">'.$key.'</option>';
  }
}

function ph_municipality($region,$province){
  $str = file_get_contents("../dist/ph.json");
  $json = json_decode($str, true);
  	echo '<option></option>';
  foreach ($json[$region]['province_list'][$province]['municipality_list'] as $key => $value) {
	echo '<option value="'.$key.'">'.$key.'</option>';
  }
}


function ph_barangay($region,$province,$municipality){
  $str = file_get_contents("../dist/ph.json");
  $json = json_decode($str, true);
  	echo '<option></option>';
  foreach ($json[$region]['province_list'][$province]['municipality_list'][$municipality]['barangay_list'] as $key) {
    echo '<option value="'.$key.'">'.$key.'</option>';
  }
}


?>



