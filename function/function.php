<?php 
date_default_timezone_set('Asia/Manila');
session_start();
include("config.php");
include("helper.php");
include("../core/core.php");
include("charts_function.php");

$id = (isset($_SESSION['account_id']))? $_SESSION['account_id'] : '';
$today = date('Y-m-d h:i:s A');

$action  = $_POST['action'];
switch ($action) {
	
	//charts 
	case 'get_data_chart':
		$date_to = $_POST['date_to'];
		$year_to = $_POST['year_to'];

		$param_date = $year_to.' '.$date_to;

		echo get_data_charts($con,$param_date);

	break;

	case 'save_illness':
		$illness_id = $_POST['illness_id'];
		$illness_name = $_POST['illness_name'];
		$symptoms = $_POST['symptoms'];
		$status = 0;

		if (!empty($illness_id)) {
			// insert illness
			$data_illness = array('illness_name' => $illness_name);	
			$last_id = $illness_id;

			$arr = explode(',', $symptoms);

			$param = array('illness_id' => $last_id);
			delete($con,'tbl_illness_symptoms',$param);

			for ($i=0; $i < count($arr); $i++) { 
				$data_symptoms = array('illness_id' => $last_id, 'symptoms_name' => $arr[$i]);
				if (insert($con,'tbl_illness_symptoms',$data_symptoms) > 0) {
					$status = 2;
				}
			}

			echo $status;
		}else{
			// insert illness
			$data_illness = array('illness_name' => $illness_name);	
			$last_id = get_last_id($con,'tbl_illness',$data_illness);

			$arr = explode(',', $symptoms);

			for ($i=0; $i < count($arr); $i++) { 
				$data_symptoms = array('illness_id' => $last_id, 'symptoms_name' => $arr[$i]);
				if (insert($con,'tbl_illness_symptoms',$data_symptoms) > 0) {
					$status = 1;
				}
			}

			echo $status;
		}

	break;

	case 'forget_password':
		$username = $_POST['username'];

		$data = array('username' => $username);
		$sql = "SELECT * from tbl_account where username=:username";


		if (verify_record($con,$data,$sql) > 0) {
			$result = fetch_record($con,$data,$sql);
			$row = $result->fetch();

			$ran = rand(1,2);

			?>
				<div class="row" id="questionare">
		          <div class="col-sm-12">
		           <hr>
		            <div class="text-center">
		              <span class="text-dark h5"><?php echo $row['question_'.$ran] ?></span>
		            </div>
		             <br>
		            <div class="form-group">
		              <input type="text" name="answer" id="answer" class="form-control" placeholder="Enter your answer">
		            </div>
		          </div>
		          <div class="col-sm-12"></div>

		            <div class="col-sm-7">
		            </div>
		          
		            <div class="col-sm-5">
		              <button class=" btn-sm btn btn-success btn-block btn-flat" id="verifybtn" onclick="check_answer()">Check Answer</button>
		            </div>

		        </div>
			<?php

		}else{
			echo 0;
		}

	break;

	case 'show_password':
		$username = $_POST['username'];
		$answer = $_POST['answer'];

		$data = array('username' => $username, 'answer' => $answer);
		$sql = "SELECT * from tbl_account where username=:username and (answer_1=:answer or answer_2=:answer)";

		if (verify_record($con,$data,$sql) > 0) {
			$result = fetch_record($con,$data,$sql);
			$row = $result->fetch();
			?>
			<div class="row" id="show_password_form">
		          <div class="col-sm-12">
		           <hr>
		            <div class="form-group">
		              <input type="password" name="password" id="password" class="form-control" placeholder="Enter your answer">
		            </div>
		          </div>
		          <div class="col-sm-12"></div>

		            <div class="col-sm-7">
		            </div>
		          
		            <div class="col-sm-5">
		              <button class=" btn-sm btn btn-success btn-block btn-flat" onclick="submit_password('<?php echo $row['account_id'] ?>')">Change Password</button>
		            </div>
		        </div>
			<?php
		}else{
			echo 0;
		}
	break;

	case 'change_forgot_password':
		$password = $_POST['password'];
		$account_id = $_POST['account_id'];

		$new_password = password_hash($password, PASSWORD_DEFAULT);

		$data = array('account_id' => $account_id,'password' => $new_password);

		echo update($con,'tbl_account',$data);

	break;

	case 'save_license_no':
		$license_no = $_POST['license_no'];
		$data = array('account_id' => $id, 'license_no' => $license_no);
		$sql = "UPDATE tbl_account set license_number=:license_no where account_id=:account_id";

		if (save($con,$data,$sql) > 0) {
			echo 1;
		}

	break;

	case 'patient_today':
			$date_today = date('Ymd');
			$data = array();
			$sql = "SELECT count(medical_id) from tbl_medical where is_finish = 0 and DATE_FORMAT(date_entry,'%Y%m%d') = $date_today";

			$result = fetch_record($con,$data,$sql);
			$row = $result->fetch();

			echo $row['count(medical_id)'];
	break;


	case 'show_patient_ini':
			$date_today = date('Ymd');
			$data = array();
			$sql = "SELECT a.*,b.* from tbl_medical a left join tbl_patient b on a.patient_id=b.patient_id where a.is_finish = 0 and DATE_FORMAT(a.date_entry,'%Y%m%d') = $date_today";

			$result = fetch_record($con,$data,$sql);
			while ($row = $result->fetch()) {
				$name = ucfirst($row['ln']).', '.ucfirst($row['fn']).' '.ucfirst($row['mn'][0]);
				?>
				<tr id="data_doctor_<?php echo $row['patient_id'] ?>">
					<td class="text-capitalize" nowrap><?php echo $name ?></td>
					<td class="text-capitalize" nowrap><?php echo get_age($row['bdate']) ?></td>
					<td nowrap><?php echo $row['gender'] ?></td>
					<td class="text-capitalize" nowrap><?php echo strtolower($row['barangay'].', '.$row['municipality'].' '.$row['province']) ?></td>
					<td class="text-center" nowrap>
						<button class="btn btn-info" data-toggle="dropdown"><i class="fa fa-user-cog"></i></button>
						<div class="dropdown-menu">
							<button class="dropdown-item" onclick="edit_check_up('<?php echo $name ?>','<?php echo $row['medical_id'] ?>','<?php echo $row['patient_id'] ?>','<?php echo $row['bp'] ?>','<?php echo $row['pulse_rate'] ?>','<?php echo $row['respiratory_rate'] ?>','<?php echo $row['temp'] ?>','<?php echo $row['height'] ?>','<?php echo $row['weight'] ?>','<?php echo $row['cbc'] ?>','<?php echo $row['urine_analysis'] ?>','<?php echo $row['fecalysis'] ?>','<?php echo $row['gram_staining'] ?>','<?php echo $row['hepa_b'] ?>','<?php echo $row['syphilis_screening'] ?>','<?php echo $row['bun'] ?>','<?php echo $row['ca'] ?>','<?php echo $row['ci'] ?>','<?php echo $row['chol'] ?>','<?php echo $row['creat'] ?>','<?php echo $row['fasting_blood_sugar'] ?>','<?php echo $row['potassium'] ?>','<?php echo $row['sodium'] ?>','<?php echo $row['tsh'] ?>','<?php echo $row['urea'] ?>','<?php echo $row['illness_history'] ?>','<?php echo $row['medical_history'] ?>','<?php echo $row['symptoms'] ?>');"><i class="fa fa-edit text-success"></i> Edit</button>
							<button class="dropdown-item" onclick="delete_patient_ini('<?php echo $row['medical_id'] ?>','<?php echo $name ?>');"><i class="fa fa-trash text-danger"></i> Delete</button>
						</div>
					</td>
				</tr>
			<?php
			}
			
	break;

	case 'count_patient':
		$date_today = date('Ymd');
		$data = array();
		$sql = "SELECT count(medical_id) from tbl_medical where is_finish = 1 and DATE_FORMAT(date_entry,'%Y%m%d') = $date_today";

		$result = fetch_record($con,$data,$sql);
		$row = $result->fetch();

		echo $row['count(medical_id)'];

	break;
	// charts

	// Login backend Script
	case 'login':
		$email_add = $_POST['username'];
		$pwd = $_POST['password'];

		$data = array('email' => $email_add);

		$sql = "SELECT * from tbl_account where username=:email";
		
		$prep = $con->prepare($sql);
		$prep->execute($data);

		if ($prep->rowCount() > 0) {
			$row = $prep->fetch();
			if (password_verify($pwd, $row['password'])) {
				userAuth($row);
				logs($con,'Login on '.$today,$row['account_id'],$today);
				// echo $row['user_type'];
				echo 1;
			}else{
				echo 0;
			}

		}else{
			echo 0;
		}

	break;

	case 'get_list_province':
		$region = $_POST['region'];

		echo ph_province($region);
	break;

	case 'get_list_city':
		$region = $_POST['region'];
		$province = $_POST['province'];
		
		echo ph_municipality($region,$province);
	break;

	case 'get_list_barangay':
		$region = $_POST['region'];
		$province = $_POST['province'];
		$city = $_POST['city'];
		
		echo ph_barangay($region,$province,$city);
	break;


	case 'change_password':
		$old_password = $_POST['o_pwd'];
		$confirm_password = $_POST['c_pwd'];

		$new_password = password_hash($confirm_password, PASSWORD_DEFAULT);

		$data = array('password' => $new_password, 'id' => $id);
		$sql = "UPDATE tbl_user set password=:password where user_id=:id";


		if (password_verify($old_password, $auth['password']) == true) {
			echo save($con,$data,$sql);
				logs($con,'Change password on '.$today,$id,$today);
		}else{
			echo 2;
		}

	break;

	case 'add_patient':
		$patient_id = $_POST['patient_id'];
		$fn = $_POST['patient_fn'];
		$mn = $_POST['patient_mn'];
		$ln = $_POST['patient_ln'];
		$bdate = $_POST['bdate'];
		$gender = $_POST['patient_gender'];
		$region = $_POST['region'];
		$province = $_POST['province'];
		$city = $_POST['city'];
		$barangay = $_POST['barangay'];
		$accept = $_POST['accept'];

		if (!empty($patient_id)) {
			$sql_update ="UPDATE tbl_patient set fn=:fn,mn=:mn,ln=:ln,bdate=:bdate,gender=:gender,region=:region,province=:province,municipality=:city,barangay=:barangay where patient_id=:patient_id";
			$update_data = array(
				'patient_id' => $patient_id,
				'fn' => $fn,
				'mn' => $mn,
				'ln' => $ln,
				'bdate' => $bdate,
				'gender' => $gender,
				'region' => $region,
				'province' => $province,
				'city' => $city,
				'barangay' => $barangay
			);
			if (save($con,$update_data,$sql_update) > 0) {
				logs($con,'Edited Patient "'.$fn.' '.$ln.'" on '.$today,$id,$today);

				echo 2;
			}
		}else{

			$ver_data = array('fn' => $fn,
							'mn' => $mn,
							'ln' => $ln,
							'bdate' => $bdate,
							'gender' => $gender);

			$ver_sql = "SELECT * from tbl_patient where fn =:fn and mn =:mn and ln =:ln and bdate =:bdate and gender =:gender";

			if (verify_record($con,$ver_data,$ver_sql) > 0 &&  $accept == 0) {
				echo 123;
			}else{
				$sql_insert ="INSERT INTO tbl_patient(fn,mn,ln,bdate,gender,region,province,municipality,barangay) VALUES(:fn,:mn,:ln,:bdate,:gender,:region,:province,:city,:barangay)";
				$insert_data = array(
					'fn' => $fn,
					'mn' => $mn,
					'ln' => $ln,
					'bdate' => $bdate,
					'gender' => $gender,
					'region' => $region,
					'province' => $province,
					'city' => $city,
					'barangay' => $barangay
				);

				if (save($con,$insert_data,$sql_insert) > 0) {
					logs($con,'Added Patient "'.$fn.' '.$ln.'" on '.$today,$id,$today);
					echo 1;
				}
			}
		}
	break;

	case 'save_meds':
		$medicine_id = $_POST['medicine_id'];
		$gen_name = $_POST['gen_name'];
		$qty = $_POST['qty'];
		$dosage = $_POST['dosage'];
		$form_type = $_POST['form_type'];
		$brand = $_POST['brand'];
		$supplier = $_POST['supplier'];
		$illness = $_POST['illness'];


		if (!empty($medicine_id)) {
			$data_update = array('medicine_id' => $medicine_id,
								'gen_name' => $gen_name,
								'qty' => $qty,
								'dosage' => $dosage,
								'form_type' => $form_type,
								'brand' => $brand,
								'supplier' => $supplier,
								'illness' => $illness
								);
			$sql_update = "UPDATE tbl_medicine set brand_name=:brand,generic_name=:gen_name,qty=:qty,dosage=:dosage,form_type=:form_type,supplier=:supplier,illness=:illness where medicine_id=:medicine_id";
			if (save($con,$data_update,$sql_update) > 0) {
				echo 2;
				logs($con,'Edited medicine "'.$brand.'" on '.$today,$id,$today);
			}
		}else{
			$data_insert =array('gen_name' => $gen_name,
								'qty' => $qty,
								'dosage' => $dosage,
								'form_type' => $form_type,
								'brand' => $brand,
								'supplier' => $supplier,
								'illness' => $illness,
								'date_entry' => $today
								);
			$sql_insert = "INSERT INTO tbl_medicine(brand_name,generic_name,qty,dosage,form_type,supplier,illness,date_entry) VALUES(:brand,:gen_name,:qty,:dosage,:form_type,:supplier,:illness,:date_entry)";
			if (save($con,$data_insert,$sql_insert) > 0) {
				echo 1;
				logs($con,'Added medicine "'.$brand.'" on '.$today,$id,$today);

			}
		}

	break;


	case 'add_check_up':
		
		// Patient name
		$patient_name = $_POST['patient_name'];

		// doctor Id
		$doctor_id = $id;

		$date_entry = date('Y-m-d H:i:s A');

		$check_id = $_POST['check_id'];
		$patient_id = $_POST['patient_id'];
		$bp = $_POST['bp'];
		$pulse_rate = $_POST['pulse_rate'];
		$respiratory_rate = $_POST['respiratory_rate'];
		$temp = $_POST['temp'];
		$height = $_POST['height'];
		$weight = $_POST['weight'];
		$cbc = $_POST['cbc'];
		$urine = $_POST['urine'];
		$fecalysis = $_POST['fecalysis'];
		$gram_staining = $_POST['gram_staining'];
		$hepa_b = $_POST['hepa_b'];
		$syphilis_screening = $_POST['syphilis_screening'];
		$bun = $_POST['bun'];
		$ca = $_POST['ca'];
		$ci = $_POST['ci'];
		$chol = $_POST['chol'];
		$creat = $_POST['creat'];
		$fasting_blood_sugar = $_POST['fasting_blood_sugar'];
		$potassium = $_POST['potassium'];
		$sodium = $_POST['sodium'];
		$tsh = $_POST['tsh'];
		$urea = $_POST['urea'];
		$illness_history = $_POST['illness_history'];
		$medication_history = $_POST['medication_history'];
		$symptoms = $_POST['symptoms'];


		if (!empty($check_id)) {
			// update_data
			$data_update = array('medical_id' => $check_id,
			'patient_id' => $patient_id,
			'doctor_name' => $doctor_id,
			'in_charge' => $doctor_id,
			'height' => $height,
			'weight' => $weight,
			'bp' => $bp,
			'symptoms' => $symptoms,
			'urine_analysis' => $urine,
			'fasting_blood_sugar' => $fasting_blood_sugar,
			'illness_history' => $illness_history,
			'medical_history' => $medication_history,
			'temp' => $temp,
			'pulse_rate' => $pulse_rate,
			'respiratory_rate' => $respiratory_rate,
			'cbc' => $cbc,
			'fecalysis' => $fecalysis,
			'gram_staining' => $gram_staining,
			'hepa_b' => $hepa_b,
			'syphilis_screening' => $syphilis_screening,
			'bun' => $bun,
			'ca' => $ca,
			'ci' => $ci,
			'chol' => $chol,
			'creat' => $creat,
			'potassium' => $potassium,
			'sodium' => $sodium,
			'tsh' => $tsh,
			'urea' => $urea,
			'date_entry' => $date_entry);

			if (update($con,'tbl_medical',$data_update) > 0) {
				echo 2;
				logs($con,'Edited Check up record of "'.$patient_name.'" on '.$today,$id,$today);

			}
		}else{
			// insert data
			$data_insert = array('patient_id' => $patient_id,
			'doctor_name' => $doctor_id,
			'in_charge' => $doctor_id,
			'height' => $height,
			'weight' => $weight,
			'bp' => $bp,
			'symptoms' => $symptoms,
			'urine_analysis' => $urine,
			'fasting_blood_sugar' => $fasting_blood_sugar,
			'illness_history' => $illness_history,
			'medical_history' => $medication_history,
			'temp' => $temp,
			'pulse_rate' => $pulse_rate,
			'respiratory_rate' => $respiratory_rate,
			'cbc' => $cbc,
			'fecalysis' => $fecalysis,
			'gram_staining' => $gram_staining,
			'hepa_b' => $hepa_b,
			'syphilis_screening' => $syphilis_screening,
			'bun' => $bun,
			'ca' => $ca,
			'ci' => $ci,
			'chol' => $chol,
			'creat' => $creat,
			'potassium' => $potassium,
			'sodium' => $sodium,
			'tsh' => $tsh,
			'urea' => $urea,
			'date_entry' => $date_entry);

			if (insert($con,'tbl_medical',$data_insert)> 0) {
				echo 1;
				logs($con,'Added Check up record of "'.$patient_name.'" on '.$today,$id,$today);
				
			}
		}
	break;


	case 'show_patient':
		$data = array();
		$sql = "SELECT * from  tbl_patient where is_delete is null order by ln,fn,mn";
		$result = fetch_record($con,$data,$sql);

		while ($row = $result->fetch()) {
			$name = ucfirst($row['ln']).', '.ucfirst($row['fn']).' '.ucfirst($row['mn'][0]);
			?>
			<tr id="data_doctor_<?php echo $row['patient_id'] ?>">
				<td class="text-capitalize" nowrap><?php echo $name ?></td>
				<td class="text-capitalize" nowrap><?php echo get_age($row['bdate']) ?></td>
				<td nowrap><?php echo $row['gender'] ?></td>
				<td class="text-capitalize" nowrap><?php echo strtolower($row['barangay'].', '.$row['municipality'].' '.$row['province']) ?></td>
				<td class="text-center" nowrap>
					<button class="btn btn-info" data-toggle="dropdown"><i class="fa fa-user-cog"></i></button>
					<div class="dropdown-menu">
						<button class="dropdown-item" onclick="check_up('<?php echo $row['patient_id'] ?>','<?php echo $name ?>');"><i class="fa fa-calendar-check text-info"></i> Initial Check-up</button>
						<div class="dropdown-divider"></div>
						<button class="dropdown-item" onclick="edit_patient(); add_edit_patient('<?php echo $row['patient_id'] ?>','<?php echo $row['fn'] ?>','<?php echo $row['mn'] ?>','<?php echo $row['ln'] ?>','<?php echo date('Y-m-d',strtotime($row['bdate'])) ?>','<?php echo $row['gender'] ?>','<?php echo $row['region'] ?>','<?php echo $row['province'] ?>','<?php echo $row['municipality'] ?>','<?php echo $row['barangay'] ?>')"><i class="fa fa-edit text-success"></i> Edit</button>
						<button class="dropdown-item" onclick="delete_patient('<?php echo $row['patient_id'] ?>','<?php echo $name ?>');"><i class="fa fa-trash text-danger"></i> Delete</button>
					</div>
				</td>
			</tr>
			<?php
		}
	break;

	case 'show_check_up':
		$illness = $_POST['illness'];
		$date_check = $_POST['date_check'];
		$clause = '';

		if (!empty($illness)) {
			$clause = "and a.diagnoses_result like '%$illness%'";
		}

		$data = array();
		$sql = "SELECT a.*,b.* from  tbl_medical a left join tbl_patient b on b.patient_id=a.patient_id where (b.is_delete is null and a.is_delete is null and a.is_finish = 1) ".$clause." order by a.medical_id desc";
		$result = fetch_record($con,$data,$sql);

		$i = 0;

		while ($row = $result->fetch()) {
			$i++;
			if (strtotime(date('Y-m-d',strtotime($date_check))) == strtotime(date('Y-m-d',strtotime($row['date_entry'])))) {
			$name = ucfirst($row['ln']).', '.ucfirst($row['fn']).' '.ucfirst($row['mn'][0]);
			$age = get_age($row['bdate']);
			$gender = $row['gender'];
			$patient_id = $row['medical_id'];
			$addr = $row['barangay'].', '.$row['municipality'].' '.$row['province'];

			?>
			<tr id="data_doctor_<?php echo $row['patient_id'] ?>">
				<td class="text-capitalize" nowrap><?php echo $i ?></td>
				<td class="text-capitalize" nowrap><?php echo $name ?></td>
				<td class="text-capitalize" nowrap><?php echo $row['date_entry'] ?></td>
				<td class="text-capitalize" nowrap><?php echo $row['action_taken'] ?></td>
				<td class="text-capitalize" nowrap><?php echo $row['diagnoses_result'] ?></td>
				<td class="text-capitalize" >
					<?php
					$data_med = array('medical_id' => $row['medical_id']);
					$query = "SELECT a.*,b.* from tbl_medical_med_reco a left join tbl_medicine b on a.medicine_id=b.medicine_id where a.medical_id=:medical_id";
					$res = fetch_record($con,$data_med,$query);


					$data_meds_out = array('id' => $row['medical_id']);
				    $sql_meds_out = "SELECT * from tbl_out_medicine where medical_id=:id";

				    $res_out = fetch_record($con,$data_meds_out,$sql_meds_out);

						while ($rows = $res->fetch()) {
							echo $rows['brand_name'].', ';
						}

						while ($rowss = $res_out->fetch()) {
							echo $rowss['medicine_brand'].', ';
						}
					 ?>
				</td>
				<td class="text-center" nowrap>
					<button class="btn btn-info" data-toggle="dropdown"><i class="fa fa-user-cog"></i></button>
					<div class="dropdown-menu">
						<button class="dropdown-item" onclick="show_diagnoses('<?php echo $row['medical_id'] ?>','<?php echo $name ?>','<?php echo strtolower($row['barangay'].', '.$row['municipality'].' '.$row['province']) ?>','<?php echo $age ?>','<?php echo $gender ?>');"><i class="fa fa-list-alt text-info"></i> Diagnosis</button>
						<?php if ($auth['user_type'] == 1): ?>
							<a class="dropdown-item" target="_blank" href="mc.php?id=<?php echo $patient_id ?>&name=<?php echo $name ?>&address=<?php echo $addr ?>&age=<?php echo $age ?>&gender=<?php echo $gender ?>" > <i class="fa fa-print text-info"></i> Print Medical Certificate</a>
						<?php endif ?>
						<!-- <div class="dropdown-divider"></div> -->
						<!-- <button class="dropdown-item" onclick="delete_check_up('<?php echo $row['medical_id'] ?>','<?php echo $name ?>');"><i class="fa fa-trash text-danger"></i> Delete</button> -->
					</div>
				</td>
			</tr>
			<?php
			}
		}
	break;


	// doctor part

	case 'save_diagnosis_doctor':
	    $names = $_POST['name']; 
		$diag_id = $_POST['diag_id'];
		$illness_result = $_POST['illness_result'];
		$action_taken = $_POST['action_taken'];
		$num_meds_selected = $_POST['num_meds_selected'];
		$qty_selected = $_POST['qty_selected'];
		$dossage = $_POST['dossage'];
		$intake = $_POST['intake'];
		$number_of_row = $_POST['number_of_row'];
		
		$submit_type = $_POST['submit_type'];

		$data_out_med = array();



		// echo $number_of_row;
		// var_dump($data_out_med);


		$sql = "UPDATE tbl_medical set doctor_name=:doctor,diagnoses_result=:result,action_taken=:action_taken,is_finish=:finish where medical_id=:medical_id";

		$data = array('medical_id' => $diag_id, 'result' => $illness_result, 'action_taken' => $action_taken,'doctor' => $id, 'finish'=>1);



		if (save($con,$data,$sql) > 0) {
			if (!empty($dossage)) {
				$medicine_ids = explode(',', $num_meds_selected);
				$qty_meds = explode(',', $qty_selected);
				$dossage_qty = explode(',', $dossage);
				$intake_sched = explode(',', $intake);


					for ($i=0; $i < count($medicine_ids); $i++) { 

						if ($qty_meds[$i] == '' || $qty_meds[$i] == null || $qty_meds[$i] == 0) { 

						}else{
							$qty = $qty_meds[$i];
							$dsg = (!empty($dossage_qty[$i]))? $dossage_qty[$i] : 'N/A';
							$intake_sc = (!empty($intake_sched[$i]))? $intake_sched[$i] : 'N/A';


							if (!empty($qty)) {
								$fetch_qty = get_medicine_qty($con,$medicine_ids[$i]);
								$new_qty = ($fetch_qty - $qty);

								$arr_new_med = array('medicine_id' => $medicine_ids[$i], 'qty' => $new_qty); 

								if (update($con,'tbl_medicine',$arr_new_med) > 0) {
									$medicines_and_qty = array('medical_id' => $diag_id,'medicine_id' => $medicine_ids[$i] , 'qty' => $qty, 'dosage' => $dsg, 'intake_sched' => $intake_sc);

									$ssql = "INSERT INTO tbl_medical_med_reco(medical_id,medicine_id,qty_reco,dosage,intake_schedule) VALUES(:medical_id,:medicine_id,:qty,:dosage,:intake_sched)";
									
									save($con,$medicines_and_qty,$ssql);
								}

								
							}

							
						}
				}

			}

			for ($i=0; $i <= $number_of_row; $i++) { 
				 // echo $i;

				if (!empty($_POST['out_med_qty_'.$i] || $_POST['out_med_brand_'.$i])) {
					 $data_out_med = array('medical_id' => $diag_id,
					 'medicine_brand'=> $_POST['out_med_brand_'.$i],
					 'medicine_generic_name'=> $_POST['out_med_generic_'.$i],
					 'qty'=> $_POST['out_med_qty_'.$i],
					 'dossage'=> $_POST['out_med_dossage_'.$i],
					 'intake_schedule'=> $_POST['out_med_intake_'.$i],
					 'date_entry' => date('Y-m-d H:i:s A')
					);

					$sql_out = "INSERT INTO tbl_out_medicine(medical_id,medicine_brand,medicine_generic_name,qty,dossage,intake_schedule,date_entry) VALUES(:medical_id,:medicine_brand,:medicine_generic_name,:qty,:dossage,:intake_schedule,:date_entry)";
					save($con,$data_out_med,$sql_out);
				}

				
			}


			logs($con, $names.' is Diagnosed "'.$illness_result.'" on '.$today,$id,$today);
			

			if ($submit_type == 'print') {
				echo 2;
			}else{
				echo 1;
			}

			
		}

	break;

	case 'show_check_up_pending':

		$date_check = $_POST['date_check'];
		$data = array();
		$sql = "SELECT a.*,b.*,a.symptoms as diag_symptoms from  tbl_medical a left join tbl_patient b on b.patient_id=a.patient_id where b.is_delete is null and a.is_delete is null and a.is_finish = 0 order by medical_id desc";
		$result = fetch_record($con,$data,$sql);
		$i = 1;
		while ($row = $result->fetch()) {
			if (strtotime(date('Y-m-d',strtotime($date_check))) == strtotime(date('Y-m-d',strtotime($row['date_entry'])))) {
				$name = ucfirst($row['ln']).', '.ucfirst($row['fn']).' '.ucfirst($row['mn'][0]);
				$brgy = strtolower($row['barangay'].', '.$row['municipality'].' '.$row['province']);
				$age = get_age($row['bdate']);
				$gender = $row['gender'];
			?>
			<tr id="data_doctor_<?php echo $row['patient_id'] ?>">
				<td>P00-<?php echo $i ?></td>
				<td class="text-capitalize" nowrap><?php echo $name ?></td>
				<td class="text-capitalize" nowrap><?php echo $brgy ?></td>
				<td class="text-capitalize" nowrap><?php echo $row['date_entry'] ?></td>
				<td class="text-center" nowrap>
					<button class="btn btn-info" data-toggle="dropdown"><i class="fa fa-user-cog"></i></button>
					<div class="dropdown-menu">
						<button class="dropdown-item" onclick="show_recommendations('<?php echo $row['medical_id'] ?>','<?php echo $name ?>','<?php echo $row['diag_symptoms'] ?>','<?php echo $brgy ?>','<?php echo $age ?>','<?php echo $gender ?>');"><i class="fa fa-list-alt text-info"></i> Diagnose</button>

					</div>
				</td>
			</tr>
			<?php
		}
		$i++;
	}
	break;

	case 'show_diagnoses':
		$patient_id = $_POST['id'];
		$name = $_POST['name'];
		$addr = $_POST['addr'];
		$age = $_POST['age'];
		$gender = $_POST['gender'];
		
		$data = array('id' => $patient_id);
		$sql = "SELECT a.*,b.* from tbl_medical a left join tbl_account b on a.doctor_name=b.account_id where a.medical_id=:id";
		$result = fetch_record($con,$data,$sql);

		$data_meds = array('id' => $patient_id);
		$sql_meds = "SELECT a.*,b.*, a.dosage as dosage from tbl_medical_med_reco a left join tbl_medicine b on a.medicine_id=b.medicine_id where a.medical_id=:id";

		$result_meds = fetch_record($con,$data_meds,$sql_meds);

		$data_meds_out = array('id' => $patient_id);
	    $sql_meds_out = "SELECT * from tbl_out_medicine where medical_id=:id";
	    $res_out = fetch_record($con,$data_meds_out,$sql_meds_out);

		$row = $result->fetch();

		$name_doctor = $row['fn'].' '.$row['ln'];
		?>

		<div class="row">
			<div class="col-sm-12">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs">
				  <li class="nav-item">
				    <a class="nav-link active" data-toggle="tab" href="#vital_reco">Vital Sign</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link" data-toggle="tab" href="#lab_reco">Lab Result</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link" data-toggle="tab" href="#medical_reco_">Medical History</a>
				  </li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
				  <div class="tab-pane container active" id="vital_reco">
				  	<div class="row">
				  		<div class="col-sm-12"><br></div>
						<div class="h6 col-sm-8"><b>Blood Preasure:</b> <?php echo $row['bp'] ?></div>
						<div class="h6 col-sm-4"><b>Pulse Rate:</b> <?php echo $row['pulse_rate'] ?></div>
						<div class="h6 col-sm-8"><b>Respiratory Rate:</b> <?php echo $row['respiratory_rate'] ?></div>
						<div class="h6 col-sm-4"><b>Temperature:</b> <?php echo $row['temp'] ?></div>
						<div class="h6 col-sm-8"><b>Height:</b> <?php echo $row['height'] ?></div>
						<div class="h6 col-sm-4"><b>Weight:</b> <?php echo $row['weight'] ?></div>
					</div>
				  </div>
				  <div class="tab-pane container fade" id="lab_reco">
				  	<div class="row">
				  		<div class="col-sm-12"><br></div>
						<div class="h6 col-sm-8"><b>Complete Blood Count:</b> <?php echo $row['cbc'] ?></div>
						<div class="h6 col-sm-4"><b>Urinalysis:</b> <?php echo $row['urine_analysis'] ?></div>
						<div class="h6 col-sm-8"><b>Fecalysis:</b> <?php echo $row['fecalysis'] ?></div>
						<div class="h6 col-sm-4"><b>Gram Staining:</b> <?php echo $row['gram_staining'] ?></div>
						<div class="h6 col-sm-8"><b>Hepa B Screening:</b> <?php echo $row['height'] ?></div>
						<div class="h6 col-sm-4"><b>Syphilis Screening:</b> <?php echo $row['syphilis_screening'] ?></div>
						<div class="col-sm-12 text-center">
							<hr>
							<label>BLOOD CHEMISTRY</label>
						</div>
						<div class="h6 col-sm-8"><b>Blood urea Nitrogen:</b> <?php echo $row['bun'] ?></div>
						<div class="h6 col-sm-4"><b>Calcium:</b> <?php echo $row['ca'] ?></div>
						<div class="h6 col-sm-8"><b>Chloride:</b> <?php echo $row['ci'] ?></div>
						<div class="h6 col-sm-4"><b>Creatinine:</b> <?php echo $row['creat'] ?></div>
						<div class="h6 col-sm-8"><b>Fasting Blood Sugar:</b> <?php echo $row['fasting_blood_sugar'] ?></div>
						<div class="h6 col-sm-4"><b>Potassium:</b> <?php echo $row['potassium'] ?></div>
						<div class="h6 col-sm-8"><b>Sodium:</b> <?php echo $row['sodium'] ?></div>
						<div class="h6 col-sm-4"><b>Thyroid Stimulating Hormone:</b> <?php echo $row['tsh'] ?></div>
						<div class="h6 col-sm-8"><b>Urea:</b> <?php echo $row['urea'] ?></div>
						<div class="h6 col-sm-4"></div>
					</div>
				  </div>
				  <div class="tab-pane container fade" id="medical_reco_">
			  		<div class="row">
				  		<div class="col-sm-12"><br></div>
						<div class="h6 col-sm-8"><b>Illness History:</b> <?php echo $row['illness_history'] ?></div>
						<div class="h6 col-sm-4"><b>Medication History:</b> <?php echo $row['medical_history'] ?></div>
						<div class="h6 col-sm-8"><b>Sympthoms/Complains:</b> <?php echo $row['symptoms'] ?></div>
						<div class="h6 col-sm-4"></div>
					</div>
				  </div>
				</div>
			</div>
		</div>

		<div class="row">
			<hr>
			<div class="h6 col-sm-12"> <br> <b>Physician's Notes:</b> <?php echo $row['action_taken'] ?></div>

			<hr>
		</div>
		<br>

		<table class="table table-striped">
			<thead>
				<tr>
					<th nowrap>Prescription</th>
					<th nowrap>Qty</th>
					<th nowrap>Dosage</th>
					<th nowrap>Intake Schedule</th>
				</tr>
			</thead>

			<tbody>
				<?php 
				while ($rows = $result_meds->fetch()) {
				?>
				<?php if ($rows['qty_reco'] != 0 || $rows['qty_reco'] != '' || $rows['qty_reco'] != null): ?>
					<tr>
						<td nowrap><?php echo $rows['brand_name'].' ('.$rows['generic_name'].')' ?></td>
						<td nowrap><?php echo $rows['qty_reco'] ?></td>
						<td nowrap><?php echo $rows['dosage'] ?></td>
						<td nowrap><?php echo $rows['intake_schedule'] ?></td>
					</tr>
				<?php endif ?>
				<?php
				}

				while ($rowss = $res_out->fetch()) {
						?>
						
						<?php if (empty($rowss['medicine_brand'])): ?>
							<?php else: ?>
								<tr>
									<td nowrap><?php echo $rowss['medicine_brand'].' ('.$rowss['medicine_generic_name'].')' ?></td>
									<td nowrap><?php echo $rowss['qty'] ?></td>
									<td nowrap><?php echo $rowss['dossage'] ?></td>
									<td nowrap><?php echo $rowss['intake_schedule'] ?></td>
								</tr>
						<?php endif ?>

						
					<?php
					}
				 ?>


				
			</tbody>
		</table>

		<br><br>
		<div class="text-right">
			<!-- <span class="text-capitalize">Doctor: <?php echo $name_doctor ?></span> -->
		</div>
		<br><br>
		<div class="text-right">
				<hr>
				<button class="btn btn-default" data-dismiss="modal">Close</button>
				<?php if ($auth['user_type'] == 1): ?>
					<a class="btn btn-primary" target="_blank" href="print_pdf.php?id=<?php echo $patient_id ?>&name=<?php echo $name ?>&address=<?php echo $addr ?>&age=<?php echo $age ?>&gender=<?php echo $gender ?>" >Print</a>
				<?php endif ?>

				<?php if ($auth['user_type'] == 3): ?>
					<button class="btn btn-success" onclick="claim_certificate('<?php echo $row['medical_id']  ?>')">Mark as Finish</button>
				<?php endif ?>
				
			</div>
		<?php

	break;

	case 'finish_medical':
		$medid = $_POST['medid'];
		$data = array('medid' => $medid);

		$sql = "UPDATE tbl_medical set is_finish=2 where medical_id=:medid";

		if (save($con,$data,$sql) > 0) {
			echo 1;
		}

	break;

	case 'show_diagnosis_recommendations':
		$patient_id = $_POST['id'];
		$name = $_POST['name'];
		$symptoms = $_POST['symptoms'];
		

		$str = explode(',', $symptoms);
		$parameter = '';

		$data = array('id' => $patient_id);
		$sql = "SELECT * from tbl_medical where medical_id=:id";
		$result = fetch_record($con,$data,$sql);

		$data_meds = array('symptoms' => $symptoms);

		for ($i=0; $i < count($str); $i++) { 
			 $parameter .= " or a.symptoms_name LIKE '%".$str[$i]."%'";
		}


		// echo $parameter;

		$sql_meds = "SELECT a.*,b.* from tbl_illness_symptoms a left join tbl_illness b on a.illness_id=b.illness_id where a.symptoms_name LIKE '%$symptoms%' ".$parameter." group by b.illness_name";

		$result_meds = fetch_record($con,$data_meds,$sql_meds);

		$row = $result->fetch();
		?>

		
		<div class="row">
			<div class="col-sm-12">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs">
				  <li class="nav-item">
				    <a class="nav-link active" data-toggle="tab" href="#vital_reco">Vital Sign</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link" data-toggle="tab" href="#lab_reco">Lab Result</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link" data-toggle="tab" href="#medical_reco_">Medical History</a>
				  </li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
				  <div class="tab-pane container active" id="vital_reco">
				  	<div class="row">
				  		<div class="col-sm-12"><br></div>
						<div class="h6 col-sm-8"><b>Blood Preasure:</b> <?php echo $row['bp'] ?></div>
						<div class="h6 col-sm-4"><b>Pulse Rate:</b> <?php echo $row['pulse_rate'] ?></div>
						<div class="h6 col-sm-8"><b>Respiratory Rate:</b> <?php echo $row['respiratory_rate'] ?></div>
						<div class="h6 col-sm-4"><b>Temperature:</b> <?php echo $row['temp'] ?></div>
						<div class="h6 col-sm-8"><b>Height:</b> <?php echo $row['height'] ?></div>
						<div class="h6 col-sm-4"><b>Weight:</b> <?php echo $row['weight'] ?></div>
					</div>
				  </div>
				  <div class="tab-pane container fade" id="lab_reco">
				  	<div class="row">
				  		<div class="col-sm-12"><br></div>
						<div class="h6 col-sm-8"><b>Complete Blood Count:</b> <?php echo $row['cbc'] ?></div>
						<div class="h6 col-sm-4"><b>Urinalysis:</b> <?php echo $row['urine_analysis'] ?></div>
						<div class="h6 col-sm-8"><b>Fecalysis:</b> <?php echo $row['fecalysis'] ?></div>
						<div class="h6 col-sm-4"><b>Gram Staining:</b> <?php echo $row['gram_staining'] ?></div>
						<div class="h6 col-sm-8"><b>Hepa B Screening:</b> <?php echo $row['height'] ?></div>
						<div class="h6 col-sm-4"><b>Syphilis Screening:</b> <?php echo $row['syphilis_screening'] ?></div>
						<div class="col-sm-12 text-center">
							<hr>
							<label>BLOOD CHEMISTRY</label>
						</div>
						<div class="h6 col-sm-8"><b>Blood urea Nitrogen:</b> <?php echo $row['bun'] ?></div>
						<div class="h6 col-sm-4"><b>Calcium:</b> <?php echo $row['ca'] ?></div>
						<div class="h6 col-sm-8"><b>Chloride:</b> <?php echo $row['ci'] ?></div>
						<div class="h6 col-sm-4"><b>Creatinine:</b> <?php echo $row['creat'] ?></div>
						<div class="h6 col-sm-8"><b>Fasting Blood Sugar:</b> <?php echo $row['fasting_blood_sugar'] ?></div>
						<div class="h6 col-sm-4"><b>Potassium:</b> <?php echo $row['potassium'] ?></div>
						<div class="h6 col-sm-8"><b>Sodium:</b> <?php echo $row['sodium'] ?></div>
						<div class="h6 col-sm-4"><b>Thyroid Stimulating Hormone:</b> <?php echo $row['tsh'] ?></div>
						<div class="h6 col-sm-8"><b>Urea:</b> <?php echo $row['urea'] ?></div>
						<div class="h6 col-sm-4"></div>
					</div>
				  </div>
				  <div class="tab-pane container fade" id="medical_reco_">
			  		<div class="row">
				  		<div class="col-sm-12"><br></div>
						<div class="h6 col-sm-8"><b>Illness History:</b> <?php echo $row['illness_history'] ?></div>
						<div class="h6 col-sm-4"><b>Medication History:</b> <?php echo $row['medical_history'] ?></div>
						<div class="h6 col-sm-8"><b>Sympthoms/Complains:</b> <?php echo $row['symptoms'] ?></div>
						<div class="h6 col-sm-4"></div>
					</div>
				  </div>
				</div>
			</div>
		</div>

		<hr>
		<br>
		<h5><b>Possible Illness</b></h5>
		<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th nowrap>Suggested Illness</th>
					<th>Symptoms/Complaints</th>
					<th nowrap class="text-center">Choose</th>
				</tr>
			</thead>

			<tbody>
				<?php 
				while ($rows = $result_meds->fetch()) {
				?>
					<tr>
						<td nowrap><?php echo $rows['illness_name'] ?></td>
						<td><?php echo to_list_symptoms($con,$rows['illness_id']); ?></td>
						<td class="text-center"><input type="checkbox" name="illnesses" value="<?php echo $rows['illness_name'] ?>"></td>
					</tr>
				<?php
				}
				 ?>
				
			</tbody>
		</table>
		</div>
		<?php
	break;

	case 'show_medicine_result':
		$result = $_POST['result'];
		

		$str = explode(',', $result);
		$parameter = '';

		for ($i=0; $i < count($str); $i++) { 
			 $parameter .= " or illness LIKE '%".$str[$i]."%'";
		}

		$data_meds = array();
		$sql_meds = "SELECT * from tbl_medicine where  is_delete is null and (qty != 0 and illness LIKE '%$result% '".$parameter.")";

		$result_meds = fetch_record($con,$data_meds,$sql_meds);

		if (verify_record($con,$data_meds,$sql_meds) > 0) {
			?>
			<h5><b>Prescription</b></h5>
			<table class="table table-striped">
				<thead>
					<tr>
						<th nowrap>Brand Name</th>
						<th nowrap>Generic Name</th>
						<th nowrap class="text-center" width="20%">Qty</th>
						<th nowrap class="text-center" width="20%">Dosage</th>
						<th nowrap class="text-center" width="20%">Intake Schedule</th>
						<th nowrap class="text-center">Choose</th>
					</tr>
				</thead>
				
				<tbody id="">
					<?php 
					$i = 0;
					while ($rows = $result_meds->fetch()) {
					?>
						<tr>
							<td nowrap><?php echo $rows['brand_name'] ?></td>
							<td nowrap><?php echo $rows['generic_name'] ?></td>
							<td class="text-center">
								<input type="number" step="1" min="0"  title="<?php echo $rows['qty'].' '.$rows['form_type'] ?> Available"  oninput="get_verify_value('<?php echo $i ?>'); get_max($(this),'<?php echo $rows['qty'] ?>');" class="form-control meds_form" id="medicine_qty_<?php echo $i ?>" disabled>
							</td>
							<td class="text-center">
								<input type="text" oninput="get_verify_value_dossage('<?php echo $i ?>')" class="form-control dossage_form" id="dossage_<?php echo $i ?>" disabled>
							</td>

							<td class="text-center">
								<input type="text" oninput="get_verify_value_intake('<?php echo $i ?>')" class="form-control intake_form" id="intake_<?php echo $i ?>" disabled>
							</td>
							<td class="text-center"><input type="checkbox" onclick="get_medicine_selected('<?php echo $i ?>');" name="medicine_selected" id="medicine_selected_<?php echo $i ?>" value="<?php echo $rows['medicine_id'] ?>"></td>
						</tr>
					<?php
					$i++;
					}
					 ?>
					 <input type="hidden" id="num_meds_selected">
					 <input type="hidden" id="qty_selected">
					 <input type="hidden" id="dossage">
					 <input type="hidden" id="intake">
				</tbody>
			</table>
			</div>
			<?php
		}
		else{
			echo 0;
		}
	break;

	case 'show_medicine':
		$data = array();
		$sql = "SELECT * from  tbl_medicine where is_delete is null order by brand_name,generic_name";
		$result = fetch_record($con,$data,$sql);

		while ($row = $result->fetch()) {
			?>
			<tr id="data_doctor_<?php echo $row['medicine_id'] ?>">
				<td class="text-capitalize" nowrap><?php echo $row['brand_name'] ?></td>
				<td class="" nowrap><?php echo $row['generic_name'] ?></td>
				<td nowrap><?php echo $row['qty'] ?></td>
				<td nowrap><?php echo $row['dosage'] ?></td>
				<td nowrap><?php echo $row['form_type'] ?></td>
				<td nowrap><?php echo $row['supplier'] ?></td>
				<td ><?php echo $row['illness'] ?></td>
				<td class="text-center" nowrap>
					<button class="btn btn-info" data-toggle="dropdown"><i class="fa fa-user-cog"></i></button>
					<div class="dropdown-menu">
					<button class="dropdown-item" onclick="edit_meds(); add_edit_meds('<?php echo $row['medicine_id'] ?>','<?php echo $row['generic_name'] ?>','<?php echo $row['qty'] ?>','<?php echo $row['dosage'] ?>','<?php echo $row['form_type'] ?>','<?php echo $row['brand_name'] ?>','<?php echo $row['supplier'] ?>','<?php echo $row['illness'] ?>')"><i class="fa fa-edit text-success"></i> Edit</button>
					<button class="dropdown-item" onclick="delete_med('<?php echo $row['medicine_id'] ?>','<?php echo $row['brand_name'] ?>');"><i class="fa fa-trash text-danger"></i> Delete</button>
					</div>
					
				</td>
			</tr>
			<?php
		}
	break;

	case 'show_out_medicine':
		$data = array();
		$sql = "SELECT a.*,b.*,sum(a.qty_reco) as total_qty_less from tbl_medical_med_reco  a left join tbl_medicine b on a.medicine_id=b.medicine_id where b.is_delete is null group by b.medicine_id order by b.brand_name,b.generic_name";
		$result = fetch_record($con,$data,$sql);

		while ($row = $result->fetch()) {
			?>
			<tr id="data_doctor_<?php echo $row['medicine_id'] ?>">
				<td class="text-capitalize" nowrap><?php echo $row['brand_name'] ?></td>
				<td class="" nowrap><?php echo $row['generic_name'] ?></td>
				<td nowrap><?php echo $row['total_qty_less'] ?></td>
				<td nowrap><?php echo $row['dosage'] ?></td>
				<td nowrap><?php echo $row['form_type'] ?></td>
				<td nowrap><?php echo $row['supplier'] ?></td>
				<td nowrap><?php echo $row['illness'] ?></td>
				<!-- <td class="text-center" nowrap> -->
					<!-- <button class="btn btn-info" data-toggle="dropdown"><i class="fa fa-user-cog"></i></button> -->
					<!-- <div class="dropdown-menu"> -->
					<!-- <button class="dropdown-item" onclick="edit_meds(); add_edit_meds('<?php echo $row['medicine_id'] ?>','<?php echo $row['generic_name'] ?>','<?php echo $row['qty'] ?>','<?php echo $row['dosage'] ?>','<?php echo $row['form_type'] ?>','<?php echo $row['brand_name'] ?>','<?php echo $row['supplier'] ?>','<?php echo $row['illness'] ?>')"><i class="fa fa-edit text-success"></i> Edit</button> -->
					<!-- <button class="dropdown-item" onclick="delete_med('<?php echo $row['medicine_id'] ?>','<?php echo $row['brand_name'] ?>');"><i class="fa fa-trash text-danger"></i> Delete</button> -->
					<!-- </div> -->
					
				<!-- </td> -->
			</tr>
			<?php
		}
	break;

	case 'show_logs':
		$data = array();
		$sql = "SELECT a.*,b.* from  tbl_logs a left join tbl_account b on a.account_id=b.account_id where a.is_delete is null order by a.act_id desc";
		$result = fetch_record($con,$data,$sql);

		while ($row = $result->fetch()) {
			?>
			<tr id="data_doctor_<?php echo $row['act_id'] ?>">
				<td class="text-capitalize"><?php echo $row['fn'].' '.$row['ln'] ?> <?php echo $row['message'] ?></td>
			</tr>
			<?php
		}
	break;

	case 'show_doctor':
		$data = array();
		$sql = "SELECT * from  tbl_account where user_type=1 and is_delete is null order by ln,fn,mn";
		$result = fetch_record($con,$data,$sql);

		while ($row = $result->fetch()) {
			$mname = (!empty($row['mn'])) ? $row['mn'][0] : '';
			$name = $row['ln'].', '.$row['fn'].' '.$mname.'.'; 
			?>
			<tr id="data_doctor_<?php echo $row['account_id'] ?>">
				<td class="text-capitalize" nowrap><?php echo $name ?></td>
				<td class="text-capitalize" nowrap><?php echo $row['username'] ?></td>
				<td nowrap><?php echo $row['gender'] ?></td>
				<td class="text-center" nowrap>

					<?php if ($auth['user_type'] == 4): ?>
					
					<?php else: ?>
						<button class="btn btn-info" data-toggle="dropdown"><i class="fa fa-user-cog"></i></button>
						<div class="dropdown-menu">
						<button class="dropdown-item" onclick="edit_account(); $('#add_what').text('Edit Doctor'); edit_clear_account('<?php echo $row['account_id'] ?>','<?php echo $row['fn'] ?>','<?php echo $row['mn'] ?>','<?php echo $row['ln'] ?>','<?php echo $row['gender'] ?>','<?php echo $row['username'] ?>','','','1','<?php echo $row['license_number']?>','<?php echo $row['s2_number']?>','<?php echo $row['date_issue']?>','<?php echo $row['date_expiry']?>','<?php echo $row['ptr_number']?>','<?php echo $row['question_1']?>','<?php echo $row['answer_1']?>','<?php echo $row['question_2']?>','<?php echo $row['answer_2']?>')"><i class="fa fa-edit text-success"></i> Edit</button>
						<button class="dropdown-item" onclick="delete_doctor('<?php echo $row['account_id'] ?>','<?php echo $name ?>');"><i class="fa fa-trash text-danger"></i> Delete</button>
						</div>
					<?php endif ?>
					
				</td>
			</tr>
			<?php
		}
	break;

	case 'show_nurse':
		$data = array();
		$sql = "SELECT * from  tbl_account where user_type=2 and is_delete is null order by ln,fn,mn";
		$result = fetch_record($con,$data,$sql);

		while ($row = $result->fetch()) {
			$mname = (!empty($row['mn'])) ? $row['mn'][0] : '';
			$name = $row['ln'].', '.$row['fn'].' '.$mname.'.'; 
			?>
			<tr id="data_doctor_<?php echo $row['account_id'] ?>">
				<td class="text-capitalize" nowrap><?php echo $name ?></td>
				<td class="text-capitalize" nowrap><?php echo $row['username'] ?></td>
				<td nowrap><?php echo $row['gender'] ?></td>
				<td class="text-center" nowrap>

					<?php if ($auth['user_type'] == 4): ?>
					
					<?php else: ?>
						<button class="btn btn-info" data-toggle="dropdown"><i class="fa fa-user-cog"></i></button>
						<div class="dropdown-menu">
						<button class="dropdown-item" onclick="edit_account(); $('#add_what').text('Edit Nurse');  edit_clear_account('<?php echo $row['account_id'] ?>','<?php echo $row['fn'] ?>','<?php echo $row['mn'] ?>','<?php echo $row['ln'] ?>','<?php echo $row['gender'] ?>','<?php echo $row['username'] ?>','','','2''<?php echo $row['license_number']?>','<?php echo $row['s2_number']?>','<?php echo $row['date_issue']?>','<?php echo $row['date_expiry']?>','<?php echo $row['ptr_number']?>','<?php echo $row['question_1']?>','<?php echo $row['answer_1']?>','<?php echo $row['question_2']?>','<?php echo $row['answer_2']?>')"><i class="fa fa-edit text-success"></i> Edit</button>
						<button class="dropdown-item" onclick="delete_doctor('<?php echo $row['account_id'] ?>','<?php echo $name ?>');"><i class="fa fa-trash text-danger"></i> Delete</button>
						</div>
					<?php endif ?>
				</td>
			</tr>
			<?php
		}
	break;


	case 'show_parma':
		$data = array();
		$sql = "SELECT * from  tbl_account where user_type=3 and is_delete is null order by ln,fn,mn";
		$result = fetch_record($con,$data,$sql);

		while ($row = $result->fetch()) {
			$mname = (!empty($row['mn'])) ? $row['mn'][0] : '';
			$name = $row['ln'].', '.$row['fn'].' '.$mname.'.'; 
			?>
			<tr id="data_doctor_<?php echo $row['account_id'] ?>">
				<td class="text-capitalize"><?php echo $name ?></td>
				<td class="text-capitalize"><?php echo $row['username'] ?></td>
				<td><?php echo $row['gender'] ?></td>
				<td class="text-center">

					<?php if ($auth['user_type'] == 4): ?>
					
					<?php else: ?>
							<button class="btn btn-info" data-toggle="dropdown"><i class="fa fa-user-cog"></i></button>
							<div class="dropdown-menu">
							<button class="dropdown-item" onclick="edit_account(); $('#add_what').text('Edit Pharmacist');  edit_clear_account('<?php echo $row['account_id'] ?>','<?php echo $row['fn'] ?>','<?php echo $row['mn'] ?>','<?php echo $row['ln'] ?>','<?php echo $row['gender'] ?>','<?php echo $row['username'] ?>','','','3''<?php echo $row['license_number']?>','<?php echo $row['s2_number']?>','<?php echo $row['date_issue']?>','<?php echo $row['date_expiry']?>','<?php echo $row['ptr_number']?>','<?php echo $row['question_1']?>','<?php echo $row['answer_1']?>','<?php echo $row['question_2']?>','<?php echo $row['answer_2']?>')"><i class="fa fa-edit text-success"></i> Edit</button>
							<button class="dropdown-item" onclick="delete_doctor('<?php echo $row['account_id'] ?>','<?php echo $name ?>');"><i class="fa fa-trash text-danger"></i> Delete</button>
							</div>
					<?php endif ?>
					
				</td>
			</tr>
			<?php
		}
	break;


	case 'show_record_officer':
		$data = array();
		$sql = "SELECT * from  tbl_account where user_type=4 and is_delete is null order by ln,fn,mn";
		$result = fetch_record($con,$data,$sql);

		while ($row = $result->fetch()) {
			$mname = (!empty($row['mn'])) ? $row['mn'][0] : '';
			$name = $row['ln'].', '.$row['fn'].' '.$mname.'.'; 
			?>
			<tr id="data_doctor_<?php echo $row['account_id'] ?>">
				<td class="text-capitalize"><?php echo $name ?></td>
				<td class="text-capitalize"><?php echo $row['username'] ?></td>
				<td><?php echo $row['gender'] ?></td>
				<td class="text-center">

					<?php if ($auth['user_type'] == 4): ?>
					
					<?php else: ?>
						<button class="btn btn-info" data-toggle="dropdown"><i class="fa fa-user-cog"></i></button>
						<div class="dropdown-menu">
						<button class="dropdown-item" onclick="edit_account(); $('#add_what').text('Edit Record Officer');  edit_clear_account('<?php echo $row['account_id'] ?>','<?php echo $row['fn'] ?>','<?php echo $row['mn'] ?>','<?php echo $row['ln'] ?>','<?php echo $row['gender'] ?>','<?php echo $row['username'] ?>','','','3','<?php echo $row['license_number']?>','<?php echo $row['s2_number']?>','<?php echo $row['date_issue']?>','<?php echo $row['date_expiry']?>','<?php echo $row['ptr_number']?>','<?php echo $row['question_1']?>','<?php echo $row['answer_1']?>','<?php echo $row['question_2']?>','<?php echo $row['answer_2']?>')"><i class="fa fa-edit text-success"></i> Edit</button>
						<button class="dropdown-item" onclick="delete_doctor('<?php echo $row['account_id'] ?>','<?php echo $name ?>');"><i class="fa fa-trash text-danger"></i> Delete</button>
						</div>
					<?php endif ?>
					
				</td>
			</tr>
			<?php
		}
	break;

	case 'show_illness':
		$data = array();
		$sql = "SELECT * from tbl_illness where is_delete is null order by illness_name";
		$result = fetch_record($con,$data,$sql);
		$arr = array();

		while ($row = $result->fetch()) {
			$illness_id = $row['illness_id'];
			$arr = [];
			$data_symp = array('illness_id' => $illness_id);
			$sql_symp = "SELECT * from tbl_illness_symptoms where illness_id=:illness_id order by symptoms_name";
			$result_symp = fetch_record($con,$data_symp,$sql_symp);
			while ($roww = $result_symp->fetch()) {
				$arr[] = $roww['symptoms_name'];
			}

			$sympt = implode(',', $arr);
			?>
			<tr id="data_illness_<?php echo $row['illness_id'] ?>" onclick="on_edit_illness('Edit Illness'); edit_clear_illness('<?php echo $row['illness_id'] ?>','<?php echo $row['illness_name'] ?>','<?php echo $sympt ?>')">
				<td class="text-capitalize"><?php echo $row['illness_name'] ?></td>
				<td class="text-capitalize"><?php echo $sympt ?></td>
			</tr>
			<?php
		}
	break;


	case 'delete_med':
		$profid = $_POST['id'];
		$names = $_POST['name'];

		$data = array('prof_id' => $profid);
		$sql = "UPDATE tbl_medicine set is_delete='$today' where medicine_id=:prof_id";

		logs($con,'Deleted medicine "'.$names.'" on '.$today,$id,$today);
		echo save($con,$data,$sql);
	break;

	case 'delete_patient':
		$profid = $_POST['id'];
		$names = $_POST['name'];

		$data = array('prof_id' => $profid);
		$sql = "UPDATE tbl_patient set is_delete='$today' where patient_id=:prof_id";

		logs($con,'Deleted Patient "'.$names.'" on '.$today,$id,$today);
		echo save($con,$data,$sql);
	break;

	case 'delete_user':
		$profid = $_POST['id'];
		$names = $_POST['name'];

		$data = array('prof_id' => $profid);
		$sql = "UPDATE tbl_account set is_delete='$today' where account_id=:prof_id";

		logs($con,'Deleted Account of "'.$names.'" on '.$today,$id,$today);
		echo save($con,$data,$sql);
	break;

	case 'delete_check_ini':
		$profid = $_POST['id'];
		$names = $_POST['name'];

		$data = array('prof_id' => $profid);
		$sql = "DELETE FROM tbl_medical where medical_id=:prof_id";

		logs($con,'Deleted Account of "'.$names.'" on '.$today,$id,$today);
		echo save($con,$data,$sql);
	break;


	case 'add_account':
		$user_id = (isset($_POST['user_id'])) ? $_POST['user_id'] : '';
		$fn = (isset($_POST['fn'])) ? $_POST['fn'] : '';
		$mn = (isset($_POST['mn'])) ? $_POST['mn'] : '';
		$ln = (isset($_POST['ln'])) ? $_POST['ln'] : '';
		$gender = (isset($_POST['gender'])) ? $_POST['gender'] : '';
		$username = (isset($_POST['username'])) ? $_POST['username'] : '';
		$pwd = (isset($_POST['password'])) ? $_POST['password'] : '';
		$user_type = (isset($_POST['user_type'])) ? $_POST['user_type'] : '';

		$license_number = (isset($_POST['license_number'])) ? $_POST['license_number'] : '';
		$s2_number = (isset($_POST['s2_number'])) ? $_POST['s2_number'] : '';
		$date_issue = (isset($_POST['date_issue'])) ? $_POST['date_issue'] : '';
		$date_expiry = (isset($_POST['date_expiry'])) ? $_POST['date_expiry'] : '';
		$ptr_number = (isset($_POST['ptr_number'])) ? $_POST['ptr_number'] : '';
		$question_1 = (isset($_POST['question_1'])) ? $_POST['question_1'] : '';
		$answer_1 = (isset($_POST['answer_1'])) ? $_POST['answer_1'] : '';
		$question_2 = (isset($_POST['question_2'])) ? $_POST['question_2'] : '';
		$answer_2 = (isset($_POST['answer_2'])) ? $_POST['answer_2'] : '';


		$password = password_hash($pwd, PASSWORD_DEFAULT);

		$ver_data = array('username' => $username , 'account_id' => $user_id);
		$sql_ver = "SELECT * from tbl_account where username=:username and account_id!=:account_id";

		if (verify_record($con,$ver_data,$sql_ver) > 0) {
			echo 202;
		}else{
			if (!empty($user_id)) {
				$sql_update ="UPDATE tbl_account set fn=:fn,mn=:mn,ln=:ln,gender=:gender,username=:username,password=:password,user_type=:user_type,license_number=:license_number,s2_number=:s2_number,date_issue=:date_issue,date_expiry=:date_expiry,ptr_number=:ptr_number,question_1=:question_1,answer_1=:answer_1,question_2=:question_2,answer_2=:answer_2 where account_id=:account_id";
				$update_data = array(
					'account_id' => $user_id,
					'fn' => $fn,
					'mn' => $mn,
					'ln' => $ln,
					'gender' => $gender,
					'username' => $username,
					'password' => $password,
					'user_type' => $user_type,
					'license_number' => $license_number,
					's2_number' => $s2_number,
					'date_issue' => $date_issue,
					'date_expiry' => $date_expiry,
					'ptr_number' => $ptr_number,
					'question_1' => $question_1,
					'answer_1' => $answer_1,
					'question_2' => $question_2,
					'answer_2' => $answer_2
				);

				if (save($con,$update_data,$sql_update) > 0) {
					echo 2;
					logs($con,'Edited Account of "'.$fn.' '.$ln.'" as '.user_position()[$user_type].' on '.$today,$id,$today);
				}
			}else{
				$sql_insert ="INSERT INTO tbl_account(license_number,s2_number,date_issue,date_expiry,ptr_number,fn,mn,ln,gender,username,password,user_type,question_1,answer_1,question_2,answer_2) VALUES(:license_number,:s2_number,:date_issue,:date_expiry,:ptr_number,:fn,:mn,:ln,:gender,:username,:password,:user_type,:question_1,:answer_1,:question_2,:answer_2)";
				$insert_data = array(
					'fn' => $fn,
					'mn' => $mn,
					'ln' => $ln,
					'gender' => $gender,
					'username' => $username,
					'password' => $password,
					'user_type' => $user_type,
					'license_number' => $license_number,
					's2_number' => $s2_number,
					'date_issue' => $date_issue,
					'date_expiry' => $date_expiry,
					'ptr_number' => $ptr_number,
					'question_1' => $question_1,
					'answer_1' => $answer_1,
					'question_2' => $question_2,
					'answer_2' => $answer_2
				);

				if (save($con,$insert_data,$sql_insert) > 0) {
					echo 1;
					logs($con,'Added Account of "'.$fn.' '.$ln.'" as '.user_position()[$user_type].' on '.$today,$id,$today);

				}
			}
		}

	break;
	
	default:
		die('Action '.$action.' is not exist !');
	break;
}

 ?>