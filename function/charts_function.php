<?php 

function get_data_charts($con,$param_date){
		$arrays = array();
		$data = array();
		
		$sql = "SELECT diagnoses_result,count(diagnoses_result),date_entry from tbl_medical where is_finish = 1 group by diagnoses_result";
		$result = fetch_record($con,$data,$sql);


		$sql_count = "SELECT count(diagnoses_result),date_entry from tbl_medical where is_finish = 1 and DATE_FORMAT(date_entry,'%Y %M') = '$param_date' group by DATE_FORMAT(date_entry,'%Y %M')";
		$total_result = fetch_record($con,$data,$sql_count);
		$rows = $total_result->fetch();

		// echo $rows['count(diagnoses_result)'];

		if ($rows['count(diagnoses_result)'] > 0) {
			while ($row = $result->fetch()) {
				$per = get_percentage($rows['count(diagnoses_result)'],$row['count(diagnoses_result)']);

				if (date('F Y',strtotime($param_date)) === date('F Y',strtotime($row['date_entry']))) {
					 $arrays[] ='{name:\''.$row['diagnoses_result'].'\', y:'.$per.'}'; 
				}
			}

			

			?>

			 <script type="text/javascript">
			    // Build the chart
			    Highcharts.chart('container', {
			      chart: {
			        plotBackgroundColor: null,
			        plotBorderWidth: null,
			        plotShadow: false,
			        type: 'pie'
			      },
			      title: {
			        text: 'Estimated Illnesses of <?php echo date('F Y',strtotime($param_date)) ?> <br> Total Patient <?php echo $rows['count(diagnoses_result)'] ?>'
			      },
			      tooltip: {
			        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			      },
			      plotOptions: {
			        pie: {
			          allowPointSelect: true,
			          cursor: 'pointer',
			          dataLabels: {
			            enabled: true,
			            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
			            connectorColor: 'silver'
			          }
			        }
			      },
			      series: [{
			        name: 'Illness',
			        data: <?php echo str_replace('"', "", json_encode($arrays)); ?>
			      }]
			    });
			  
			 </script>
			 <div id="container" style="min-width: 100%; height: 400px; max-width: 600px; margin: 0 auto"></div>
			<?php

		}else{
			echo 0;
		}
}

 ?>