<?php 
session_start();
if (isset($_SESSION['account_id'])) {
  @header('location:admin');
}

 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>DIAGNOSIS DECISION SUPPORT SYSTEM</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="plugins/toastr/toastr.min.css">
</head>

<style type="text/css">
  @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,600,300,700);
@import url(https://fonts.googleapis.com/css?family=Roboto+Slab:400,100);

body { 
  font-family: 'Open Sans', sans-serif;
  background-color: #f4f4f4;
  margin: 0;
  padding: 0;
}

header { 
  background-color: #fff; 
  padding: 5px 15px 0px;
  border-bottom: 1px solid #ccc;
  margin-bottom: 25px;
}

h1 {
  font-size: 28px; 
  font-weight: 700; 
  margin: 0px; 
  padding: 0px 0px 10px 0; 
  border-bottom : 1px solid #ccc;
}
h4 { font-size : 16px; font-weight: 300; margin-top: 5px; line-height: 22px; }
h4 > * { display: inline-block; vertical-align: top; }

fieldset { 
  border: 1px solid #ccc; 
  padding: 15px; 
  max-width: 345px;
  background-color: #fff;
  border-radius: 5px;
}

section { padding: 0 15px; }

.CaptchaWrap { position: relative; }
.CaptchaTxtField { 
  border-radius: 5px; 
  border: 1px solid #ccc; 
  display: block;  
  box-sizing: border-box;
}

#UserCaptchaCode { 
  padding: 5px; 
  outline: none; 
  font-size: 18px; 
  font-weight: normal; 
  font-family: 'Open Sans', sans-serif;
  width: 320px;
}
#CaptchaImageCode { 
  text-align:center;
  margin-top: 5px;
  padding: 0px 0;
  width: 300px;
  overflow: hidden;
}

.capcode { 
  font-size: 46px; 
  display: block; 
  -moz-user-select: none;
  -webkit-user-select: none;
  user-select: none; 
  cursor: default;
  letter-spacing: 1px;
  color: #ccc;
  font-family: 'Roboto Slab', serif;
  font-weight: 100;
  font-style: italic;
}

.ReloadBtn { 
  background:url('https://cdn3.iconfinder.com/data/icons/basic-interface/100/update-64.png') left top no-repeat;   
  background-size : 100%;
  width: 20px; 
  height: 32px;
  border: 0px; outline none;
  position: absolute; 
  bottom: 30px;
  left : 310px;
  outline: none;
  cursor: pointer; /**/
}
.btnSubmit {
  margin-top: 15px;
  border: 0px;
  padding: 5px 20px; 
  border-radius: 5px;
  font-size: 18px;
  background-color: #1285c4;
  color: #fff;
  cursor: pointer;
}

.error { 
  color: red; 
  font-size: 12px; 
  display: none; 
}
.success {
  color: green;
  font-size: 18px;
  margin-bottom: 15px;
  display: none;
}
.login-board{
  padding-left:490px;
  padding-right:490px;
}
</style>
<body class="hold-transition login-page bg-info mt-1">
<div class="login-box">
  <div class="login-logo p-0 text-center mt-0">
    <a href="index.php"><b><img src="img/logo.png" class="img-fluid shadow rounded-circle" width="140"></b> </a>
    <p class="h5" style="text-shadow: 1px 1px 1px #555;">City Health Office’s Common Illnesses Diagnosis Decision Support System</p>
  </div>
  <!-- /.login-logo -->
  <div class="card shadow">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Log in</p>

      <form action="#" method="post" id="formLogin">
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Username" id="username" autocomplete="off" onkeypress="return noenter()">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password" id="password" autocomplete="off" onkeypress="return noenter()">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
                <span id="SuccessMessage" class="success text-sm">You have successfully entered the captcha.</span>
                  <div class='CaptchaWrap'>
                    <div id="CaptchaImageCode" class="CaptchaTxtField">
                      <canvas id="CapCode" class="capcode" width="300" height="50"></canvas>
                    </div> 
                     <input type="button" class="ReloadBtn" onclick='CreateCaptcha();'>
                  </div>
                  <br>

                  <div class="CaptchaWrap">
                     <input type="text" id="UserCaptchaCode" autocomplete="off" class="CaptchaTxtField" placeholder='Enter Captcha - Case Sensitive' onkeypress="return noenter()">
                     <span id="WrongCaptchaError" class="error"></span>
                  </div>
               
              
             <br>
          </div>
          <div class="col-sm-5">
              <a href="forgot_password.php" class="text-sm">Forgot Password?</a>
             
          </div>

          <div class="col-sm-3">
            <span class="btn btn-success btn-block btn-flat btn-sm" id="btn_cap" onclick="CheckCaptcha();">Verify</span>
          </div>
          <!-- /.col -->
          <div class="col-sm-4">
            <button type="submit" class=" btn-sm btn btn-primary btn-block btn-flat" id="loginbtn" disabled="">Log In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <!-- /.social-auth-links -->

    </div>
    <!-- /.login-card-body -->
  </div>
</div>

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="plugins/toastr/toastr.min.js"></script>

<script src="dist/js/tools.js"></script>
<script src="dist/js/index.js"></script>

</body>
</html>


<script type="text/javascript">
function noenter() {
  return !(window.event && window.event.keyCode == 13); }
</script>