const url = '../function/function.php';
var is_edit = 0;
var field_count = 0;
var fields = [];


 function check_up(id,name){
    $("#check_up_modal").modal({'backdrop':'static'});
    $("#name_patient").text(name);
    $("#patient_id").val(id);
  }


 function count_patient(){

    var mydata = 'action=count_patient';
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(data){
        $("#finish_today").text(data.trim());
      }
    });
  }

function patient_todays(){

    var mydata = 'action=patient_today';
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(data){
        $("#patient_today").text(data.trim());
      }
    });
  }


  count_patient();
  patient_todays();

  setInterval(function(){
    count_patient();
    patient_todays();
  },4000);

// Admin


// Check up
function edit_clear_checkup(check_id,patient_id,height,weight,bp,symptoms){
    $("#check_id").val(check_id);
    $("#patient_id").val(patient_id);
    $("#height").val(height);
    $("#weight").val(weight);
    $("#bp").val(bp);
    $("#symptoms").val(symptoms);
  }


  function clear_form_medical(){
    document.getElementById('form_medical').reset();
  }

  function add_check_up(){

    var data_all_form = '&'+$("#form_medical").serialize();

    var check_id = $("#check_id");
    var height = $("#height");
    var weight = $("#weight");
    var bp = $("#bp");
    var symptoms = $("#symptoms");
    var blood_sugar = $("#blood_sugar");
    var urine = $("#urine");
    var illness_history = $("#illness_history");
    var medication_history = $("#medication_history");
    var temp = $("#temp");



    if (height.val() == "" || height.val() == null) {
      height.focus();
      twal("Height is required!","error");
    }
    else if (weight.val() == "" || weight.val() == null) {
      weight.focus();
      twal("Weight is required!","error");
    }
    else if (symptoms.val() == "" || symptoms.val() == null) {
      symptoms.focus();
      twal("Symptoms is required!","error");
    }else{
    

    // var add_temp = '&temp=' + temp.val();
    // var new_data = '&blood_sugar=' + blood_sugar.val() +'&urine=' + urine.val() +'&illness_history=' + illness_history.val() +'&medication_history=' + medication_history.val();

    // var mydata = 'action=add_check_up' + '&check_id=' + check_id.val() + '&height=' + height.val() + '&weight=' + weight.val() + '&bp=' + bp.val() + '&symptoms=' + symptoms.val() + '&patient_id=' + $("#patient_id").val() + '&patient_name=' + $("#name_patient").text();
    var mydata = 'action=add_check_up' + data_all_form + '&symptoms=' + symptoms.val() + '&patient_name=' + $("#name_patient").text();

    // alert(mydata);
      $.ajax({
        type:"POST",
        url:url,
        data:mydata,
        cache:false,
        success:function(data){
          if (data.trim() == 1) {
            twal("Check up has been added!","success");
            // edit_clear_checkup('','','','','','');
            clear_form_medical();
            $("#check_up_modal").modal('hide');
          }else if (data.trim() == 2) {
            twal("Check up has been edited!","success");
            // edit_clear_checkup('','','','','','');
            clear_form_medical();
            $("#check_up_modal").modal('hide');
          }else{
            console.log(data.trim());
          }
        }
      });
    }
  }

  function add_edit_meds(medicine_id,gen_name,qty,dosage,form_type,brand,supplier,illness){
    $("#illness").tagsinput('removeAll');
    $("#medicine_id").val(medicine_id);
    $("#gen_name").val(gen_name);
    $("#qty").val(qty);
    $("#dosage").val(dosage);
    $("#form_type").val(form_type);
    $("#brand").val(brand);
    $("#supplier").val(supplier);
    $('#illness').tagsinput('add',illness);
    // alert(illness);
  }

  // Check up


  // Meds
  function edit_meds(){
    $("#add_medicine").modal({'backdrop':'static'});
    $("#title_med").text('Edit Medicine');
  }

  function add_meds(){
    var medicine_id = $("#medicine_id");
    var gen_name = $("#gen_name");
    var qty = $("#qty");
    var dosage = $("#dosage");
    var form_type = $("#form_type");
    var brand = $("#brand");
    var supplier = $("#supplier");
    var illness = $("#illness");

    if (brand.val() == "") {
      brand.focus();
      twal("brand is required!","error");
    }
    else if (gen_name.val() == "") {
      gen_name.focus();
      twal("gen_name is required!","error");
    }
    else if (qty.val() == "") {
      qty.focus();
      twal("qty is required!","error");
    }
    else if (dosage.val() == "" || dosage.val() == null) {
      dosage.focus();
      twal("dossage is required!","error");
    }
    else if (form_type.val() == "" || form_type.val() == null) {
      form_type.focus();
      twal("form/type is required!","error");
    }
    else if (supplier.val() == "") {
      supplier.focus();
      twal("supplier is required!","error");
    }
    else if (illness.val() == "") {
      illness.focus();
      twal("illness is required!","error");
    }else{
      var additional = '&dosage=' + dosage.val() + '&form_type=' + form_type.val();
      var mydata = 'action=save_meds' + '&medicine_id=' + medicine_id.val() +'&gen_name=' + gen_name.val() +'&qty=' + qty.val() +'&brand=' + brand.val() +'&supplier=' + supplier.val() +'&illness=' + illness.val();
      // console.log(mydata);
      $.ajax({
        type:"POST",
        url:url,
        data:mydata + additional,
        cache:false,
        success:function(data){
          if (data.trim() == 1) {
            twal("Medicine has been saved!","success");
           add_edit_meds('','','','','','','','');
            $("#illness").tagsinput('removeAll');
            show_medicine();
            $("#add_medicine").modal('hide');
             $("#title_med").text('Add Medicine');
          }else if (data.trim() == 2) {
            twal("Medicine has been updated!","success");
            add_edit_meds('','','','','','','','');
            $("#illness").tagsinput('removeAll');
            show_medicine();
            $("#add_medicine").modal('hide');
             $("#title_med").text('Add Medicine');
          }else{
            console.log(data.trim());
            twal("Please check your server!","error");
          }
        }
      });
    }

  }


  function delete_med(id,name){
  swal({
      title: "Are you sure?",
      text: "Do you want to delete "+name+" ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: true
    },
    function(){
      $.ajax({
        type:"POST",
        url:url,
        data:'action=delete_med' + '&id=' + id + '&name=' + name,
        cache:false,
        success:function(data){
          if (data.trim() == 1) {
             // show_student();
             $("#data_doctor_"+id).hide('fast');
             twal(name+" has been deleted!","success");
          }else{
            console.log(data);
          }
        }
      });
    });
}

function delete_patient_ini(id,name){
  swal({
      title: "Are you sure?",
      text: "Do you want to delete "+name+" ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: true
    },
    function(){
      $.ajax({
        type:"POST",
        url:url,
        data:'action=delete_check_ini' + '&id=' + id + '&name=' + name,
        cache:false,
        success:function(data){
          if (data.trim() == 1) {
             // show_student();
             $("#data_doctor_"+id).hide('fast');
             twal(name+" has been deleted!","success");
          }else{
            console.log(data);
          }
        }
      });
    });
}


// Meds


// Account
  function delete_user(id,name){
  swal({
      title: "Are you sure?",
      text: "Do you want to delete "+name+" ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: true
    },
    function(){
      $.ajax({
        type:"POST",
        url:url,
        data:'action=delete_user' + '&id=' + id + '&name=' + name,
        cache:false,
        success:function(data){
          if (data.trim() == 1) {
          	 // show_student();
             $("#data_student_"+id).hide('fast');
             twal(name+" has been deleted!","success");
          }else{
            console.log(data);
          }
        }
      });
    });
}



// Patient
function delete_patient(id,name){
   swal({
      title: "Are you sure?",
      text: "Do you want to delete "+name+" ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: true
    },
    function(){
      $.ajax({
        type:"POST",
        url:url,
        data:'action=delete_patient' + '&id=' + id + '&name=' + name,
        cache:false,
        success:function(data){
          if (data.trim() == 1) {
             // show_student();
             $("#data_doctor_"+id).hide('fast');
             twal(name+" has been deleted!","success");
          }else{
            console.log(data);
          }
        }
      });
    });
}


// Account


  function edit_clear_account(user_id,fn,mn,ln,gender,username,password,c_password,user_type,license_number,s2_number,date_issue,date_expiry,ptr_number,question_1,answer_1,question_2,answer_2){
    $("#user_id").val(user_id);
    $("#fn").val(fn);
    $("#mn").val(mn);
    $("#ln").val(ln);
    $("#gender_account").val(gender);
    $("#username").val(username);
    $("#password").val(password);
    $("#c_password").val(c_password);
    $("#user_type").val(user_type);
    $("#license_number").val(license_number);
    $("#s2_number").val(s2_number);
    $("#date_issue").val(date_issue);
    $("#date_expiry").val(date_expiry);
    $("#ptr_number").val(ptr_number);
    $("#question_1").val(question_1);
    $("#answer_1").val(answer_1);
    $("#question_2").val(question_2);
    $("#answer_2").val(answer_2);

    if (user_type == 1) {
      $("#doctor_forms").show('fast');
    }else{
      $("#doctor_forms").hide('fast');
    }

  }


  function new_account(usertype,add_what){
    is_edit = 0;

    if (usertype == 1) {
      $("#doctor_forms").show('fast');
    }else{
      $("#doctor_forms").hide('fast');
    }

    $("#user_type").val(usertype);
    $("#add_what").text(add_what);
    $("#add_account").modal({'backdrop' : 'static'});
  }

  function show_data(param){
    if (param == 1) {
      return show_doctor();
    }else if (param == 2) {
      show_nurse();
    }else if (param == 3) {
      show_parma();
    }else if (param == 4) {
      show_rec_officer();
    }
  }


  function add_account(){
    var user_id = $("#user_id");
    var fn = $("#fn");
    var mn = $("#mn");
    var ln = $("#ln");
    var gender = $("#gender_account");
    var username = $("#username");
    var password = $("#password");
    var c_password = $("#c_password");
    var user_type = $("#user_type");

    var license_number = $("#license_number");
    var s2_number = $("#s2_number");
    var date_issue = $("#date_issue");
    var date_expiry = $("#date_expiry");
    var ptr_number = $("#ptr_number");
    var question_1 = $("#question_1");
    var answer_1 = $("#answer_1");
    var question_2 = $("#question_2");
    var answer_2 = $("#answer_2");


    var data_doctor = '&license_number=' + license_number.val() +'&s2_number=' + s2_number.val() +'&date_issue=' + date_issue.val() +'&date_expiry=' + date_expiry.val() +'&ptr_number=' + ptr_number.val() +'&question_1=' + question_1.val() +'&answer_1=' + answer_1.val() +'&question_2=' + question_2.val() +'&answer_2=' + answer_2.val();


  


  if (fn.val() == "" || fn.val() == null) {
    fn.focus();
    twal('Firstname is required!',"error");
  }
  else if (mn.val() == "" || mn.val() == null) {
    mn.focus();
    twal('Middlename is required!',"error");
  }
  else if (ln.val() == "" || ln.val() == null) {
    ln.focus();
    twal('Lastname is required!',"error");
  }
  else if (gender.val() == "" || gender.val() == null) {
    gender.focus();
    twal('Gender is required!',"error");
  }
  else if (license_number.val() == "" && user_type.val == 1  || license_number.val() == null && user_type.val == 1) {
    license_number.focus();
    twal('License number is required!',"error");

  }
  else if (s2_number.val() == "" && user_type.val == 1 || s2_number.val() == null && user_type.val == 1) {
    s2_number.focus();
    twal('S2 number is required!',"error");

  }
  else if (date_issue.val() == "" && user_type.val == 1 || date_issue.val() == null && user_type.val == 1) {
    date_issue.focus();
    twal('Date issue is required!',"error");

  }
  else if (date_expiry.val() == "" && user_type.val == 1 || date_expiry.val() == null && user_type.val == 1) {
    date_expiry.focus();
    twal('Date expiry is required!',"error");

  }
  else if (ptr_number.val() == "" && user_type.val == 1 || ptr_number.val() == null && user_type.val == 1) {
    ptr_number.focus();
    twal('PTR number is required!',"error");
  }
  else if (username.val() == "" || username.val() == null) {
    username.focus();
    twal('Username is required!',"error");
  }
  else if (password.val() == "" && is_edit == 0) {
    password.focus();
    twal('Confirm password!',"error");
  }
  else if (c_password.val() == "" && password.val() != "") {
    c_password.focus();
    twal('Confirm password!',"error");
  }
  else if (c_password.val() != password.val()) {
    c_password.focus();
    twal('Password doesn\'t match!',"error");
  }
  else if (question_1.val() == "" || question_1.val() == null) {
    question_1.focus();
    twal('Question 1 is required!',"error");

  }
  else if (answer_1.val() == "" || answer_1.val() == null) {
    answer_1.focus();
    twal('Answer 1 is required!',"error");

  }
  else if (question_2.val() == "" || question_2.val() == null) {
    question_2.focus();
    twal('Question 2 is required!',"error");

  }
  else if (answer_2.val() == "" || answer_2.val() == null) {
    answer_2.focus();
    twal('Answer 2 is required!',"error");

  }
  else{
    var mydata = 'action=add_account'+ '&user_id=' + user_id.val() +'&fn=' + fn.val() +'&mn=' + mn.val() +'&ln=' + ln.val() +'&gender=' + gender.val() + '&username=' + username.val() +'&password=' + password.val() +'&user_type=' + user_type.val();
     $.ajax({
          type:"POST",
          url:url,
          data:mydata + data_doctor,
          cache:false,
           beforeSend:function(){
              twal("Please Wait...!","info");
          },
          success:function(data){
            // alert(mydata);
            console.log(data.trim());
            if (data.trim() == 1) {
              show_data(user_type.val());
              edit_clear_account('','','','','','','','','','','','','','','','','','');

              twal("Account has been saved!","success");
              $("#add_account").modal('hide');
              // show_student();
              is_edit = 0;
            }else if (data.trim() == 2) {
              show_data(user_type.val());
             edit_clear_account('','','','','','','','','','','','','','','','','','');
              twal("Account has been updated!","success");
              $("#add_account").modal('hide');
              // show_student();
              is_edit = 0;
            }else if (data.trim() == 202) {
               twal("Username is already exist!","error");
               username.focus();
            }else{
              console.log(data.trim());
            }
          }

        });
  }
  }

   function edit_account(){
    is_edit = 1;
    $("#add_account").modal({'backdrop':'static'});
  }


// Account


// Displays
function show_logs(){
  $.ajax({
    type:"POST",
    url:url,
    data:'action=show_logs',
    cache:false,
    success:function(data){
       $("#data_logs").html(data);
       $("#tbl_logs").DataTable();
    }
  });
}

function show_patient(){
  $.ajax({
    type:"POST",
    url:url,
    data:'action=show_patient',
    cache:false,
    success:function(data){
      // console.log(data);
       $("#data_patient").html(data);
       $("#tbl_patient").DataTable();
    }
  });
}

function show_patient_ini(){
  $.ajax({
    type:"POST",
    url:url,
    data:'action=show_patient_ini',
    cache:false,
    success:function(data){
      // console.log(data);
       $("#data_patient").html(data);
       $("#tbl_patient").DataTable();
    }
  });
}

function show_check_up(){
  var mydata = '&date_check=' + $("#date_check").val() +'&illness='+$("#illness_type").val();
  $.ajax({
    type:"POST",
    url:url,
    data:'action=show_check_up' + mydata,
    cache:false,
    success:function(data){
      // console.log(data);
       $("#data_check").html(data);
       $("#tbl_check").DataTable();
    }
  });
}


function show_check_up_pending(){
    var mydata = '&date_check=' + $("#date_check").val();
   $.ajax({
    type:"POST",
    url:url,
    data:'action=show_check_up_pending' + mydata,
    cache:false,
    success:function(data){
      // console.log(data);
       $("#data_check").html(data);
       $("#tbl_check").DataTable();
    }
  });
}

  function show_doctor(){
  $.ajax({
    type:"POST",
    url:url,
    data:'action=show_doctor',
    cache:false,
    success:function(data){
       $("#data_doctor").html(data);
       $("#tbl_doctor").DataTable();
    }
  });
}

function show_medicine(){
  $.ajax({
    type:"POST",
    url:url,
    data:'action=show_medicine',
    cache:false,
    success:function(data){
       $("#data_medicine").html(data);
       $("#tbl_medicine").DataTable();
    }
  });
}

function show_illness(){
  $.ajax({
    type:"POST",
    url:url,
    data:'action=show_illness',
    cache:false,
    success:function(data){
       $("#data_illness").html(data);
       $("#tbl_illness").DataTable();
    }
  });
}



function show_out_medicine(){
  $.ajax({
    type:"POST",
    url:url,
    data:'action=show_out_medicine',
    cache:false,
    success:function(data){
       $("#data_medicine").html(data);
       $("#tbl_medicine").DataTable();
    }
  });
}

 function show_parma(){
  $.ajax({
    type:"POST",
    url:url,
    data:'action=show_parma',
    cache:false,
    success:function(data){
       $("#data_parma").html(data);
       $("#tbl_parma").DataTable();
    }
  });
}


 function show_rec_officer(){
  $.ajax({
    type:"POST",
    url:url,
    data:'action=show_record_officer',
    cache:false,
    success:function(data){
       $("#data_rec_officer").html(data);
       $("#tbl_rec_officer").DataTable();
    }
  });
}
 function show_nurse(){
  $.ajax({
    type:"POST",
    url:url,
    data:'action=show_nurse',
    cache:false,
    success:function(data){
       $("#data_nurse").html(data);
       $("#tbl_nurse").DataTable();
    }
  });
}

// Displays



 function delete_doctor(id,name){
  swal({
      title: "Are you sure?",
      text: "Do you want to delete "+name+" ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: true
    },
    function(){
      $.ajax({
        type:"POST",
        url:url,
        data:'action=delete_user' + '&id=' + id + '&name=' + name,
        cache:false,
        success:function(data){
          if (data.trim() == 1) {
             $("#data_doctor_"+id).hide('fast');
             twal(name+" has been deleted!","success");
          }else{
            console.log(data);
          }
        }
      });
    });
}


function change_password(){
    var o_pwd = $("#o_pwd");
    var pwd = $("#pwd");
    var c_pwd = $("#c_pwd");

    if (o_pwd.val() == "") {
      twal("Enter old password!","error");
      o_pwd.focus();
    }
    else if (pwd.val() == "") {
      twal("Enter new password!","error");
      pwd.focus();
    }
    else if (c_pwd.val() == "") {
      twal("Confirm password!","error");
      c_pwd.focus();
    }else if (c_pwd.val() != pwd.val()) {
      twal("Password Doesn\'t match!","error");
    }else{

      var mydata = 'action=change_password' + '&o_pwd=' + o_pwd.val() + '&pwd=' + pwd.val() + '&c_pwd=' + c_pwd.val();
      $.ajax({
        type:"POST",
        url:url,
        data:mydata,
        cache:false,
        success:function(data){
          if (data.trim() == 1) {
            twal("Password has been saved!","success");
            o_pwd.val("");
            pwd.val("");
            c_pwd.val("");
            $("#update_password").modal('hide');
          }else{
            twal("Incorrect Password!","error");
          }
        }
      });
    }
  }


  // Patient

  function get_list_province(region){
    mydata = 'action=get_list_province' + '&region=' + region;
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(data){
        $("#province").html(data);
        // alert(data);
      }
    });
  }

  function get_list_city(region,province){
    mydata = 'action=get_list_city' + '&region=' + region + '&province=' + province;
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(data){
        $("#city").html(data);
        // alert(data);
      }
    });
  }

  function get_list_barangay(region,province,city){
    mydata = 'action=get_list_barangay' + '&region=' + region + '&province=' + province + '&city=' + city;
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(data){
        $("#barangay").html(data);
        // alert(data);
      }
    });
  }


  function add_edit_patient(patient_id,patient_fn,patient_mn,patient_ln,bdate,patient_gender,region,province,city,barangay){
    // twal("Please Wait..","info");
    get_list_province(region);
    get_list_city(region,province);
    get_list_barangay(region,province,city);

    setTimeout(function(){
      // twal("Data is ready!","success");
      $("#patient_id").val(patient_id);
      $("#patient_fn").val(patient_fn);
      $("#patient_mn").val(patient_mn);
      $("#patient_ln").val(patient_ln);
      $("#bdate").val(bdate);
      $("#patient_gender").val(patient_gender);
      $("#region").val(region);
      $("#province").val(province);
      $("#city").val(city);
      $("#barangay").val(barangay);
    },500);
    
  }


  function edit_patient(){
    $("#add_patient").modal({'backdrop':'static'});
    $("#add_patient_title").text('Edit Patient');
  }

  function add_patient(param){
    var patient_id = $("#patient_id");
    var patient_fn = $("#patient_fn");
    var patient_mn = $("#patient_mn");
    var patient_ln = $("#patient_ln");
    var bdate = $("#bdate");
    var patient_gender = $("#patient_gender");
    var region = $("#region");
    var province = $("#province");
    var city = $("#city");
    var barangay = $("#barangay");

    var fullname = patient_fn.val() +' '+ patient_ln.val();

    if (patient_fn.val() == "" || patient_fn.val() == null) {
      patient_fn.focus();
      twal("Firstname is required!","error");
    }
    else if (patient_mn.val() == "" || patient_mn.val() == null) {
      patient_mn.focus();
      twal("Middlename is required!","error");
    }
    else if (patient_ln.val() == "" || patient_ln.val() == null) {
      patient_ln.focus();
      twal("Lastname is required!","error");
    }
    else if (bdate.val() == "" || bdate.val() == null) {
      bdate.focus();
      twal("Birthdate is required!","error");
    }
    else if (patient_gender.val() == "" || patient_gender.val() == null) {
      patient_gender.focus();
      twal("Gender is required!","error");
    }
    else if (region.val() == "" || region.val() == null) {
      region.focus();
      twal("Region is required!","error");
    }
    else if (province.val() == "" || province.val() == null) {
      province.focus();
      twal("Province is required!","error");
    }
    else if (city.val() == "" || city.val() == null) {
      city.focus();
      twal("City is required!","error");
    }
    else if (barangay.val() == "" || barangay.val() == null) {
      barangay.focus();
      twal("Barangay is required!","error");
    }else{

    var mydata = 'action=add_patient' + '&patient_id=' + patient_id.val() + '&patient_fn=' + patient_fn.val() + '&patient_mn=' + patient_mn.val() + '&patient_ln=' + patient_ln.val() + '&bdate=' + bdate.val() + '&patient_gender=' + patient_gender.val() + '&region=' + region.val() + '&province=' + province.val() + '&city=' + city.val() + '&barangay=' + barangay.val() + '&accept=' + param;
      
      $.ajax({
        type:"POST",
        url:url,
        data:mydata,
        cache:false,
        beforeSend:function(){
          twal("Please Wait!","info");
        },
        success:function(data){
          console.log(data.trim());
          if (data.trim() == 1) {
            twal("Patient has been added!","success");
            show_patient();
            add_edit_patient('','','','','','','','','','');
            $("#add_patient").modal('hide');
          }else if (data.trim() == 2) {
            twal("Patient has been edited!","success");
            show_patient();
            add_edit_patient('','','','','','','','','','');
            $("#add_patient").modal('hide');
          }else if (data.trim() == 123) {
            ask_new_record(fullname);
          }
          else {
            console.log(data.trim());
          }
        }
      }); 
    }
  }


   function ask_new_record(name){
  swal({
      title: "Are you sure?",
      text: ""+name+" is already exist, Do you want to add new record ?",
      type: "info",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: true
    },
    function(){
     add_patient(1);
    });
}
  // Patient

  // Patient modal recommendations javascript


   function show_medicine_results(){
      var illness_result = $("#illness_result");

      if (illness_result.val() == "" || illness_result.val() == null) {
        $("#diagnosis_medicine").html('<center><div class="text-danger">The medicine for the diagnosed illness is not available from our inventory!</div></center>');
      }else{
          var mydata = 'action=show_medicine_result' + '&result=' + illness_result.val();
          $.ajax({
            type:"POST",
            url:url,
            data:mydata,
            cache:false,
            success:function(data){
              if (data.trim() == 0) {
                $("#diagnosis_medicine").html('<center><div class="text-danger">The medicine for the diagnosed illness is not available from our inventory!</div></center>');

              }else{
                $("#diagnosis_medicine").html(data);
              }
              
              // console.log(data);
              console.log(illness_result.val());
            }
          });
      }
    
   }

   function close_modal_diagnosis(){
            $("#illness_result").tagsinput('removeAll');
            $("#second_page").hide('fast');
            $("#first_page").show('fast');
            $("#btn_save_diag").hide('fast');
            $("#btn_action_taken").show('fast');
            $("#btn_back_diag").hide('fast');
            $("#btn_print_save").hide('fast');
   }

   function get_illness_list(){
     var favorite = [];
        $.each($("input[name='illnesses']:checked"), function(){
            favorite.push($(this).val());
        });
      var mydata = favorite.join(", ");
      // alert(mydata);
      $("#illness_result").val(mydata);
      $('#illness_result').tagsinput('add',mydata);
      $("#second_page").show('fast');
        $("#first_page").hide('fast');
        $("#btn_save_diag").show('fast');
        $("#btn_print_save").show('fast');
        $("#btn_action_taken").hide('fast');
        $("#action_taken").focus();
      $("#btn_back_diag").show('fast');
    show_medicine_results();
   }


   // function get_illness_list(){
   //   var favorite = [];
   //      $.each($("input[name='illnesses']:checked"), function(){
   //          favorite.push($(this).val());
   //      });
   //    var mydata = favorite.join(", ");
   //    // alert(mydata);
   //  $("#illness_result").val(mydata);
   //    $('#illness_result').tagsinput('add',mydata);
   //    $("#second_page").show('fast');
   //      $("#first_page").hide('fast');
   //      $("#btn_save_diag").show('fast');
   //      $("#btn_action_taken").hide('fast');
   //      $("#action_taken").focus();
   //  $("#btn_back_diag").show('fast');

   //  show_medicine_results();
   // }

   function get_medicine_selected(i){
       var meds = [];
       var qty = [];
       var dossage = [];
       var intake = [];

       var meds_selected = document.getElementById("medicine_selected_"+i);

       if (meds_selected.checked == true) {
        $("#medicine_qty_"+i).attr('disabled', false);
        $("#medicine_qty_"+i).val(0);
        $("#dossage_"+i).attr('disabled', false);
        $("#intake_"+i).attr('disabled', false);

       }else{
        $("#dossage_"+i).attr('disabled', true);
        $("#medicine_qty_"+i).attr('disabled', true);
        $("#intake_"+i).attr('disabled', true);

        $("#medicine_qty_"+i).val('');
        $("#dossage_"+i).val('');
        $("#intake_"+i).val('');
       }

        $.each($("input[name='medicine_selected']"), function(){
            meds.push($(this).val());
        });


        $(".meds_form").each(function(){
          qty.push($(this).val());
        });

        $(".dossage_form").each(function(){
          dossage.push($(this).val());
        });

        $(".intake_form").each(function(){
          intake.push($(this).val());
        });

       var qty_data = qty.join(", "); 
       var dossage_data = dossage.join(", "); 
       var intake_data = intake.join(", "); 
       var mydata = meds.join(", ");


      $("#num_meds_selected").val(mydata);
      $("#qty_selected").val(qty_data);
      $("#dossage").val(dossage_data);
      $("#intake").val(intake_data);

      // console.log(mydata+' '+qty_data);
   }

   function edit_save_diagnosis(diag_id,illness_result,action_taken){
    $("#diag_id").val(diag_id);
    $("#illness_result").val(illness_result);
    $("#action_taken").val(action_taken);
   }


   function get_verify_value(id){
      var meds_selected = document.getElementById("medicine_selected_"+id);
      var meds_qty = $("#medicine_qty_"+id);

     var qty = [];

     $(".meds_form").each(function(){
      qty.push($(this).val());
     });
      
     var qty_data = qty.join(", "); 

    $("#qty_selected").val(qty_data);
   }

   function get_verify_value_dossage(id){

     var dossage = [];

    
     $(".dossage_form").each(function(){
      dossage.push($(this).val());
     });
    
     var dossage_data = dossage.join(", "); 

    $("#dossage").val(dossage_data);
   }


   function get_verify_value_intake(id){

     var intake = [];

    
     $(".intake_form").each(function(){
      intake.push($(this).val());
     });
    
     var intake_data = intake.join(", "); 

    $("#intake").val(intake_data);
   }

   function save_diagnosis(type){
    var name = $("#name_of_patient_diagnosis").text();
    var diag_id = $("#diag_id");
    var illness_result = $("#illness_result");
    var action_taken = $("#action_taken");
    var num_meds_selected = $("#num_meds_selected");
    var qty_selected = $("#qty_selected");
    var dossage = $("#dossage");
    var intake = $("#intake");



    var outmed_data = $("#out_meds_form").serialize();

  if (illness_result.val() == "") {
    illness_result.focus();
    twal("Diagnosis result is required!","error");
  }
  else if (action_taken.val() == "") {
    action_taken.focus();
    twal("Please enter any Notes!","error");
  }else{
    var mydata = 'action=save_diagnosis_doctor' + '&diag_id=' + diag_id.val() +'&illness_result=' + illness_result.val() +'&action_taken=' + action_taken.val() + '&num_meds_selected=' + num_meds_selected.val() +'&qty_selected=' + qty_selected.val() +'&dossage=' + dossage.val() + '&name=' + name + '&intake=' + intake.val() + '&submit_type=' + type;

    // alert(outmed_data);

    $.ajax({
      type:"POST",
      url:url,
      data:mydata+'&'+outmed_data,
      cache:false,
      success:function(data){
        if (data.trim() == 1) {
          $("#doctor_diagnosis").modal('hide');
          close_modal_diagnosis();
          edit_save_diagnosis('','','');
          twal("Diagnosis has been saved!","success");
          show_check_up_pending();
        }else if (data == 2) {
          window.location = 'print_pdf.php?id='+diag_id.val()+'&name='+name+'&address='+$("#address").val()+'&age='+$("#age").val()+'&gender='+$("#gender").val()+'';
          $("#doctor_diagnosis").modal('hide');
          close_modal_diagnosis();
          edit_save_diagnosis('','','');
          twal("Diagnosis has been saved!","success");
          show_check_up_pending();
        }else{
          console.log(data.trim());
        }
      }
    });
  }
   }




  function show_recommendations(id,name,symptoms,brgy,age,gender){


    var mydata = 'action=show_diagnosis_recommendations' + '&id=' + id + '&name=' + name + '&symptoms=' + symptoms;
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(data){
        $("#doctor_diagnosis").modal({'backdrop':'static'});
        $("#diagnosis_data").html(data);
        $("#name_of_patient_diagnosis").text(name);
        $("#address").val(brgy);
        $("#age").val(age);
        $("#gender").val(gender);
        $("#diag_id").val(id);
        // console.log(data);
      }
    });
  }

  function show_diagnoses(id,name,addr,age,gender){

    var mydata = 'action=show_diagnoses' + '&id=' + id + '&name=' + name + '&addr=' + addr + '&age=' + age + '&gender=' + gender;
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(data){
        $("#diagnoses").modal({'backdrop':'static'});
        $("#diagnoses_data").html(data);
        $("#name_of_patient").text(name);
      }
    });
  }

  function claim_certificate(id){
    var mydata = 'action=finish_medical' + '&medid=' + id;
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(data){
        console.log(data.trim());

        if (data.trim() == 1) {
          twal('Medicine has been successfully dispensed!','success');
          show_check_up();
          $("#diagnoses").modal('hide');
        }else{
          console.log(data.trim());
        }
      }

    });
  }