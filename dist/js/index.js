const url = 'function/function.php';

var cd;

$(function(){
  CreateCaptcha();
});

// Create Captcha
function CreateCaptcha() {
  //$('#InvalidCapthcaError').hide();
  var alpha = new Array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
                    
  var i;
  for (i = 0; i < 6; i++) {
    var a = alpha[Math.floor(Math.random() * alpha.length)];
    var b = alpha[Math.floor(Math.random() * alpha.length)];
    var c = alpha[Math.floor(Math.random() * alpha.length)];
    var d = alpha[Math.floor(Math.random() * alpha.length)];
    var e = alpha[Math.floor(Math.random() * alpha.length)];
    var f = alpha[Math.floor(Math.random() * alpha.length)];
  }
  cd = a + ' ' + b + ' ' + c + ' ' + d + ' ' + e + ' ' + f;
  $('#CaptchaImageCode').empty().append('<canvas id="CapCode" class="capcode" width="300" height="80"></canvas>')
  
  var c = document.getElementById("CapCode"),
      ctx=c.getContext("2d"),
      x = c.width / 2,
      img = new Image();

  img.src = "https://pixelsharing.files.wordpress.com/2010/11/salvage-tileable-and-seamless-pattern.jpg";
  img.onload = function () {
      var pattern = ctx.createPattern(img, "repeat");
      ctx.fillStyle = pattern;
      ctx.fillRect(0, 0, c.width, c.height);
      ctx.font="46px Roboto Slab";
      ctx.fillStyle = '#ccc';
      ctx.textAlign = 'center';
      ctx.setTransform (1, -0.12, 0, 1, 0, 15);
      ctx.fillText(cd,x,55);
  };
  
  
}

// Validate Captcha
function ValidateCaptcha() {
  var string1 = removeSpaces(cd);
  var string2 = removeSpaces($('#UserCaptchaCode').val());
  if (string1 == string2) {
    return true;
  }
  else {
    return false;
  }
}

// Remove Spaces
function removeSpaces(string) {
  return string.split(' ').join('');
}

// Check Captcha
function CheckCaptcha() {
  var result = ValidateCaptcha();
  if( $("#UserCaptchaCode").val() == "" || $("#UserCaptchaCode").val() == null || $("#UserCaptchaCode").val() == "undefined") {
    $('#WrongCaptchaError').text('Please enter code given below in a picture.').show();
    $('#UserCaptchaCode').focus();
  } else {
    if(result == false) { 
      $('#WrongCaptchaError').text('Invalid Captcha! Please try again.').show();
      CreateCaptcha();
      $('#UserCaptchaCode').focus().select();
    }
    else { 
      $('#UserCaptchaCode').val('').attr('place-holder','Enter Captcha - Case Sensitive');
      CreateCaptcha();
     
      $('#SuccessMessage').fadeIn(500).css('display','block').delay(5000).fadeOut(250);
      $("#loginbtn").attr('disabled', false);
      $('.CaptchaWrap').fadeOut(100);
      $("#btn_cap").hide('fast');

    }
  }  
}


$("#formLogin").on('submit', function(e){
	e.preventDefault();
	var username = $("#username");
	var password = $("#password");

	if (username.val() == "") {
	  username.focus();
	  twal(" Username is required!!","error");
	}
	else if (password.val() == "") {
	  password.focus();
	  twal(" Password is required!!","error");
	}else{ 
	  var mydata = 'action=login'+'&username=' + username.val() + '&password=' + password.val();
	  $.ajax({
	    type:"POST",
	    url:url,
	    data:mydata,
	    cache:false,
	    beforeSend:function(){
	     twal("Please Wait...","info");
	    },
	    success:function(data){
	      console.log(data.trim());
	      if (data.trim() == 1) {
	        window.location="admin/index.php";
	      }else{
	  		twal(" Username or Password is invalid!!","error");
	  		CreateCaptcha();
	        console.log(data.trim());
	       $("#loginbtn").attr('disabled', true);
	       $('.CaptchaWrap').fadeIn(100);
	       $("#btn_cap").show('fast');
	      }
	    }
	  });
	}

  });



function get_question_by_username(){
	var username = $("#username");
	
	if (username.val() == "") {
	  username.focus();
	  twal("Username is required!!","error");
	}else{ 
	  var mydata = 'action=forget_password'+'&username=' + username.val();
	  $.ajax({
	    type:"POST",
	    url:url,
	    data:mydata,
	    cache:false,
	    beforeSend:function(){
	     twal("Please Wait...","info");
	    },
	    success:function(data){
	      // console.log(data.trim());
	      if (data.trim() == 0) {
	        twal(" Username is inrequired!!","error");
	      }else{
     		twal("Username is correct!","success");
	      	$("#question").html(data);
	      	$("#username_check").hide('fast');
	  		 // console.log(data.trim());
	  		 // alert(data.trim());
	      }
	    }
	  });
	}
}


function check_answer(){
	var answer = $("#answer");
	var username = $("#username");

	if (answer.val() == "") {
		 answer.focus();
	     twal("answer is required!!","error");
	}else{
		var mydata = 'action=show_password'+'&username=' + username.val()+'&answer=' + answer.val();
		  $.ajax({
		    type:"POST",
		    url:url,
		    data:mydata,
		    cache:false,
		    beforeSend:function(){
		     twal("Please Wait...","info");
		    },
		    success:function(data){
		      // console.log(data.trim());
		      if (data.trim() == 0) {
		        twal(" Answer is invalid!","error");
		      }else{
	     		twal("Answer is correct!","success");
		      	$("#password_reveal").html(data);
		      	$("#questionare").hide('fast');
		      	$("#btn_cap").hide('fast');
		    
		      }
		    }
		  });
	}

}

function submit_password(id){
	var password = $("#password");
	

	if (password.val() == "") {
		 password.focus();
	     twal("password is required!!","error");
	}else{
		var mydata = 'action=change_forgot_password'+'&password=' + password.val() + '&account_id=' + id;
		  $.ajax({
		    type:"POST",
		    url:url,
		    data:mydata,
		    cache:false,
		    beforeSend:function(){
		     twal("Please Wait...","info");
		    },
		    success:function(data){
		      // console.log(data.trim());
		      if (data.trim() == 1) {
		        twal("Pasword has been changed!","success");
		        $("#questionare").hide('fast');
				$("#show_password_form").hide('fast');
				$("#password_reveal").append('<a href="index.php" class="btn btn-primary">Login</a>')
		      }else{
		      	console.log(data.trim());
		      }
		    }
		  });
	}

}