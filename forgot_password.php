<?php 
session_start();
if (isset($_SESSION['account_id'])) {
  @header('location:admin');
}

 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>DIAGNOSIS DECISION SUPPORT SYSTEM</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="plugins/toastr/toastr.min.css">
</head>
<style type="text/css">
  .login-board{
  padding-left:490px;
  padding-right:490px;
}
</style>

<body class="hold-transition login-page bg-info mt-1">
<div class="login-box">
  <div class="login-logo p-0 text-center mt-5">
    <a href="index.php"><b><img src="img/logo.png" class="img-fluid shadow rounded-circle" width="140"></b> </a>
    <p class="h5" style="text-shadow: 1px 1px 1px #555;">City Health Office’s Common Illnesses Diagnosis Decision Support System</p>
  </div>
  <!-- /.login-logo -->
  <div class="card shadow">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Forgot Password</p>

     

        <div class="row" id="username_check">
           <div class="input-group mb-3 col-sm-12">
              <input type="text" class="form-control" placeholder="Enter your username" id="username" autocomplete="off">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-user"></span>
                </div>
              </div>
            </div>

              <div class="col-sm-4">
          </div>

          <div class="col-sm-3">
            
          </div>
          <div class="col-sm-5">
            <button class=" btn-sm btn btn-success btn-block btn-flat" id="verifybtn" onclick="get_question_by_username();">Verify</button>
          </div>
        </div>
       
        <!-- Question Append -->
        <div id="question"></div>

        <div id="password_reveal" class="text-center"></div>

      <!-- /.social-auth-links -->

    </div>
    <!-- /.login-card-body -->
  </div>
</div>

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="plugins/toastr/toastr.min.js"></script>

<script src="dist/js/tools.js"></script>
<script src="dist/js/index.js"></script>

</body>
</html>
