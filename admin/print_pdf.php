<?php
require('../function/config.php');
require('../function/helper.php');
require('../vendor/FPDF/html_table.php');

$patient_id = $_REQUEST['id'];
$name = $_REQUEST['name'].'.';
$address = $_REQUEST['address'];
$age = $_REQUEST['age'];
$gender = $_REQUEST['gender'];

$data = array('id' => $patient_id);
$sql = "SELECT a.*,b.* from tbl_medical a left join tbl_account b on a.doctor_name=b.account_id where a.medical_id=:id";
$result = fetch_record($con,$data,$sql);

$data_meds = array('id' => $patient_id);
$sql_meds = "SELECT a.*,b.*, a.dosage as dosage from tbl_medical_med_reco a left join tbl_medicine b on a.medicine_id=b.medicine_id where a.medical_id=:id";

$data_meds_out = array('id' => $patient_id);
$sql_meds_out = "SELECT * from tbl_out_medicine where medical_id=:id";

$res = fetch_record($con,$data_meds_out,$sql_meds_out);

$result_meds = fetch_record($con,$data_meds,$sql_meds);

$row = $result->fetch();

$name_doctor = $row['fn'].' '.$row['ln'];
$license_no = $row['license_number'];
$s2 = $row['s2_number'];
$date_issue = $row['date_issue'];
$date_expiry = $row['date_expiry'];
$prt_number = $row['ptr_number'];


$date_conducted = date('M d, Y',strtotime($row['date_entry']));

$illness = $row['diagnoses_result'];


$land ='';
$land_head ='';



$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Ln(5);
// $pdf->SetFont('Times','B',13);
// $pdf->Cell(0,5,'Summary Report '.ucfirst('').' Monitoring',0,1,'C',false);
// $pdf->SetFont('Times','',13);
// $pdf->Cell(0,5,'For the month of '.date('F').', '.date('Y'),0,1,'C',false);
$pdf->SetFont('Times','',11);
$pdf->Cell(0,0,'NAME: '.$name,0,1,'L',false);
$pdf->Cell(0,0,'AGE: '.$age.'              GENDER: '.$gender,0,1,'R',false);
$pdf->Ln(8);
$pdf->Cell(0,0,'ADDRESS: '. ucfirst($address),0,1,'L',false);
$pdf->Cell(0,0,'DATE: '.date('Y/m/d',strtotime($date_conducted)) ,0,1,'R',false);


$pdf->Ln(18);
$pdf->Image('../img/rx.png',10,63,12);

$land_head .='<table border>';
$land_head .='<tr>
			<td width=""><br><td>
		</tr>';
$land_head .='</table>';


	$land .='<table border="1">
			<thead>
				<tr>
					<td width="200" bgcolor="#D0D0FF">Medicine Name</td>
					<td width="160" bgcolor="#D0D0FF">QTY</td>
					<td width="200" bgcolor="#D0D0FF">Dosage</td>
					<td width="200" bgcolor="#D0D0FF">Intake Schedule</td>
				</tr>
			</thead>
			<tbody>';
			
			while ($rows = $result_meds->fetch()) {
				$land .='<tr>';
					$land .= '<td width="200">'.$rows['brand_name'].' ('.$rows['generic_name'].')'.'</td>';
					$land .= '<td width="160">'.$rows['qty_reco'].'</td>';
					$land .= '<td width="200">'. $rows['dosage'].'</td>';
					$land .= '<td width="200">'. $rows['intake_schedule'].'</td>';
				$land .= '</tr>';
			}

			while ($rr = $res->fetch()) {
				$land .='<tr>';
					$land .= '<td width="200">'.$rr['medicine_brand'].' ('.$rr['medicine_generic_name'].')'.'</td>';
					$land .= '<td width="160">'.$rr['qty'].'</td>';
					$land .= '<td width="200">'. $rr['dossage'].'</td>';
					$land .= '<td width="200">'. $rr['intake_schedule'].'</td>';
				$land .= '</tr>';
			}
			
	$land .='</tbody>';
	$land .= '</table>';

// Land



$pdf->SetFont('Times','',18);
$pdf->WriteHTML($land_head);
$pdf->SetFont('Times','',8);
$pdf->WriteHTML($land);

$pdf->Ln(10);
// $pdf->Ln(50);
$pdf->SetFont('Times','B',12);
$pdf->Cell(0,5,'Physician\'s Notes:',0,1,'L',false);
$pdf->Ln(1);
$pdf->SetFont('Times','I',10);
$pdf->Cell(0,5,$row['action_taken'],0,1,'L',false);


$pdf->Ln(50);
$pdf->SetFont('Times','B',12);
$pdf->Cell(0,5,'Dr. '.$name_doctor,0,1,'R',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(0,5,'License No.'.$license_no,0,1,'R',false);
$pdf->Cell(0,5,'S2 No.'.$license_no,0,1,'R',false);
$pdf->Cell(0,5,'Date of Issue: '.$date_issue,0,1,'R',false);
$pdf->Cell(0,5,'Date of Expiry: '.$date_expiry,0,1,'R',false);
$pdf->Cell(0,5,'PTR No: '.$prt_number,0,1,'R',false);
$pdf->Ln(3);

$pdf->AddPage();
$pdf->Ln(8);
$pdf->SetFont('Times','B',20);
$pdf->Cell(0,0,'MEDICAL CERTIFICATE',0,1,'C',false);
$pdf->Ln(18);
$pdf->SetFont('Times','I',14);
$pdf->Cell(0,0,'This is to certify that Mr./Ms. '.$name.' has undergone a',0,1,'C',false);
$pdf->Ln(8);
$pdf->Cell(0,0,'Medical examinaition conducted on '.$date_conducted.' by ',0,1,'C',false);
$pdf->Ln(8);
$pdf->Cell(0,0,'Dr.'.$name_doctor.' is currently suffering from '. $illness.'.',0,1,'C',false);

$pdf->Ln(10);

$pdf->Cell(0,0,'The physician has advised that, for the sake of the person\'s overall health, ',0,1,'C',false);
$pdf->Ln(8);
$pdf->Cell(0,0,'he/she should be allowed to rest for a period of _____ days.',0,1,'C',false);

$pdf->Ln(50);
$pdf->SetFont('Times','B',12);
$pdf->Cell(0,5,'Dr. '.$name_doctor,0,1,'R',false);
$pdf->SetFont('Times','B',10);
$pdf->Cell(0,5,'Physician',0,1,'R',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(0,5,'License No.'.$license_no,0,1,'R',false);
$pdf->Ln(3);

// $pdf->AddPage();
// $pdf->WriteHTML($crop);
$pdf->Output();
?>
