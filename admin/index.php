<?php include("header.php") ?>
 <link rel="stylesheet" href="../dist/checkup/tagsinput.css">
<?php include("user_direction.php") ?>
<body class="hold-transition sidebar-mini layout-fixed" onload="$('#date_to').val('<?php echo date("F") ?>'); get_data_charts(); sidebar_selected_side('dashboard'); show_patient(); show_illness(); $('#date_check').val('<?php echo date('Y-m-d') ?>'); show_check_up();">
<div class="wrapper">

  <!-- Navbar -->
<?php include("navbar.php") ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
<?php include("sidemenu.php") ?>
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <!-- <h1 class="m-0 text-dark">Dashboard</h1> -->
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">

          <?php if ($auth['user_type'] == 0 || $auth['user_type'] == 4): ?>
            <div class="col-lg-3 col-3">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3><?php echo count_dashboard($con,1) ?></h3>

                <p>Doctors</p>
              </div>
              <div class="icon">
                <i class="fas fa-user-md"></i>
              </div>
              <a href="doctors.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-3">
            <!-- small box -->
            <div class="small-box bg-light">
              <div class="inner">
                <h3><?php echo count_dashboard($con,2) ?></h3>

                <p>Nurse</p>
              </div>
              <div class="icon">
                <i class="fa fa-user-nurse"></i>
              </div>
              <a href="nurse.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->

           <!-- ./col -->
          <div class="col-lg-3 col-3">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3><?php echo count_dashboard($con,3) ?></h3>

                <p>Pharmacist</p>
              </div>
              <div class="icon">
                <i class="fa fa-capsules"></i>
              </div>
              <a href="pharma.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>


             <div class="col-lg-3 col-3">
                <!-- small box -->
                <div class="small-box bg-primary">
                  <div class="inner">
                    <?php $arrs = 'WHERE is_delete is null' ?>
                    <h3><?php echo number_format(sum_all($con,'tbl_medicine',$arrs,'qty')) ?></h3>
                    <p>Available Medicine</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-capsules"></i>
                  </div>
                  <a href="pharma.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>

          <?php endif ?>

          <!-- Doctor -->
          <?php if ($auth['user_type'] == 1): ?>
             <div class="col-lg-6 col-6">
              <!-- small box -->
              <div class="small-box bg-info">
                <div class="inner">
                  <?php $arrs = 'WHERE DATE_FORMAT(date_entry,"%Y%M%d") = "'.date('YFd').'" and is_delete is null and is_finish = 1' ?>
                  <h3><?php echo count_all($con,'tbl_medical',$arrs,'medical_id') ?></h3>
                  <p>Patient For Diagnosis</p>
                </div>
                <div class="icon">
                  <i class="fas fa-wheelchair"></i>
                </div>
                <a href="diagnosis.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>

             <div class="col-lg-6 col-6">
                <!-- small box -->
                <div class="small-box bg-primary">
                  <div class="inner">
                    <?php $arrs = 'WHERE is_delete is null' ?>
                    <h3><?php echo number_format(sum_all($con,'tbl_medicine',$arrs,'qty')) ?></h3>
                    <p>Available Medicine</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-capsules"></i>
                  </div>
                  <a href="pharma.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
          <?php endif ?>
          <!-- doctor -->

           <!-- Nurse -->
             <?php if ($auth['user_type'] == 2): ?>
            <div class="col-lg-6 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <?php $arrs = 'WHERE DATE_FORMAT(date_entry,"%Y%M%d") = "'.date('YFd').'" and is_delete is null and is_finish = 1' ?>
                <h3><?php echo count_all($con,'tbl_medical',$arrs,'medical_id') ?></h3>
                <p>Patient For Diagnosis</p>
              </div>
              <div class="icon">
                <i class="fas fa-wheelchair"></i>
              </div>
              <!-- <a href="doctors.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-6 col-6">
            <!-- small box -->
            <div class="small-box bg-light">
              <div class="inner">
                  <?php $arrs = 'WHERE DATE_FORMAT(date_entry,"%M") = "'.date('F').'" and is_delete is null' ?>
                <h3><?php echo count_all($con,'tbl_medical',$arrs,'medical_id') ?></h3>
                <p>Total Patient for the month of <?php echo date('F'); ?></p>
              </div>
              <div class="icon">
                <i class="fa fa-users"></i>
              </div>
              <!-- <a href="nurse.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
            </div>
          </div>

          <div class="col-lg-12 col-sm-12">
             <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="nav-icon fas fa-wheelchair"></i> Patient Management</h3>
                <button class="btn btn-success float-right" data-toggle="modal" data-backdrop="static" data-target="#add_patient" onclick="$('#add_patient_title').text('Add Patient')"><i class="fas fa-user-plus"></i> Add Patient</button>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                 <div class="table-responsive">
                <table id="tbl_patient" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <td nowrap>Name</td>
                      <td width="15%" nowrap>Age</td>
                      <td width="15%" nowrap>Gender</td>
                      <td >Address</td>
                      <td width="10%" nowrap>option</td>
                    </tr>
                  </thead>
                  <tbody id="data_patient"></tbody>
                </table>
              </div>
            </div>
              <!-- /.card-body -->
            </div>
          </div>
          <!-- ./col -->

          <?php endif ?>
          <!-- nurse -->

          <!-- medic -->
          <?php if ($auth['user_type'] == 3): ?>
             <div class="col-lg-6 col-6">
                <!-- small box -->
                <div class="small-box bg-primary">
                  <div class="inner">
                    <?php $arrs = 'WHERE is_delete is null' ?>
                    <h3><?php echo number_format(sum_all($con,'tbl_medicine',$arrs,'qty')) ?></h3>
                    <p>Available Medicine</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-capsules"></i>
                  </div>
                  <a href="medicines.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>


               <div class="col-lg-6 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                  <div class="inner">
                    <?php $arrs = '' ?>
                    <h3><?php echo number_format(sum_all($con,'tbl_medical_med_reco',$arrs,'qty_reco')) ?></h3>
                    <p>Dispensed Medicine</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-capsules"></i>
                  </div>
                  <a href="outgoing_medicine.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>

              <div class="col-lg-12 col-12">
                <div class="card">
                <div class="card-header">
                  <div class="row m-0">
                    <div class="card-title col-sm-6 h3 pt-2"><i class="nav-icon fas fa-calendar-check"></i> Patient Records</div>
                    <div class="input-group col-sm-3">
                      <input type="text" class="form-control" id="illness_type" placeholder="Search Illness">
                    </div>
                    <div class="input-group col-sm-3">
                      <input type="date" class="form-control" id="date_check">
                      <div class="input-group-append">
                        <button type="button" class="btn btn-outline-success" onclick="show_check_up();">Search</button>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <div class="table-responsive">
                  <table id="tbl_check" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <td nowrap>#</td>
                        <td nowrap>Name</td>
                        <td nowrap>Date Check Up</td>
                        <td nowrap>Notes</td>
                        <td nowrap>Diagnosed Illness</td>
                        <td nowrap>Prescription</td>
                        <td width="10%" nowrap >option</td>
                      </tr>
                    </thead>
                    <tbody id="data_check"></tbody>
                  </table>
                </div>
              </div>
                <!-- /.card-body -->
              </div>
              </div>
          <?php endif ?>

          <!-- ./col -->
          <div class="col-12">
          <!-- /.card -->
          <?php if ($auth['user_type'] == 4 || $auth['user_type'] == 0 || $auth['user_type'] == 1): ?>
              <div class="card">
                <div class="card-header">
                  <div class="row">
                   <div class="card-title h3 col-sm-8 pt-2">Dashboard</div>
                  <div class="input-group col-sm-4">
                      <select class="form-control" id="date_to">
                        <?php echo get_month_option() ?>
                      </select>
                      <select class="form-control" id="year_to">
                        <?php echo get_year_option() ?>
                      </select>
                      <div class="input-group-append">
                        <button type="button" class="btn btn-outline-success" onclick=" get_data_charts();">Search</button>
                      </div>
                    </div>  
                  </div>

                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <div id="warning_values" style="min-width: 100%; height: 400px; max-width: 600px; margin: 0 auto"></div>
                  
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
          <?php endif ?>


        
          

        
        </div>

        <?php if ($auth['user_type'] == 1 || $auth['user_type'] == 0): ?>
          <div class="col-12">
            <!-- /.card -->

            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="nav-icon fas fa-user-md"></i> List of Illness</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tbl_illness" class="table table-bordered table-striped table-hover" style="cursor: pointer;">
                  <thead>
                    <tr>
                      <td nowrap>Types of Illness</td>
                      <td nowrap>Symptoms</td>
                    </tr>
                  </thead>

                  <tbody id="data_illness"></tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        <?php endif ?>

        </div>
        <!-- /.row -->
        <!-- Main row -->
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<?php include('footer.php') ?>
<script type="text/javascript" src="../dist/checkup/tagsinput.js"></script>
</body>
</html>

<script type="text/javascript">





function get_data_charts(){
  var date_to = $("#date_to").val();
  var year_to = $("#year_to").val();

  var mydata = 'action=get_data_chart' + '&date_to=' + date_to + '&year_to=' + year_to;

  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
      // return data;
      if (data.trim() != 0) {
        $("#warning_values").html(data.trim());
        // setTimeout(function(){ charts_on(data.trim(),date_to +' '+year_to);},1000);
       
      // console.log(data);
      }else{
        $("#warning_values").html('<center><h4>No data found!</h4></center>');
      }
      // alert(data);
    }
  });
}


</script>
