<?php include("header.php") ?>
<?php 
if ($auth['user_type'] == 2) {
  @header('location:teacher_index.php');
}else if ($auth['user_type'] == 3) {
  @header('location:student_index.php'); 
}
 ?>
<body class="hold-transition sidebar-mini layout-fixed" onload="show_doctor();  sidebar_selected_side('doctors');">
<div class="wrapper">

  <!-- Navbar -->
<?php include("navbar.php") ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
<?php include("sidemenu.php") ?>
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <!-- <h1 class="m-0 text-dark">Dashboard</h1> -->
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <button class="btn btn-success" onclick="new_account('1','Add Doctors');"><i class="fas fa-user-plus"></i> Add Doctor</button>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <!-- ./col -->
          <div class="col-12">
          <!-- /.card -->

          <div class="card">
            <div class="card-header">
              <h3 class="card-title"><i class="nav-icon fas fa-user-md"></i> Doctor Management</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="tbl_doctor" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <td nowrap>Name</td>
                    <td nowrap>Username</td>
                    <td width="15%" nowrap>Gender</td>
                    <td width="10%" nowrap>option</td>
                  </tr>
                </thead>

                <tbody id="data_doctor"></tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>



        </div>
        <!-- /.row -->
        <!-- Main row -->
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>

<!-- ./wrapper -->
<?php include('footer.php') ?>

</body>
</html>


