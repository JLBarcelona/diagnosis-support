
<div class="modal fade" role="dialog" id="update_password">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<div class="modal-title">Change Password</div>
				<button class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label>Old Password</label>
					<input type="password" name="o_pwd" id="o_pwd" class="form-control" placeholder="Enter old password">
				</div>

				<div class="form-group">
					<label>New Password</label>
					<input type="password" name="pwd" id="pwd" class="form-control" placeholder="Enter New password">
				</div>

				<div class="form-group">
					<label>Old Password</label>
					<input type="password" name="c_pwd" id="c_pwd" class="form-control" placeholder="Enter Confirm password">
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-danger" data-dismiss="modal">Cancel</button>
				<button class="btn btn-primary" onclick="change_password();">Save Changes</button>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" role="dialog" id="check_up_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<div class="modal-title">Initial Check-Up </div>
				<button class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<form id="form_medical" onsubmit="event.preventDefault();">
					<h4><span id="name_patient"></span></h4>
					<hr>
					<input type="hidden" name="check_id" id="check_id">
					<input type="hidden" name="patient_id" id="patient_id">

					<div class="row" id="tab">
						<div class="col-sm-12 text-center">
							<ul class="nav nav-tabs">
							  <li class="nav-item">
							    <a class="nav-link active" data-toggle="tab" id="vitals_tab" href="#vital" disabled>Vital Signs</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" data-toggle="tab" href="#lab" id="labs" disabled>Lab Result</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" data-toggle="tab" href="#medical" id="meds" disabled>Medical History</a>
							  </li>
							</ul>
						</div>
						<div class="col-sm-12">
							<!-- Tab panes -->
							<div class="tab-content">
							  <div class="tab-pane container active" id="vital">
							  	<div class="row">
							  		<div class="col-sm-12"><br></div>
									<div class="form-group col-sm-6">
										<label>Blood Preasure</label>
										<input type="text" name="bp" id="bp" class="form-control" placeholder="Enter Blood Preasure">
									</div>

									<div class="form-group col-sm-6">
										<label>Pulse Rate</label>
										<input type="text" name="pulse_rate" id="pulse_rate" class="form-control" placeholder="Enter Pulse Rate">
									</div>

									<div class="form-group col-sm-6">
										<label>Respiratory Rate</label>
										<input type="text" name="respiratory_rate" id="respiratory_rate" class="form-control" placeholder="Enter Pulse Rate">
									</div>

									<div class="form-group col-sm-6">
										<label>Temperature </label>
										<input type="text" name="temp" id="temp" class="form-control" placeholder="Enter Body temperature">
									</div>

									<div class="form-group col-sm-6">
										<label>Height (centimeters)</label>
										<input type="text" name="height" id="height" class="form-control" placeholder="Enter height">
									</div>

									<div class="form-group col-sm-6">
										<label>Weight (kilograms)</label>
										<input type="text" name="weight" id="weight" class="form-control" placeholder="Enter weight">
									</div>

									<!-- 	<div class="col-sm-12 text-right">
											<button class="btn btn-success" onclick="$('#labs').click();">Next</button> 
										</div>	 -->			
								</div>
							  </div>
							  <div class="tab-pane container fade" id="lab">
							  	<div class="row">
							  		<div class="col-sm-12"><br></div>
									<div class="form-group col-sm-6">
										<label>Complete Blood Count</label>
										<input type="text" name="cbc" id="cbc" class="form-control" placeholder="Enter CBC">
									</div>

									<div class="form-group col-sm-6">
										<label>Urinalysis</label>
										<input type="text" name="urine" id="urine" class="form-control" placeholder="Enter Urine analysis result">
									</div>

									<div class="form-group col-sm-6">
										<label>Fecalysis</label>
										<input type="text" name="fecalysis" id="fecalysis" class="form-control" placeholder="Enter Fecalysis">
									</div>

									<div class="form-group col-sm-6">
										<label>Gram Staining</label>
										<select  name="gram_staining" id="gram_staining" class="form-control">
											<option>Negative</option>
											<option>Positive</option>
										</select>
									</div>

									<div class="form-group col-sm-6">
										<label>Hepa B Screening </label>
										<select  name="hepa_b" id="hepa_b" class="form-control">
											<option>Negative</option>
											<option>Positive</option>
										</select>

									</div>

									<div class="form-group col-sm-6">
										<label>Syphilis Screening </label>
										<select  name="syphilis_screening" id="syphilis_screening" class="form-control">
											<option>Negative</option>
											<option>Positive</option>
										</select>
									</div>


									<div class="col-sm-12 text-center">
										<hr>
										<label>BLOOD CHEMISTRY</label>
									</div>

									<div class="form-group col-sm-6">
										<label>Blood Urea Nitrogen</label>
										<input type="text" name="bun" id="bun" class="form-control" placeholder="Enter (BUN)">
									</div>

									<div class="form-group col-sm-6">
										<label>Calcium</label>
										<input type="text" name="ca" id="ca" class="form-control" placeholder="Enter Calcium">
									</div>

									<div class="form-group col-sm-6">
										<label>Chloride</label>
										<input type="text" name="ci" id="ci" class="form-control" placeholder="Enter Chloride">
									</div>


									<div class="form-group col-sm-6">
										<label>Cholesterol</label>
										<input type="text" name="chol" id="chol" class="form-control" placeholder="Enter Cholesterol">
									</div>


									<div class="form-group col-sm-6">
										<label>Creatinine</label>
										<input type="text" name="creat" id="creat" class="form-control" placeholder="Enter Creatinine">
									</div>

									<div class="form-group col-sm-6">
										<label>Fasting Blood Sugar</label>
										<input type="text" name="fasting_blood_sugar" id="fasting_blood_sugar" class="form-control" placeholder="Enter Fasting blood sugar">
									</div>

									<div class="form-group col-sm-6">
										<label>Potassium</label>
										<input type="text" name="potassium" id="potassium" class="form-control" placeholder="Enter Potassium">
									</div>


									<div class="form-group col-sm-6">
										<label>Sodium</label>
										<input type="text" name="sodium" id="sodium" class="form-control" placeholder="Enter Sodium">
									</div>


									<div class="form-group col-sm-6">
										<label>Thyroid-Stimulating Hormone</label>
										<input type="text" name="tsh" id="tsh" class="form-control" placeholder="Enter (TSH)">
									</div>

									<div class="form-group col-sm-6">
										<label>Urea</label>
										<input type="text" name="urea" id="urea" class="form-control" placeholder="Enter Urea">
									</div>

								<!-- 	<div class="col-sm-12 text-right">
										<button class="btn btn-default" onclick="$('#vitals_tab').click();">Back</button> 
										<button class="btn btn-success" onclick="$('#meds').click();">Next</button> 
									</div>		 -->

								</div>
							  </div>
							  <div class="tab-pane container fade" id="medical">
							  	<div class="row">
									<div class="form-group col-sm-12">
										<hr>
										<label>Illness History </label>
										<input type="text" name="illness_history" id="illness_history" class="form-control" placeholder="Enter Illness history">
									</div>

									<div class="form-group col-sm-12">
										<label>Medication History </label>
										<input type="text" name="medication_history" id="medication_history" class="form-control" placeholder="Enter Medication history">
									</div>

									<div class="form-group col-sm-12">
										<label>Symptoms</label>
					                 		<input type="text" list="list_symptoms" data-role="tagsinput" id="symptoms"  class="form-control" autocomplete="off">
									</div>
									<datalist id="list_symptoms">
									 	<?php echo option_symptoms($con) ?>
									</datalist>

									
								</div>
							  </div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button class="btn btn-danger" data-dismiss="modal">Cancel</button>
				<button class="btn btn-primary" onclick="add_check_up();">Save</button>
			</div>
		</div>
	</div>
</div>





<script type="text/javascript">
	function edit_check_up(check_id,name,patient_id,height,weight,bp,symptoms,temp,blood_sugar,urine,illness_history,medication_history){
		$("#check_id").val(check_id);
		$("#name_patient").text(name);
		$("#patient_id").val(patient_id);
		$("#height").val(height);
		$("#weight").val(weight);
		$("#bp").val(bp);
		$("#symptoms").val(symptoms);
		$("#temp").val(temp);
		$("#blood_sugar").val(blood_sugar);
		$("#urine").val(urine);
		$("#illness_history").val(illness_history);
		$("#medication_history").val(medication_history);
		
	}




	function edit_check_up(name,check_id,patient_id,bp,pulse_rate,respiratory_rate,temp,height,weight,cbc,urine,fecalysis,gram_staining,hepa_b,syphilis_screening,bun,ca,ci,chol,creat,fasting_blood_sugar,potassium,sodium,tsh,urea,illness_history,medication_history,symptoms){

		$("#name_patient").text(name);
		$("#check_id").val(check_id);
		$("#patient_id").val(patient_id);
		$("#bp").val(bp);
		$("#pulse_rate").val(pulse_rate);
		$("#respiratory_rate").val(respiratory_rate);
		$("#temp").val(temp);
		$("#height").val(height);
		$("#weight").val(weight);
		$("#cbc").val(cbc);
		$("#urine").val(urine);
		$("#fecalysis").val(fecalysis);
		$("#gram_staining").val(gram_staining);
		$("#hepa_b").val(hepa_b);
		$("#syphilis_screening").val(syphilis_screening);
		$("#bun").val(bun);
		$("#ca").val(ca);
		$("#ci").val(ci);
		$("#chol").val(chol);
		$("#creat").val(creat);
		$("#fasting_blood_sugar").val(fasting_blood_sugar);
		$("#potassium").val(potassium);
		$("#sodium").val(sodium);
		$("#tsh").val(tsh);
		$("#urea").val(urea);
		$("#illness_history").val(illness_history);
		$("#medication_history").val(medication_history);
		$("#symptoms").val(symptoms);
		$('#symptoms').tagsinput('add',symptoms);
		$("#check_up_modal").modal({'static':'backdrop'});
	}
</script>



<div class="modal fade" role="dialog" id="add_medicine">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<div class="modal-title" id="title_med">Add Medicine</div>
				<button class="close" onclick="add_edit_meds('','','','','','','','');" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<input type="hidden" name="" id="medicine_id">
				<div class="row">

					<div class="form-group col-sm-12">
						<label>Brand Name</label>
						<input type="text" name="brand" id="brand" class="form-control" placeholder="Enter brand name">
					</div>

					<div class="form-group col-sm-12">
						<label>Generic Name</label>
						<input type="text" name="gen_name" id="gen_name" class="form-control" placeholder="Enter generic name">
					</div>

					<div class="form-group col-sm-3">
						<label>Quantity</label>
						<input type="number" name="qty" id="qty" class="form-control" placeholder="Enter quantity" min="0">
					</div>

					<div class="form-group col-sm-3">
						<label>Dosage</label>
						<input type="text" name="dosage" id="dosage" class="form-control" placeholder="Enter dosage">
					</div>

					<div class="form-group col-sm-3">
						<label>Form/Type</label>
						<select class="form-control" id="form_type" name="form_type">
							<option></option>
							<?php echo list_form_type() ?>
						</select>
					</div>

					

					<div class="form-group col-sm-3">
						<label>Supplier</label>
						<input type="text" name="supplier" id="supplier" class="form-control" placeholder="Enter supplier name">
					</div>

					<div class="col-sm-12">
						<hr>
						
						<div class="row">
							<div class="col-sm-12">
								<label>Prescribe for illness</label>
							</div>
							<!-- <div class="col-sm-1"><button class="btn btn-success btn-sm"><i class="fa fa-plus"></i></button></div> -->
							<div class="col-sm-12 form-group mt-1">
                         		<input type="text" list="list_illness" data-role="tagsinput" id="illness"  class="form-control" >
							</div>
						</div>

						<datalist id="list_illness">
						  <?php echo option_illness($con) ?>
						</datalist>

					</div>
				</div>
				
			</div>
			<div class="modal-footer">
				<button class="btn btn-danger" onclick="add_edit_meds('','','','','','','','');" data-dismiss="modal">Cancel</button>
				<button class="btn btn-primary" onclick="add_meds();">Save</button>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" role="dialog" id="update_user">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<div class="modal-title">Update License Number</div>
				<button class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label>License No.</label>
					<input type="text" id="license_no" class="form-control" placeholder="Enter license number" value="<?php echo $auth['license_number'] ?>">
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" onclick="save_license_no();">Save Changes</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function save_license_no(){
		var license_no = $("#license_no");

		if (license_no.val() == "") {
			license_no.focus();
			twal("License number is required!","error");
		}else{
			var mydata = 'action=save_license_no' + '&license_no=' + license_no.val();
			$.ajax({
				type:"POST",
				url:url,
				data:mydata,
				cache:false,
				success:function(data){
					if (data.trim() == 1) {
					twal("License number has been saved!","success");
					$("#update_user").modal('hide');
					}else{
						console.log(data.trim());
					}
				}
			});
		}
	}
</script>

<div class="modal fade" role="dialog" id="diagnoses">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<div class="modal-title">Diagnosis</div>
				<button class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<span id="name_of_patient" class="text-capitalize h3"></span>
				<hr>
				<div id="diagnoses_data"></div>

			</div>
		</div>
	</div>
</div>



<!-- Recommendations -->
<div class="modal fade" role="dialog" id="doctor_diagnosis">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<div class="modal-title">Diagnosis</div>
				<button class="close" data-dismiss="modal" onclick="close_modal_diagnosis(); edit_save_diagnosis('','','');">&times;</button>
			</div>
			<div class="modal-body">
				<input type="hidden" name="diag_id" id="diag_id">
				<span id="name_of_patient_diagnosis" class="text-capitalize h3"></span>
				<input type="hidden" id="address">
				<input type="hidden" id="age">
				<input type="hidden" id="gender">
				<hr>
				<div id="first_page">
					<div id="diagnosis_data"></div>
				</div>	
				<div id="second_page" style="display: none;">
					<div class="row">
						<div class="col-sm-12 form-group">
							<label>Diagnosed Illness</label>
							<input type="text" list="illness_options" onchange="show_medicine_results();" data-role="tagsinput" id="illness_result"  class="form-control" >
							<datalist id="illness_options">
							  <?php echo option_illness($con) ?>
							</datalist>
						</div>

						<div class="col-sm-12 form-group">
							<label>Physician's Notes</label>
							<textarea class="form-control" id="action_taken"></textarea>
						</div>

						<div class="col-sm-12">
							<hr>
							<div id="diagnosis_medicine"></div>
							<hr>
							<div class="row mb-1">
								<div class="col-sm-6">
										<h5><b>Medicine to be purchased outside</b></h5>
								</div>
								<div class="col-sm-6 text-right">
									<button class="btn btn-success btn-sm" onclick="add_new_row();"><i class="fa fa-plus"></i></button>
									<button class="btn btn-danger btn-sm" onclick="remove_last_row();"><i class="fa fa-minus"></i></button>
								</div>
							</div>
							<form id="out_meds_form">
							<table class="table table-striped">
								<thead>
									<tr>
										<th nowrap>Brand Name</th>
										<th nowrap>Generic Name</th>
										<th nowrap class="text-center" width="20%">Qty</th>
										<th nowrap class="text-center" width="20%">Dosage</th>
										<th nowrap class="text-center" width="20%">Intake Schedule</th>
									</tr>
								</thead>
								
								
									<input type="hidden" id="number_of_row" name="number_of_row" value="0">
									<tbody id="out_medicine">
										<tr id="out_med_row_0">
											<td>
												<input type="text" id="out_med_brand_0" name="out_med_brand_0" placeholder="Brand name" class="form-control">
											</td>
											<td>
												<input type="text" id="out_med_generic_0" name="out_med_generic_0" placeholder="Generic name" class="form-control">
											</td>
											<td>
												<input type="number" id="out_med_qty_0" name="out_med_qty_0" placeholder="Quantity" class="form-control">
											</td>
											<td>
												<input type="text" id="out_med_dossage_0" name="out_med_dossage_0" placeholder="Dosage" class="form-control">
											</td>
											<td>
												<input type="text" id="out_med_intake_0" name="out_med_intake_0" placeholder="Intake schedule" class="form-control">
											</td>
										</tr>
									</tbody>
							</table>
							</form>
						</div>
					</div>
				</div>
				
			</div>

			<div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal" onclick="close_modal_diagnosis(); edit_save_diagnosis('','','');">Close</button>
				<button class="btn btn-success" id="btn_action_taken" onclick="get_illness_list();">Finish Diagnosis</button>
				<button class="btn btn-danger" style="display: none;" id="btn_back_diag" onclick="close_modal_diagnosis();">Back</button>
				<button class="btn btn-primary" style="display: none;" id="btn_print_save" onclick="save_diagnosis('print');">Save & Print</button>
				<button class="btn btn-success" style="display: none;" id="btn_save_diag" onclick="save_diagnosis('finish');">Mark as Finish</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
   function get_max(input,max_val){

   	console.log(max_val+' '+input.val());

    if (Number(input.val()) > Number(max_val)) {
      input.focus();
      twal("The maximum quantity for this item is "+ max_val+"!","error");
      input.val(max_val);
      // return false;
    }else{
      // return true;	
    }
   }
</script>

<script type="text/javascript">
	function add_new_row(){
		var number_of_row = $("#number_of_row");
		var out_medicine = $("#out_medicine");

		var number = (Number(number_of_row.val()) + 1);


		var data = `
		<tr id="out_med_row_`+number+`">
				<td>
					<input type="text" id="out_med_brand_`+number+`" name="out_med_brand_`+number+`" placeholder="Brand name" class="form-control">
				</td>
				<td>
					<input type="text" id="out_med_generic_`+number+`" name="out_med_generic_`+number+`" placeholder="Generic name" class="form-control">
				</td>
				<td>
					<input type="number" id="out_med_qty_`+number+`" name="out_med_qty_`+number+`" placeholder="Quantity" class="form-control">
				</td>
				<td>
					<input type="text" id="out_med_dossage_`+number+`" name="out_med_dossage_`+number+`" placeholder="Dosage" class="form-control">
				</td>
				<td>
					<input type="text" id="out_med_intake_`+number+`" name="out_med_intake_`+number+`" placeholder="Intake schedule" class="form-control">
				</td>
			</tr>
		`;


		console.log(number_of_row.val());

		number_of_row.val(number);
		out_medicine.append(data);
		
	}

	function remove_last_row(){
		var number_of_row = $("#number_of_row");
		var out_medicine = $("#out_medicine");

		if (Number(number_of_row.val()) == 0) {
			number_of_row.val(0);
		}else{
			$("#out_med_row_" + number_of_row.val()).remove("#out_med_row_" + number_of_row.val());

			var number = (Number(number_of_row.val()) - 1);
			number_of_row.val(number);
		}
	}
</script>


	<div class="modal fade" id="add_account">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title"><span id="add_what"></span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"  onclick=" edit_clear_account('','','','','','','','','','','','','','','','','','');">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
          	  	<input type="hidden" name="" id="user_type">
                <div class="container-fluid">
                  <div class="row">
                    <input type="hidden" name="" id="user_id">
                  
                    <div class="col-sm-6 form-group">
                      <label>Firstname</label>
                      <input type="text" class="form-control" placeholder="Firstname" id="fn" autocomplete="off">
                    </div>
                    <div class="col-sm-6 form-group">
                      <label>Middlename</label>
                      <input type="text" class="form-control" placeholder="Middlename" id="mn" autocomplete="off">
                    </div>
                  </div>
                  <div class="row">
                       <div class="col-sm-12 form-group">
                        <label>Lastname</label>
                           <input type="text" class="form-control" placeholder="Lastname" id="ln" autocomplete="off">
                       </div>

                       <div class="col-sm-12 form-group">
                        <label>Gender</label>
                           <select  class="form-control" id="gender_account">
                             <option selected=""></option>
                             <option value="Male">Male</option>
                             <option value="Female">Female</option>
                           </select>
                       </div>

                  </div>

                  	<div class="row" id="doctor_forms" style="display:none ;">

	                   		<div class="col-sm-12 form-group">
	                       		<label> License Number</label>
	                       		<input type="text" name="license_number" id="license_number" class="form-control" placeholder="Enter License number">
	                       	</div>

	                       	<div class="col-sm-6 form-group">
	                       		<label> S2 Number</label>
	                       		<input type="text" name="s2_number" id="s2_number" class="form-control" placeholder="Enter S2 number">
	                       	</div>

	                   		<div class="col-sm-6 form-group">
	                       		<label> Date Issue</label>
	                       		<input type="date" name="date_issue" id="date_issue" class="form-control" placeholder="Enter Date issue">
	                       	</div>

	                       	<div class="col-sm-6 form-group">
	                       		<label> Date Expiry</label>
	                       		<input type="date" name="date_expiry" id="date_expiry" class="form-control" placeholder="Enter Date expiry">
	                       	</div>

	                   		<div class="col-sm-6 form-group">
	                       		<label> PTR Number</label>
	                       		<input type="text" name="ptr_number" id="ptr_number" class="form-control" placeholder="Enter PTR number">
	                       	</div>
	                   	</div>

                  	<div class="row">
                  		<div class="col-sm-12 form-group">
                        <label>Username</label>
                           <input type="text" class="form-control" placeholder="Username" id="username" autocomplete="off">
                       </div>

                        <div class="col-sm-12 form-group">
                        <label>Password</label>
                           <input type="password" class="form-control" placeholder="Password" id="password" autocomplete="off">
                       </div>

                        <div class="col-sm-12 form-group">
                        <label>Confirm Password</label>
                           <input type="password" class="form-control" placeholder="Confirm password" id="c_password" autocomplete="off">
                       </div>

                       <div class="col-sm-12">
                       	<hr>
                       	<span class="text-danger text-sm">
                       		For security, please do not use your social media or email
							password. Remember your password, and answers to your security questions.
                       	</span>
                       		<hr>
                       </div>

                       	<div class="col-sm-8 form-group">
                       		<label> Question 1</label>
                       		<select name="question_1" id="question_1" class="form-control">
                       			<option></option>
                       			<option>What was your childhood nickname?</option>
								<option>In what city did you meet your spouse/significant other?</option>
								<option>What is the name of your favorite childhood friend?</option>
								<option>What street did you live on in third grade?</option>
								<option>What school did you attend for sixth grade?</option>
                       		</select>
                       	</div>

                       		<div class="col-sm-4 form-group">
                       		<label> Answer 1</label>
                       		<input type="text" name="answer_1" id="answer_1" class="form-control" placeholder="Enter Answer 1">
                       	</div>


                       	<div class="col-sm-8 form-group">
                       		<label> Question 2</label>
                       		<select name="question_2" id="question_2" class="form-control">
                       			<option></option>
                       			<option>What is the country of your ultimate dream vacation?</option>
								<option>What was your favorite place to visit as a child?</option>
								<option>What was the name of your first stuffed animal?</option>
								<option>What is your maternal grandmother's maiden name?</option>
								<option>In what city or town was your first job?</option>
                       		</select>
                       	</div>

                       	<div class="col-sm-4 form-group">
                       		<label> Answer 2</label>
                       		<input type="text" name="answer_2" id="answer_2" class="form-control" placeholder="Enter Answer 1">
                       	</div>
                  	</div>


	              
                </div>
              </div>
              <div class="modal-footer text-right">
                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick=" edit_clear_account('','','','','','','','','','','','','','','','','','');">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="add_account();">Save</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
        <!-- /.modal-dialog -->
      </div>



	<div class="modal fade" id="add_patient">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h4 class="modal-title"><span id="add_patient_title">Add Patient</span></h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="add_edit_patient('','','','','','','','','','');">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <div class="container-fluid">
	          <div class="row">
	            <input type="hidden" name="" id="patient_id">
	          
	            <div class="col-sm-6 form-group">
	              <label>Firstname</label>
	              <input type="text" class="form-control" placeholder="Firstname" id="patient_fn" autocomplete="off">
	            </div>
	            <div class="col-sm-6 form-group">
	              <label>Middlename</label>
	              <input type="text" class="form-control" placeholder="Middlename" id="patient_mn" autocomplete="off">
	            </div>
	          </div>
	          <div class="row">
	               <div class="col-sm-12 form-group">
	                <label>Lastname</label>
	                   <input type="text" class="form-control" placeholder="Lastname" id="patient_ln" autocomplete="off">
	               </div>

	               <div class="col-sm-6 form-group">
	                <label>Birthdate</label>
	                   <input type="date" name="" id="bdate" class="form-control">
	               </div>

	               <div class="col-sm-6 form-group">
	                <label>Gender</label>
	                   <select  class="form-control" id="patient_gender">
	                     <option selected=""></option>
	                     <option value="Male">Male</option>
	                     <option value="Female">Female</option>
	                   </select>
	               </div>

	          </div>
	          <div class="row">
	          	<div class="col-sm-6 form-group">
	                <label>Region</label>
	                   <select  class="form-control" id="region" oninput="get_list_province($(this).val());" >
	                   	<?php echo ph_region(); ?>
	                   </select>
               	</div>

               	<div class="col-sm-6 form-group">
	                <label>Province</label>
	                   <select  class="form-control" id="province" oninput="get_list_city($('#region').val(),$(this).val());">
	                   	
	                   </select>
               	</div>


               	<div class="col-sm-6 form-group">
	                <label>City</label>
	                   <select  class="form-control" id="city" oninput="get_list_barangay($('#region').val(),$('#province').val(),$(this).val());">
	                  
	                   </select>
               	</div>


               	<div class="col-sm-6 form-group">
	                <label>Barangay</label>
	                   <select  class="form-control" id="barangay">
	                 
	                   </select>
               	</div>
	          </div>

	        </div>
	      </div>
	      <div class="modal-footer text-right">
	        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="add_edit_patient('','','','','','','','','','');">Cancel</button>
	        <button type="button" class="btn btn-primary" onclick="add_patient(0);">Save</button>
	      </div>
	    </div>
	    <!-- /.modal-content -->
	  </div>
	<!-- /.modal-dialog -->
	</div>

