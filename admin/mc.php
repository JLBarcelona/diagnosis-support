<?php
require('../function/config.php');
require('../function/helper.php');
require('../vendor/FPDF/html_table.php');

$patient_id = $_REQUEST['id'];
$name = $_REQUEST['name'].'.';
$address = $_REQUEST['address'];
$age = $_REQUEST['age'];
$gender = $_REQUEST['gender'];

$data = array('id' => $patient_id);
$sql = "SELECT a.*,b.* from tbl_medical a left join tbl_account b on a.doctor_name=b.account_id where a.medical_id=:id";
$result = fetch_record($con,$data,$sql);

$data_meds = array('id' => $patient_id);
$sql_meds = "SELECT a.*,b.*, a.dosage as dosage from tbl_medical_med_reco a left join tbl_medicine b on a.medicine_id=b.medicine_id where a.medical_id=:id";

$data_meds_out = array('id' => $patient_id);
$sql_meds_out = "SELECT * from tbl_out_medicine where medical_id=:id";

$res = fetch_record($con,$data_meds_out,$sql_meds_out);

$result_meds = fetch_record($con,$data_meds,$sql_meds);

$row = $result->fetch();

$name_doctor = $row['fn'].' '.$row['ln'];
$license_no = $row['license_number'];
$s2 = $row['s2_number'];
$date_issue = $row['date_issue'];
$date_expiry = $row['date_expiry'];
$prt_number = $row['ptr_number'];


$date_conducted = date('M d, Y',strtotime($row['date_entry']));

$illness = $row['diagnoses_result'];


$land ='';
$land_head ='';



$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Ln(8);
$pdf->SetFont('Times','B',20);
$pdf->Cell(0,0,'MEDICAL CERTIFICATE',0,1,'C',false);
$pdf->Ln(18);
$pdf->SetFont('Times','I',14);
$pdf->Cell(0,0,'This is to certify that Mr./Ms. '.$name.' has undergone a',0,1,'C',false);
$pdf->Ln(8);
$pdf->Cell(0,0,'Medical examinaition conducted on '.$date_conducted.' by ',0,1,'C',false);
$pdf->Ln(8);
$pdf->Cell(0,0,'Dr.'.$name_doctor.' is currently suffering from '. $illness.'.',0,1,'C',false);

$pdf->Ln(10);

$pdf->Cell(0,0,'The physician has advised that, for the sake of the person\'s overall health, ',0,1,'C',false);
$pdf->Ln(8);
$pdf->Cell(0,0,'he/she should be allowed to rest for a period of _____ days.',0,1,'C',false);

$pdf->Ln(50);
$pdf->SetFont('Times','B',12);
$pdf->Cell(0,5,'Dr. '.$name_doctor,0,1,'R',false);
$pdf->SetFont('Times','B',10);
$pdf->Cell(0,5,'Physician',0,1,'R',false);
$pdf->SetFont('Times','',10);
$pdf->Cell(0,5,'License No.'.$license_no,0,1,'R',false);
$pdf->Ln(3);

// $pdf->AddPage();
// $pdf->WriteHTML($crop);
$pdf->Output();
?>
