<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<!-- setTimeout(function(){window.print();},2000); -->
<!-- onload="setTimeout(function(){window.print(); window.close();},1000);" -->
<body >
<style type="text/css">
  .font-change{
    font-family: serif;
  }
  @media print {
    p { page-break-after: always; }
  }
</style>
<div class="wrapper font-change">
  <?php 
  include("../function/config.php");
  include("../function/helper.php");
    $patient_id = $_REQUEST['id'];
    $name = $_REQUEST['name'];
    $address = $_REQUEST['address'];
    $age = $_REQUEST['age'];
    $gender = $_REQUEST['gender'];
    
    $data = array('id' => $patient_id);
    $sql = "SELECT a.*,b.* from tbl_medical a left join tbl_account b on a.doctor_name=b.account_id where a.medical_id=:id";
    $result = fetch_record($con,$data,$sql);

    $data_meds = array('id' => $patient_id);
    $sql_meds = "SELECT a.*,b.*, a.dosage as dosage from tbl_medical_med_reco a left join tbl_medicine b on a.medicine_id=b.medicine_id where a.medical_id=:id";

    $data_meds_out = array('id' => $patient_id);
    $sql_meds_out = "SELECT * from tbl_out_medicine where medical_id=:id";

    $res = fetch_record($con,$data_meds_out,$sql_meds_out);

    $result_meds = fetch_record($con,$data_meds,$sql_meds);

    $row = $result->fetch();

    $name_doctor = $row['fn'].' '.$row['ln'];
    $license_no = $row['license_number'];
   ?>
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-12 text-center">
        <div class="h4" style="margin-bottom: -20px;">Republic of the Philippines</div>
        <div class="page-header h1 pt-0 mt-0">
          <img src="../img/logo.png" width="90">
            City Health Office -I
          <img src="../img/ilagan.png" width="90">
          <!-- <small class="float-right">Date: <?php echo date('m/d/Y') ?></small> -->
        </div>
        <div class="h5" style="margin-bottom: -20px;">Centro Poblacion, San Vicente, City of Ilagan, Isabela 3300</div>

         <!-- <h3><?php echo strtoupper('Physician'); ?></h3> -->
        <br><br>
        <hr style="margin-top: -10px;"><hr>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-8 invoice-col">
        <h4>Name: <?php echo $name ?></h4>
      </div>

      <div class="col-sm-2 invoice-col">
        <h4>Age: <?php echo $age ?></h4>
      </div>

      <div class="col-sm-2 invoice-col">
        <h4>Sex: <?php echo $gender ?></h4>
      </div>

    
      <div class="col-sm-8">
         <h4>Address: <?php echo $address ?></h5>
      </div>

      <div class="col-sm-4">
         <h4>Date: <?php echo date('Y-m-d') ?></h5>
      </div>

      <!-- <div class="col-sm-6 invoice-col">
      
        <address>
           Diagnosed: <?php echo $row['diagnoses_result'] ?><br>
           Blood Preassure: <?php echo $row['bp'] ?><br>
        </address>
      </div>
      <div class="col-sm-6 invoice-col">
        <br>
        <address>
         Height:  <?php echo $row['height'] ?><br>
         Weight:  <?php echo $row['weight'] ?><br>
        </address>
      </div> -->
<!-- 
      <div class="col-sm-12 invoice-col">
        <br>
        <address>
         <strong><h4>Note:</h4></strong> <?php echo $row['action_taken'] ?><br>
        </address>
      </div> -->
      <!-- /.col -->
      <div class="col-sm-6 invoice-col">
        <br><br>
        <img src="../img/rx.png" width="100">
      </div>
     
     </div>
    <!-- /.row -->

    <!-- Table row -->
    <br><br>
    <div class="row">
      <div class="col-12 table-responsive">
        <table class="table table-striped">
          <thead>
          <tr>
            <th>Medicine Name</th>
            <th>Qty</th>
            <th>Dosage</th>
            <th>Intake Schedule</th>
          </tr>
          </thead>
         <tbody>
            <?php 
            while ($rows = $result_meds->fetch()) {
            ?>
              <tr>
                <td ><?php echo $rows['brand_name'].' ('.$rows['generic_name'].')' ?></td>
                <td ><?php echo $rows['qty_reco'] ?></td>
                <td ><?php echo $rows['dosage'] ?></td>
                <td ><?php echo $rows['intake_schedule'] ?></td>
              </tr>
            <?php
            }
             ?>

             <?php 
              while ($rr = $res->fetch()) {
               ?>
                <tr>
                  <td ><?php echo $rr['medicine_brand'].' ('.$rr['medicine_generic_name'].')' ?></td>
                  <td ><?php echo $rr['qty'] ?></td>
                  <td ><?php echo $rr['dossage'] ?></td>
                  <td ><?php echo $rr['intake_schedule'] ?></td>
                </tr>
              <?php
              }
              ?>
            
          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>

    <div class="row invoice-info">
      <div class="col-sm-12 invoice-col text-right">
        <br><br><br>
         <strong><h4>Lic No: <?php echo $license_no ?></h4></strong> <br>

      </div>
    </div>
    <!-- /.row -->

    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
