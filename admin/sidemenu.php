<aside class="main-sidebar sidebar-light-info elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link bg-info">
      <img src="../img/logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light"><?php echo user_position()[$auth['user_type']] ?></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo '../'.avatar('',$auth['gender'],$auth['user_type']) ?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo ucfirst($auth['fn']).' '.ucfirst($auth['ln']) ?></a>
        </div>
      </div>

      <?php if ($auth['user_type'] == 0): ?>
           <!-- Sidebar Menu -->
            <nav class="mt-2">
              <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                 <li class="nav-item" >
                  <a href="index.php" class="nav-link" id="dashboard">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                     Dashboard
                  </a>
                </li>

                <li class="nav-item">
                  <hr style="margin:3px;">
                </li>
               

                <li class="nav-header">Account Management</li>
                 
             
                <li class="nav-item">
                  <a href="record_officer.php" class="nav-link" id="record_off">
                    <i class="nav-icon fas fa-user-lock"></i>
                    <p>Record Officer Account</p>
                  </a>
                </li>
                
                <li class="nav-item">
                  <a href="doctors.php" class="nav-link" id="doctors">
                    <i class="nav-icon fas fa-user-md"></i>
                    <p>Doctors Account</p>
                  </a>
                </li>

                <li class="nav-item">
                   <a href="nurse.php" class="nav-link" id="nurse">
                    <i class="nav-icon fas fa-user-nurse"></i>
                    <p>Nurses Account</p>
                  </a>
                </li>

                 <li class="nav-item">
                  <a href="pharma.php" class="nav-link" id="pharma">
                    <i class="nav-icon fas fa-capsules"></i>
                    <p>Pharmacists Account</p>
                  </a>
                </li>

                <li class="nav-item">
                  <a href="activity_logs.php" class="nav-link" id="logs">
                    <i class="nav-icon fas fa-history"></i>
                    <p>Activity Logs</p>
                  </a>
                </li>

                <li class="nav-header">Medicine Inventory</li>

                 <li class="nav-item">
                  <a href="medicines.php" class="nav-link" id="med_inventory">
                    <i class="fas fa-briefcase-medical"></i>
                    <p>Available Medicines</p>
                  </a>
                </li>

                <li class="nav-header">Patient Information</li>
                <li class="nav-item">
                  <a href="patient.php" class="nav-link" id="patient">
                    <i class="fas fa-wheelchair"></i>
                    <p>Patient Management</p>
                  </a>
                </li>

                <li class="nav-item">
                  <a href="check_up.php" class="nav-link" id="check">
                    <i class="fas fa-calendar-check"></i>
                    <p>Check Up Records</p>
                  </a>
                </li>

                 <li class="nav-item">
                  <a href="diagnosis.php" class="nav-link" id="diagnosis">
                    <i class="fas fa-wheelchair"></i>
                    <p>Patient</p>
                  </a>
                </li>

              </ul>
            </nav>
            <!-- /.sidebar-menu -->
      <?php endif ?>


       <?php if ($auth['user_type'] == 1): ?>
           <!-- Sidebar Menu -->
            <nav class="mt-2">
              <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                 <li class="nav-item" >
                  <a href="index.php" class="nav-link" id="dashboard">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                     Dashboard
                  </a>
                </li>

                <li class="nav-item">
                  <hr style="margin:3px;">
                </li>

                

                 <li class="nav-header">Patient Management</li>
                   <li class="nav-item">
                    <a href="diagnosis.php" class="nav-link" id="diagnosis">
                      <i class="fas fa-wheelchair"></i>
                      <p>For Diagnosis <div class="badge badge-danger float-right" id="patient_today">0</div></p>
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="check_up.php" class="nav-link" id="check">
                      <i class="fas fa-wheelchair"></i>
                      <p>Patient Records</p>
                    </a>
                  </li>

                <li class="nav-header">Medicine Inventory</li>

                 <li class="nav-item">
                  <a href="medicines.php" class="nav-link" id="med_inventory">
                    <i class="fas fa-briefcase-medical"></i>
                    <p>Available Medicines</p>
                  </a>
                </li>

                <li class="nav-item">
                  <a href="outgoing_medicine.php" class="nav-link" id="outgoing">
                    <i class="fas fa-briefcase-medical"></i>
                    <p>Dispensed Medicines</p>
                  </a>
                </li>

               
                
              </ul>
            </nav>
            <!-- /.sidebar-menu -->
      <?php endif ?>


       <?php if ($auth['user_type'] == 2): ?>
           <!-- Sidebar Menu -->
            <nav class="mt-2">
              <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                 <li class="nav-item" >
                  <a href="index.php" class="nav-link" id="dashboard">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                     Dashboard
                  </a>
                </li>

                <li class="nav-item">
                  <hr style="margin:3px;">
                </li>

                <li class="nav-header">Patient Management</li>
               <!--  <li class="nav-item">
                  <a href="patient.php" class="nav-link" id="patient">
                    <i class="fas fa-wheelchair"></i>
                    <p>Patient Information</p>
                  </a>
                </li> -->

                <li class="nav-item">
                  <a href="patient_ini.php" class="nav-link" id="patient_ini">
                    <i class="fas fa-wheelchair"></i>
                    <p>Patient's Initial Check-up</p>
                  </a>
                </li>

            <!--     <li class="nav-item">
                  <a href="check_up.php" class="nav-link" id="check">
                    <i class="fas fa-calendar-check"></i>
                    <p>Check Up Records</p>
                  </a>
                </li> -->

              </ul>
            </nav>
            <!-- /.sidebar-menu -->
      <?php endif ?>


      <?php if ($auth['user_type'] == 3): ?>
           <!-- Sidebar Menu -->
            <nav class="mt-2">
              <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                 <li class="nav-item" >
                  <a href="index.php" class="nav-link" id="dashboard">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                     Dashboard
                  </a>
                </li>

                <li class="nav-item">
                  <hr style="margin:3px;">
                </li>
               
                <li class="nav-header">Medicine Inventory</li>

                 <li class="nav-item">
                  <a href="medicines.php" class="nav-link" id="med_inventory">
                    <i class="fas fa-briefcase-medical"></i>
                    <p>Available Medicines</p>
                  </a>
                </li>


                 <li class="nav-item">
                  <a href="outgoing_medicine.php" class="nav-link" id="outgoing">
                    <i class="fas fa-briefcase-medical"></i>
                    <p>Dispensed Medicines</p>
                  </a>
                </li>

                <li class="nav-header">Check-Up Records</li>
                <li class="nav-item">
                  <a href="check_up.php" class="nav-link" id="check">
                    <i class="fas fa-wheelchair"></i>
                    <p>Patient Records</p>
                    <div class="badge badge-danger float-right" id="finish_today">0</div>
                  </a>
                </li>

              </ul>
            </nav>
            <!-- /.sidebar-menu -->
      <?php endif ?>

       <?php if ($auth['user_type'] == 4): ?>
           <!-- Sidebar Menu -->
            <nav class="mt-2">
              <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                 <li class="nav-item" >
                  <a href="index.php" class="nav-link" id="dashboard">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                     Dashboard
                  </a>
                </li>

                <li class="nav-item">
                  <hr style="margin:3px;">
                </li>
               

                <li class="nav-header">Account Management</li>
                 
             
                <li class="nav-item">
                  <a href="record_officer.php" class="nav-link" id="record_off">
                    <i class="nav-icon fas fa-user-lock"></i>
                    <p>Record Officer Account</p>
                  </a>
                </li>
                
                <li class="nav-item">
                  <a href="doctors.php" class="nav-link" id="doctors">
                    <i class="nav-icon fas fa-user-md"></i>
                    <p>Doctors Account</p>
                  </a>
                </li>

                <li class="nav-item">
                   <a href="nurse.php" class="nav-link" id="nurse">
                    <i class="nav-icon fas fa-user-nurse"></i>
                    <p>Nurses Account</p>
                  </a>
                </li>

                 <li class="nav-item">
                  <a href="pharma.php" class="nav-link" id="pharma">
                    <i class="nav-icon fas fa-capsules"></i>
                    <p>Pharmacists Account</p>
                  </a>
                </li>

                <li class="nav-header">Medicine Inventory</li>

                 <li class="nav-item">
                  <a href="medicines.php" class="nav-link" id="med_inventory">
                    <i class="fas fa-briefcase-medical"></i>
                    <p>Available Medicines</p>
                  </a>
                </li>

                  <li class="nav-item">
                  <a href="outgoing_medicine.php" class="nav-link" id="outgoing">
                    <i class="fas fa-briefcase-medical"></i>
                    <p>Dispensed Medicines</p>
                  </a>
                </li>

               
               
                 <li class="nav-header">Patient Information</li>
                  <li class="nav-item">
                    <a href="check_up.php" class="nav-link" id="check">
                      <i class="fas fa-calendar-check"></i>
                      <p>Check Up Records</p>
                    </a>
                  </li>

                 <li class="nav-header">Illness Information</li>
                 <li class="nav-item">
                    <a href="illness.php" class="nav-link" id="illness_page">
                      <i class="fas fa-thermometer-empty"></i>
                      <p>Illness</p>
                    </a>
                  </li>


              </ul>
            </nav>
            <!-- /.sidebar-menu -->
      <?php endif ?>

    </div>
    <!-- /.sidebar -->
  </aside>

  <script type="text/javascript">
  function sidebar_selected_side(id){
    $("#"+id).addClass('active');
  }

 
  </script>