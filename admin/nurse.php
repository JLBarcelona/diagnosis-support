<?php include("header.php") ?>
<?php include("user_direction.php") ?>

<body class="hold-transition sidebar-mini layout-fixed" onload="show_nurse();  sidebar_selected_side('nurse');">
<div class="wrapper">

  <!-- Navbar -->
<?php include("navbar.php") ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
<?php include("sidemenu.php") ?>
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <!-- <h1 class="m-0 text-dark">Dashboard</h1> -->
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <button class="btn btn-success" onclick="new_account('2','Add Nurse');"><i class="fas fa-user-plus"></i> Add Nurse</button>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <!-- ./col -->
          <div class="col-12">
          <!-- /.card -->

          <div class="card">
            <div class="card-header">
              <h3 class="card-title"><i class="nav-icon fas fa-user-nurse"></i> Nurse Management</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="table-responsive">
              <table id="tbl_nurse" class="table table-bordered table-striped">
                <thead>
                   <tr>
                    <td nowrap>Name</td>
                    <td nowrap>Username</td>
                    <td width="15%" nowrap>Gender</td>
                    <td width="10%" nowrap>option</td>
                  </tr>
                </thead>

                <tbody id="data_nurse"></tbody>
              </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>

        </div>
        <!-- /.row -->
        <!-- Main row -->
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>

  <div class="modal fade" id="add_teacher">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Add Teacher</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="container-fluid">
                <div class="row">
                  <input type="hidden" name="" id="user_id">
                  <div class="col-sm-6 form-group">
                    <label>Firstname</label>
                    <input type="text" class="form-control" placeholder="Firstname" id="fn" autocomplete="off">
                  </div>
                  <div class="col-sm-6 form-group">
                    <label>Middlename</label>
                    <input type="text" class="form-control" placeholder="Middlename" id="mn" autocomplete="off">
                  </div>
                </div>
                <div class="row">
                     <div class="col-sm-12 form-group">
                      <label>Lastname</label>
                         <input type="text" class="form-control" placeholder="Lastname" id="ln" autocomplete="off">
                     </div>

                     <div class="col-sm-12 form-group">
                      <label>Gender</label>
                         <select  class="form-control" id="gender">
                           <option selected=""></option>
                           <option value="Male">Male</option>
                           <option value="Female">Female</option>
                         </select>
                     </div>

                     <div class="col-sm-12 form-group">
                      <label>Address</label>
                         <textarea class="form-control" id="address" placeholder="Address" autocomplete="off"></textarea>
                     </div>

                      <div class="col-sm-12 form-group">
                      <label>CP Number</label>
                         <input type="text" class="form-control" maxlength="11" onkeypress="return num_only(event)" placeholder="CP Number" id="cp_number" autocomplete="off">
                     </div>

                     <div class="col-sm-12 form-group">
                      <label>Email</label>
                         <input type="text" class="form-control" placeholder="Email" id="email" autocomplete="off">
                     </div>
                </div>
              </div>
            </div>
            <div class="modal-footer text-right">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-primary" onclick="register_teacher();">Save</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
  </div>
<!-- ./wrapper -->
<?php include('footer.php') ?>
</body>
</html>


