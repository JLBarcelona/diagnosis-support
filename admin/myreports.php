<?php include("header.php") ?>
<body class="hold-transition sidebar-mini layout-fixed" onload="show_teacher_class_attendance(); sidebar_selected_side('teacher_report');">
<div class="wrapper">

  <!-- Navbar -->
<?php include("navbar.php") ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
<?php include("sidemenu.php") ?>
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-5">
            <div class="form-group">
              <label>Subject</label>
              <select class="form-control" id="subject" oninput="onpick();">
                <option></option>
                <?php echo show_teachers_subject($con,$auth['user_id']); ?>
              </select>
            </div>
          </div>
          <div class="col-sm-3">
            <label>Date</label>
           <input type="date" class="form-control" id="date_from" disabled="">
           <!--  <button class="btn btn-primary">Submit</button> -->
          </div>
          <div class="col-sm-1">
            <label style="color:transparent;">submit</label>
            <button class="btn btn-primary" onclick="show_teacher_class_attendance()">Search</button>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <!-- ./col -->
          <div class="col-12">
          <!-- /.card -->

          <div class="card">
            <div class="card-header">
              <h3 class="card-title"><i class="nav-icon fas fa-table"></i> Reports</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="tbl_teacher" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <td width="5%" class="text-center">#</td>
                    <td>Name</td>
                    <td width="20%">Time In</td>
                    <td width="20%">Time Out</td> 
                  </tr>
                </thead>

                <tbody id="data_teacher"></tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>

        </div>
        <!-- /.row -->
        <!-- Main row -->
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<?php include('footer.php') ?>
</body>
</html>


<script type="text/javascript">
  function onpick(){
     if ($("#subject").val() == null || $("#subject").val() == "") {
        $("#date_from").attr('disabled', true);
        // $("#date_end").attr('disabled', true);

         $("#date_from").val('');
         // $("#date_end").val('');
      }else{
        $("#date_from").attr('disabled', false);
        // $("#date_end").attr('disabled', false);
      }
  }
</script>

<script type="text/javascript">
  function show_teacher_class_attendance(){
    var subject = $("#subject");

    var date_from = $("#date_from");
    
    if (subject.val() == "" || date_from.val() == "") {
      $("#data_teacher").html('');
    }else{
      var mydata = 'action=show_teacher_class' + '&subject=' + subject.val() + '&date_from=' + date_from.val();  

      $.ajax({
        type:"POST",
        url:url,
        data:mydata,
        cache:false,
        beforeSend:function(){
           $("#data_teacher").html('<center><img src="../img/load.gif" width="50" class="img-fluid"></center>');
        },
        success:function(data){
           $("#data_teacher").html(data);
        }
      });
    }
     $("#tbl_teacher").DataTable();
}
</script>
