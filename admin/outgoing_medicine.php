<?php include("header.php") ?>
<?php include("user_direction.php") ?>


<link rel="stylesheet" href="../dist/tags/tagsinput.css">

<body class="hold-transition sidebar-mini layout-fixed" onload="show_out_medicine();  sidebar_selected_side('outgoing');">
<div class="wrapper">

  <!-- Navbar -->
<?php include("navbar.php") ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
<?php include("sidemenu.php") ?>
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <!-- <h1 class="m-0 text-dark">Dashboard</h1> -->
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
         <!--    <button class="btn btn-success" data-toggle="modal" data-backdrop="static" data-target="#add_medicine"><i class="fas fa-pills"></i> Add Medicine</button> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <!-- ./col -->
          <div class="col-12">
          <!-- /.card -->

          <div class="card">
            <div class="card-header">
              <h3 class="card-title"><i class="fas fa-briefcase-medical"></i> Dispensed Medicines</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="table-responsive">
              <table id="tbl_medicine" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <td width="20%" nowrap>Brand Name</td>
                    <td nowrap>Generic Name</td>
                    <td width="10%" nowrap>Qty</td>
                    <td width="10%" nowrap>Dosage</td>
                    <td width="10%" nowrap>Form/Type</td>
                    <td nowrap>Supplier</td>
                    <td width="25%" nowrap>Use for</td>
                    <!-- <td class="text-center" nowrap>Option</td> -->
                  </tr>
                </thead>

                <tbody id="data_medicine"></tbody>
              </table>
            </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>



        </div>
        <!-- /.row -->
        <!-- Main row -->
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>

<!-- ./wrapper -->
<?php include('footer.php') ?>
<script type="text/javascript" src="../dist/tags/tagsinput.js"></script>
</body>
</html>



