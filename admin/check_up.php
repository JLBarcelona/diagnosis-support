<?php include("header.php") ?>
<?php include("user_direction.php") ?>

 <link rel="stylesheet" href="../dist/checkup/tagsinput.css">

<body class="hold-transition sidebar-mini layout-fixed" onload="sidebar_selected_side('check'); $('#date_check').val('<?php echo date('Y-m-d') ?>'); show_check_up();">

<div class="wrapper">
  <!-- Navbar -->
<?php include("navbar.php") ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
<?php include("sidemenu.php") ?>
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <!-- <h1 class="m-0 text-dark">Dashboard</h1> -->
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
         <!--    <button class="btn btn-success" data-toggle="modal" data-backdrop="static" data-target="#add_patient" onclick="$('#add_patient_title').text('Add Patient')"><i class="fas fa-user-plus"></i> Add Patient</button> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <!-- ./col -->
          <div class="col-12">
          <!-- /.card -->

          <div class="card">
            <div class="card-header">
              <div class="row m-0">
                <div class="card-title col-sm-6 h3 pt-2"><i class="nav-icon fas fa-calendar-check"></i> Patient Records</div>
                <div class="input-group col-sm-3">
                  <input type="text" class="form-control" id="illness_type" placeholder="Search Illness">
                </div>
                <div class="input-group col-sm-3">
                  <input type="date" class="form-control" id="date_check">
                  <div class="input-group-append">
                    <button type="button" class="btn btn-outline-success" onclick="show_check_up();">Search</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="table-responsive">
              <table id="tbl_check" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <td nowrap>#</td>
                    <td nowrap>Name</td>
                    <td nowrap>Date Check Up</td>
                    <td nowrap>Notes</td>
                    <td nowrap>Diagnosed Illness</td>
                    <td nowrap>Prescription</td>
                    <td width="10%" nowrap >option</td>
                  </tr>
                </thead>
                <tbody id="data_check"></tbody>
              </table>
            </div>
          </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>



        </div>
        <!-- /.row -->
        <!-- Main row -->
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>

<!-- ./wrapper -->
<?php include('footer.php') ?>
<script type="text/javascript" src="../dist/checkup/tagsinput.js"></script>
</body>
</html>
