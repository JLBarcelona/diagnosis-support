 <nav class="main-header navbar navbar-expand navbar-dark navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>


    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-user-circle"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
         <?php if ($auth['user_type'] == 1): ?>
            <a href="#" class="dropdown-item" data-toggle="modal" data-backdrop="static" data-target="#update_user">
              <i class="fas fa-user-circle mr-2"></i> Enter License Number
            </a>
         <?php endif ?>
           <a href="#" class="dropdown-item" data-toggle="modal" data-backdrop="static" data-target="#update_password" >
            <i class="fas fa-user-cog mr-2"></i> Change Password
          </a>
          <div class="dropdown-divider"></div>
          <a href="logout.php" class="dropdown-item">
            <i class="fas fa-sign-out-alt mr-2"></i> Logout
          </a>
        </div>
      </li>
    </ul>
  </nav>

  <?php include("./modal.php") ?>