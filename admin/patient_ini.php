<?php include("header.php") ?>
<?php include("user_direction.php") ?>

 <link rel="stylesheet" href="../dist/checkup/tagsinput.css">

<body class="hold-transition sidebar-mini layout-fixed" onload="show_patient_ini();  sidebar_selected_side('patient_ini');">
<div class="wrapper">

  <!-- Navbar -->
<?php include("navbar.php") ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
<?php include("sidemenu.php") ?>
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <!-- <h1 class="m-0 text-dark">Dashboard</h1> -->
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
           <!--  <button class="btn btn-success" data-toggle="modal" data-backdrop="static" data-target="#add_patient" onclick="$('#add_patient_title').text('Add Patient')"><i class="fas fa-user-plus"></i> Add Patient</button> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <!-- ./col -->
          <div class="col-12">
          <!-- /.card -->

          <div class="card">
            <div class="card-header">
              <h3 class="card-title"><i class="nav-icon fas fa-wheelchair"></i> Patient Initial Check-up</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
               <div class="table-responsive">
              <table id="tbl_patient" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <td nowrap>Name</td>
                    <td width="15%" nowrap>Age</td>
                    <td width="15%" nowrap>Gender</td>
                    <td >Address</td>
                    <td width="10%" nowrap>option</td>
                  </tr>
                </thead>
                <tbody id="data_patient"></tbody>
              </table>
            </div>
          </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>



        </div>
        <!-- /.row -->
        <!-- Main row -->
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>

<!-- ./wrapper -->
<?php include('footer.php') ?>
<script type="text/javascript" src="../dist/checkup/tagsinput.js"></script>
</body>
</html>


<script type="text/javascript">
  function check_up(id,name){
    $("#check_up_modal").modal({'backdrop':'static'});
    $("#name_patient").text(name);
    $("#patient_id").val(id);
  }
</script>
