<?php include("header.php") ?>
<?php 
if ($auth['user_type'] == 2) {
  @header('location:teacher_index.php');
}else if ($auth['user_type'] == 3) {
  @header('location:student_index.php'); 
}
 ?>

 <link rel="stylesheet" href="../dist/tags/tagsinput.css">
<body class="hold-transition sidebar-mini layout-fixed" onload="show_illness();  sidebar_selected_side('illness_page');">
<div class="wrapper">

  <!-- Navbar -->
<?php include("navbar.php") ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
<?php include("sidemenu.php") ?>
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <!-- <h1 class="m-0 text-dark">Dashboard</h1> -->
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <button class="btn btn-success" data-toggle="modal" data-backdrop="static" data-target="#illness_form_modal"><i class="fas fa-user-plus"></i> Add Illness</button>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

 <div class="modal fade" role="dialog" id="illness_form_modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title" id="illness_form_title">
              Add Illness
            </div>
              <button class="close" data-dismiss="modal" onclick=" edit_clear_illness('','',''); $('#illness_form_title').text('Add illness');">&times;</button>
          </div>
          <div class="modal-body">
            <form id="form_illness_symp" onsubmit="event.preventDefault(); save_illnesses(); ">
              <div class="row">
                <div class="form-group col-sm-12">
                  <input type="hidden" name="illness_id" id="illness_id">
                  <label> Type of Illness</label>
                  <input type="text" name="illness_name" id="illness_name" class="form-control" placeholder="Enter Type of illness">
                </div>
              </div>
              
              <div class="form-group col-sm-12">
                <label>Symptoms</label>
                <input type="text" list="list_symptoms" data-role="tagsinput" id="illness_symptoms"  class="form-control" autocomplete="off">
              </div>
              <datalist id="list_symptoms">
                <?php echo option_symptoms($con) ?>
              </datalist>    

              <div class="col-sm-12 text-right">
                <button class="btn btn-danger" data-dismiss="modal" onclick=" edit_clear_illness('','',''); $('#illness_form_title').text('Add illness');">Cancel</button>
                <button class="btn btn-primary">Save</button>
              </div>          
            </form>
          </div>
          <div class="modal-footer">
            
          </div>
        </div>
      </div>
    </div>

   

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <!-- ./col -->
          <div class="col-12">
          <!-- /.card -->

          <div class="card">
            <div class="card-header">
              <h3 class="card-title"><i class="nav-icon fas fa-user-md"></i> Illness Management</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="tbl_illness" class="table table-bordered table-striped table-hover" style="cursor: pointer;">
                <thead>
                  <tr>
                    <td nowrap>Types of Illness</td>
                    <td nowrap>Symptoms</td>
                  </tr>
                </thead>

                <tbody id="data_illness"></tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>



        </div>
        <!-- /.row -->
        <!-- Main row -->
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>

<!-- ./wrapper -->
<?php include('footer.php') ?>
<script type="text/javascript" src="../dist/tags/tagsinput.js"></script>
 <script type="text/javascript">

      function edit_clear_illness(illness_id,illness_name,illness_symptoms){
        $("#illness_id").val(illness_id);
        $("#illness_name").val(illness_name);
        // alert(illness_symptoms);
        $('#illness_symptoms').tagsinput('removeAll');
        $("#illness_symptoms").val(illness_symptoms);
        $("#illness_symptoms").tagsinput('add',illness_symptoms);
      }

      function on_edit_illness(title){
        $("#illness_form_modal").modal({'backdrop' : 'static'});
        $("#illness_form_title").text(title);
      }

      function save_illnesses(){
        var illness_name = $("#illness_name");
        var illness_symptoms = $("#illness_symptoms");


        if (illness_name.val() == "") {
          illness_name.focus();
          twal("Types of Illness required!","error");
        }
        else if (illness_symptoms.val() == "") {
          illness_symptoms.focus();
          twal("Illness symptoms is required!","error");
          $(".bootstrap-tagsinput").focus();
        }else{

          var mydata = 'action=save_illness' + '&'+$("#form_illness_symp").serialize() + '&symptoms=' + illness_symptoms.val();

            $.ajax({
            type:"POST",
            url:url,
            data:mydata,
            cache:false,
            success:function(data){
              if (data.trim() == 1) {
               twal("Illness has been saved!","success");
               show_illness();
               edit_clear_illness('','','');
               $("#illness_form_title").text('Add illness');
               $("#illness_form_modal").modal('hide');
              }
              else if (data.trim() == 2) {
                twal("Illness has been edited!","success");
                show_illness();
                edit_clear_illness('','','');
                $("#illness_form_title").text('Add illness');
                $("#illness_form_modal").modal('hide');
              }else{
                console.log(data.trim());
                twal("Illness saving occured error please check your console!","error");

              }
            }
          });
        }
      }


    </script>

</body>
</html>


