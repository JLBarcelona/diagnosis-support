<?php
  date_default_timezone_set('Asia/Manila');
  header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Credentials: true");
  header("Access-Control-Allow-Methods: POST");
  header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");
  header("Content-Type: application/json; charset=utf-8");

  include "library/config.php";
  
  $postjson = json_decode(file_get_contents('php://input'), true);
  $today    = date('Y-m-d');

   function format_date($format,$date){
        return date ($format,strtotime($date));
      }

    function get_attendance($time,$date){
      if (format_date('l',$date) == 'Sunday' && empty($time) || format_date('l',$date) == 'Saturday' && empty($time)) {
        return 'Weekend';
      }else if (format_date('l',$date) != 'Sunday' && empty($time) || format_date('l',$date) != 'Saturday' && empty($time)) {
        return 'Absent';
      }else{
        return format_date('h:i A', $time);
      }
    }

  $action = $postjson['action'];
  switch ($action) {
    case 'login':

      $username = $postjson['username'];
      $password = $postjson['password'];

      if (empty($username) || empty($password)) {
          $result = json_encode(array('success'=>false, 'msg'=>'Username and Password is required!'));
      }else{
        $query = mysqli_query($mysqli, "SELECT * FROM tbl_user WHERE username='$postjson[username]' and user_type = 2");

        $check = mysqli_num_rows($query);

        if($check>0){
          $data = mysqli_fetch_array($query);
          $datauser = array(
            'user_id' => $data['user_id'],
            'username' => $data['username'],
            'password' => $data['password'],
            'name' => ucfirst($data['fn']).' '.ucfirst($data['ln'])
          );

          if(password_verify($postjson['password'], $data['password']) && $data['is_delete']==0){
            $result = json_encode(array('success'=>true, 'result'=>$datauser));
          }else{
            $result = json_encode(array('success'=>false, 'msg'=>'Account Inactive')); 
          }

        }else{
          $result = json_encode(array('success'=>false, 'msg'=>'Invalid Account'));
        } 
      }

       

        echo $result;
      break;
    
      case 'getclass':
          $data = array();
          $query = mysqli_query($mysqli, "SELECT * FROM tbl_class where teacher_id='$postjson[user_id]' ");

          while($row = mysqli_fetch_array($query)){

            $data[] = array(
              'class_id' => $row['class_id'],
              'subject' => $row['subject']
            );
          }

          if($query) $result = json_encode(array('success'=>true, 'result'=>$data));
          else $result = json_encode(array('success'=>false));

          echo $result;
      break;

      case 'time_in':
       $class_id = $postjson['class_id'];
       $qrcode = $postjson['qr_code'];



       $query = mysqli_query($mysqli, "SELECT * FROM tbl_user where qrcode_id='$qrcode' ");

       if (mysqli_num_rows($query) > 0) {
          $row = mysqli_fetch_array($query);
          $user_id = $row['user_id'];
          $date_today = date('Y-m-d');
          $date_time_now = date('Y-m-d h:i');
          $ampm = date('A');

          $check = mysqli_query($mysqli,"SELECT * from tbl_attendance where class_id='$class_id' and user_id='$user_id' and date_attend='$date_today'");

          $rows = mysqli_fetch_array($check);

          $check_time_in_out = mysqli_query($mysqli,"SELECT * from tbl_attendance where class_id='$class_id' and user_id='$user_id' and date_attend='$date_today' and time_in is not null and time_out is not null");

          if (mysqli_num_rows($check_time_in_out) > 0) {
                  $result = json_encode(array('success'=>false, 'msg'=>'You already timed in and timed out!'));
          }else{
                if (strtotime($rows['time_in']) > strtotime("-1 minutes")) {
                  $result = json_encode(array('success'=>false, 'msg'=>'You already timed in!'));
                }else{

                    $check_time_in = mysqli_query($mysqli,"SELECT * from tbl_attendance where class_id='$class_id' and user_id='$user_id' and date_attend='$date_today' and time_in is not null");

                    if (mysqli_num_rows($check_time_in) > 0) {

                      $result_check = mysqli_fetch_array($check_time_in);

                      $atten_id = $result_check['attendance_id'];

                       $sql = mysqli_query($mysqli,"UPDATE tbl_attendance set time_out='$date_time_now',ampm='AM/PM' where attendance_id='$atten_id'");

                     if($sql) $result = json_encode(array('success'=>true, 'msg'=> 'Time out successfully!'));
                       else $result = json_encode(array('success'=>false, 'msg'=>'Invalid'));
                    }else{
                       $sql = mysqli_query($mysqli,"INSERT INTO tbl_attendance (class_id,user_id,date_attend,time_in,ampm) VALUES('$class_id','$user_id','$date_today','$date_time_now','AM')");

                       if($sql) $result = json_encode(array('success'=>true, 'msg'=> 'Time in successfully!'));
                       else $result = json_encode(array('success'=>false, 'msg'=>'Invalid'));
                    }
                }
          }
       }else{
          $result = json_encode(array('success'=>false, 'msg'=>'Account Inactive'));
       }

        echo $result;

      break;

      case 'getclass_attendance':
        $data = array();
        $subject_id = $postjson['class_id'];
        $date_from = format_date('Y-m-d',$postjson['date_attend']);

        $sql_class = mysqli_query($mysqli,"SELECT * from tbl_class where class_id='$subject_id'");

        $class =  mysqli_fetch_array($sql_class);


        $course = $class['course'];
        $year = $class['year'];
        $section = $class['section'];

        $sql = mysqli_query($mysqli,"SELECT * from tbl_user where course='$course' and year='$year' and section='$section' and is_delete = 0 order by ln,fn,mn asc");

        while ($row = mysqli_fetch_array($sql)) {
          $mname = (!empty($row['mn'])) ? $row['mn'][0] : '';
          $name = $row['ln'].', '.$row['fn'].' '.$mname.'.'; 

          $users = $row['user_id'];


          $sql_attendance = mysqli_query($mysqli,"SELECT * from tbl_attendance where date_attend='$date_from' and user_id='$users' and class_id='$subject_id'");

          $attendance = mysqli_fetch_array($sql_attendance);
           
           $data[] = array(
              'user_id' => $users,
              'name' => $name,
              'attendance' => get_attendance($attendance['time_in'],$date_from)
            );
          }


          if($sql_attendance) $result = json_encode(array('success'=>true, 'result'=>$data));
          else $result = json_encode(array('success'=>false));

          echo $result;
       

      break;

    default:
      echo json_encode(array('success'=>false, 'msg'=>'Invalid '.$action));
    break;
  }


       

?>