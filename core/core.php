<?php 


function insert($con,$table,$arrs){
		$parameter = array();
		$data = array();
		$dataparam = array();

		foreach ($arrs as $key => $value) {
			$parameter[] = $key;
			$dataparam[] = ":".$key;
			$data[$key] = $value;
		}

		$sql = "INSERT INTO ".$table."(".implode(",", $parameter).") VALUES(".implode(",", $dataparam).")";

		if (save($con,$data,$sql) > 0) {
			return 1;
		}else{
			return save($con,$data,$sql);
		}
		
	}

function get_last_id($con,$table,$arrs){
		$parameter = array();
		$data = array();
		$dataparam = array();

		foreach ($arrs as $key => $value) {
			$parameter[] = $key;
			$dataparam[] = ":".$key;
			$data[$key] = $value;
		}

		$sql = "INSERT INTO ".$table."(".implode(",", $parameter).") VALUES(".implode(",", $dataparam).")";

		if (save($con,$data,$sql) > 0) {
			return $con->lastInsertId();
		}else{
			return save($con,$data,$sql);
		}
	
}

	function update($con,$table,$arrs){
		$get_parameter = array();
		$parameter = array();
		$data = array();
		$dataparam = array();
		$selector = "";
		$data_update = array();

		foreach ($arrs as $key => $value) {
			$parameter[] = $key."=:".$key;
			$data[$key] = $value;
		}


		for ($i=0; $i < count($parameter); $i++) { 
			if ($i == 0) {
				$selector = $parameter[$i];
			}else{
				$get_parameter[] = $parameter[$i];
			}
		}


		$sql = "UPDATE ".$table." set ".implode(",", $get_parameter)." WHERE ".$selector;

		if (save($con,$data,$sql) > 0) {
			return 1;
		}else{
			return save($con,$data,$sql);
		}
		
	}


	// only 1 parameter
	function delete($con,$table,$arrs){
		$get_parameter = array();
		$parameter = array();
		$data = array();
		$dataparam = array();
		$selector = "";
		$data_update = array();

		foreach ($arrs as $key => $value) {
			$parameter[] = $key."=:".$key;
			$data[$key] = $value;
		}


		for ($i=0; $i < count($parameter); $i++) { 
			if ($i == 0) {
				$selector = $parameter[$i];
			}else{
				$get_parameter[] = $parameter[$i];
			}
		}


		$sql = "DELETE FROM ".$table." WHERE ". $selector;

		if (save($con,$data,$sql) > 0) {
			return 1;
		}else{
			return save($con,$data,$sql);
		}
	}

 ?>